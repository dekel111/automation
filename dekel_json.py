import requests
import logging
import json
from collections import Counter


# this is the URL for API that displayed in the URL below "https://restcountries.eu/" #


if __name__ == '__main__':

    url = 'https://restcountries.eu/rest/v1/all'
    r = requests.get(url=url)
    countries = json.loads(r.text)
    for country in countries:
        print(country['name'])
        print(country['capital'])
        for capit in country:
            print(country['capital'])


            # Most number display in list
            #  need to add from collections import Counter
            # numbers = [1, 1, 1, 1, 1, 15, 3, 2, 6, 0, 30, 33, 22, 77, 80, 90, 44, 50, 34]
            # count = Counter(numbers)
            # print(count.most_common(1))
            #
            #
            # # Split to even and odd
            # numbers = [1, 5, 3, 2, 4, 8, 88, 44, 33, 22, 55, 29, 68, 25, 52, 90, 80]
            # even = []
            # odd = []
            # for num in numbers:
            #     if num % 2 == 0:
            #         even.append(num)
            #     else:
            #         odd.append(num)
            # even.sort()
            # odd.sort()
            # print('This is even numbers', even)
            # print('This is even numbers', odd)
            #
            # # Max/Min/Sum number
            # numbers = [1, 5, 3, 2, 4, 8, 88, 44, 33, 22, 55, 29, 68, 25, 52, 90, 80]
            # print (max(numbers))
            # print (min(numbers))
            # print (sum(numbers))

            # AVG
            # numbers = [1, 5, 3, 2, 4, 8, 88, 44, 33, 22, 55, 29, 68, 25, 52, 90, 80]
            # avg = sum(numbers)/len(numbers)
            # print(avg)