
import unittest
import os
from random import randint
from appium import webdriver
from time import sleep
from selenium.webdriver.common.touch_actions import TouchActions

class SimpleIOSTests(unittest.TestCase):

    def setUp(self):
        # set up appium
        app = os.path.abspath('/Users/dekel/Desktop/Troll.app')
        self.driver = webdriver.Remote(
                command_executor='http://127.0.0.1:4723/wd/hub',
                desired_capabilities={
                    'app': app,
                    'platformName': 'iOS',
                    'platformVersion': '9.2',
                    'deviceName': 'iPhone 6'
                })

    def tearDown(self):
        self.driver.quit()


    def test_ui_computation(self):


        self.driver.swipe(200,200,-200,200,5000)
        first_join_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]').is_displayed()
        print(first_join_text)
        self.driver.swipe(200,200,-200,200,5000)
        second_join_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]').is_displayed()
        print(second_join_text)

        self.driver.find_element_by_id('Get Started').click()
        sleep(2)
        self.driver.background_app(1)

        # Print groups and topics and checking if any button is selected on Join #
        i = 1
        y = 1
        while i < 14:
            xpath = '//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableGroup[%d]/UIAStaticText[1]' % i
            group = self.driver.find_element_by_xpath(xpath).text
            print group
            i +=  1
            z = 1
            while z < 3:
                subXpath = '//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[1]' % y
                sub_group = self.driver.find_element_by_xpath(subXpath).text
                print sub_group
                selec = '//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[2]' % y
                button = self.driver.find_element_by_xpath(selec).is_selected()
                print button
                y += 1
                z += 1



        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIASearchBar[1]/UIASearchBar[1]').send_keys('obama') # Join Search Obama #
        sleep(2)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[2]/UIATableCell[1]/UIAStaticText[2]').click() # tap on button follow for Obama #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[2]').click() # Tap on Cancel #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableGroup[1]/UIAButton[1]').click() # tap on See all trending #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[2]').click() # Follow on topic #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]').click() # Tap on back #


        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]/UIAStaticText[2]').click() # Join-select topic #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[4]/UIAStaticText[2]').click() # Join-select topic #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[5]/UIAStaticText[2]').click() # Join-select topic #
        self.driver.find_element_by_id('NEXT').click()
        sleep(2)
        self.driver.find_element_by_id('OK').click()
        sleep(5)
        self.driver.swipe(50,300,50,-300,2000) # Swipe Up after join #
        sleep(15)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[4]').click() # Tap on back #
        intre = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAStaticText[1]').is_displayed() # checking if interests title text is displayed on following page #
        print intre

        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIASegmentedControl[1]/UIAButton[2]').click() #Tap on Saved #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIASegmentedControl[1]/UIAButton[1]').click() #Tap on Following #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAScrollView[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[3]').click() # Unfollow topic #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[3]').click() # Swipe to time line #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[4]').click() # Tap on back #

        #        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[3]').click() # Add to saved #
        #        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[2]').click() # page back #
        #        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[2]').click() # back to Following page #
        #        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIASegmentedControl[1]/UIAButton[2]').click() #Tap on Saved #
        #        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIASegmentedControl[1]/UIAButton[1]').click() #Tap on Following #

        # Settings #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[1]').click() # Tap on settings #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIASwitch[1]').click() # Enable Push notifications #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIASwitch[1]').click() # Disable Push notifications #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIASwitch[1]').click() # Enable Push notifications #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIAStaticText[1]').click() # Tap on Tell friend #
        # self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAActionSheet[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]').click() # Tap on Mail #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAActionSheet[1]/UIAButton[1]').click() # Tap on Cancel #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]/UIAStaticText[1]').click() # Tap on Clear Search History #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[1]').click() # Tap on Yes to clear history #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[4]/UIAStaticText[1]').click() # Tap on Terms of Use #
        term = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]').is_displayed() # Checking if terms texts displayed #
        print term
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]').click() # Back from Terms of Use #

        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[5]/UIAStaticText[1]').click() # Tap on Privacy policy #
        privacy = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAWebView[1]').is_displayed()
        print privacy
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]').click() # Back from Privacy policy #

        #        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[6]/UIAStaticText[1]').click() # Tap on Contact Support #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]').click() # Close Settings #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[3]').click() # navigate to time line page #
        self.driver.swipe(50,300,50,-300,2000) # Scroll up time line page #
        self.driver.swipe(50,300,50,-300,2000) # Scroll up time line page #
        self.driver.swipe(50,300,50,-300,2000) # Scroll up time line page #
        self.driver.swipe(50,300,50,-300,2000) # Scroll up time line page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[5]').click() # Navigate to Discover page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIATableView[1]/UIATableCell[6]/UIAStaticText[2]').click() # Added a new topic from discover page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[1]').click() # Navigate to time line page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[3]').click() # Tap on New stories #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]').click() # Tap on image- reader is opened #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[3]').click() # Add article to saved #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[5]').click() # Tap on more option button in article page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[4]').click() # Tap on open in browser #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[2]').click() # Tap on Done to close the web #
        self.driver.swipe(50,300,50,-300,2000) # Scroll up in article page #
        self.driver.swipe(50,300,50,-300,2000) # Scroll up in article page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[2]').click() # Tap on back- Navigate to time line page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[4]').click() # Navigate to following page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIASegmentedControl[1]/UIAButton[2]').click() # enter to saved page #
        save = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAScrollView[1]/UIATableView[2]/UIATableCell[1]/UIAStaticText[1]').is_displayed() # Checking if displayed #
        print save  # Print if article displayed in saved page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIANavigationBar[1]/UIAButton[3]').click() # Navigate to Time Line page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[5]').click() # Navigate to Discover page #

        # Discover page #

        # print groups and topics and checking if any button is selected on Dicover page#
        i = 1
        y = 1
        while i < 14:
            xpath = '//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIATableView[1]/UIATableGroup[%d]/UIAStaticText[1]' % i
            group = self.driver.find_element_by_xpath(xpath).text
            print group
            i +=  1
            z = 1
            while z < 4:
                subXpath = '//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[1]' % y
                sub_group = self.driver.find_element_by_xpath(subXpath).text
                print sub_group
                selec = '//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[2]' % y
                button = self.driver.find_element_by_xpath(selec).is_selected()
                print button
                y += 1
                z += 1






        sleep(5)








#if __name__ == '__main__':
#    suite = unittest.TestLoader().loadTestsFromTestCase(SimpleIOSTests)
#    unittest.TextTestRunner(verbosity=2).run(suite)
