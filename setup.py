from setuptools import setup, find_packages

requires = [
    'selenium',
    'requests',
    'gspread',
    'oauth2client',
    'boto',
    'boto==2.42.0',
    'gevent==1.2.1',
    'greenlet==0.4.11',
    'grequests==0.3.0',
    'gspread==0.4.1',
    'httplib2==0.9.2',
    'oauth2client==3.0.0',
    'pyasn1==0.1.9',
    'pyasn1-modules==0.0.8',
    'requests==2.11.1',
    'rsa==3.4.2',
    'selenium==2.53.6',
    'six==1.10.0',


]

# tests_requires = ['WebTest', 'mock==1.3.0']
# tests_requires.extend(requires)

setup(name='Automation',
      version='0.1',
      description='Automation tools for shellanoo',
      long_description='',
      classifiers=[
          "Programming Language :: Python",
          "Framework :: Selenium",
      ],
      author='dekel',
      author_email='',
      url='',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      # tests_require=tests_requires,
      )
