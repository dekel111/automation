import os
from time import sleep
import unittest
from appium import webdriver
import os.path
import logging
from datetime import datetime, date
from selenium.common.exceptions import NoSuchElementException
from helper_functions import write_to_log_file

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class SimpleAndroidTests(unittest.TestCase):
    def setUp(self):
        logging.basicConfig(level=logging.DEBUG)
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1'
        desired_caps['deviceName'] = 'Nexus S API 22'
        desired_caps['app'] = PATH(
            '../../../Desktop/Android_2.1.5_(23)_2679240.apk'
        )
        desired_caps["newCommandTimeout"] = 500
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        self.driver.quit()

    def write_to_log_file_failed(self, log_record):
        path = "../screenshots/ginseng/android/"

        if not os.path.exists(path):
            os.makedirs(path)

            path += "/screenshot.txt"

            # if not os.path.isfile(path):
            #     open(path, 'w').close()

        with open(path, "a") as log_file:
            log_file.write(log_record + "\n")

    def test_find_elements(self):
        logging.info('Start Sanity')
        sleep(2)

        # def onbording(self):
        #     on_bording_first_page = self.driver.find_elements_by_class_name('android.widget.ImageView')
        #     for onb_1 in range(len(on_bording_first_page)):
        #         if on_bording_first_page[onb_1].is_displayed() is False:
        #             print('image is missing on first page')
        #
        #     lets_start_button = self.driver.find_element_by_id('com.pepper.pay:id/intro_log_in_button')
        #     if lets_start_button.is_displayed() is False:
        #         print('button lets start is missing')
        #     lets_start_button.click()
        #     sleep(2)
        #
        #     # insert phone number and password #
        #     phone_number_text_header = self.driver.find_element_by_class_name('android.widget.TextView')
        #     if phone_number_text_header.is_displayed() is False:
        #         print('The text מספר הנייד ישמש לזיהוי וקבלת תשלום מאחרים is missing')
        #     phone_number = self.driver.find_element_by_class_name('android.widget.EditText')
        #     phone_number.send_keys('0547515457')
        #
        #     continue_button = self.driver.find_element_by_class_name('android.widget.Button')
        #     if continue_button.is_displayed() is False:
        #         print('button המשך is missing')
        #     continue_button.click()
        #
        #     password = self.driver.find_element_by_class_name('android.widget.EditText')
        #     password.send_keys('123456')
        #
        #     continue_button_2 = self.driver.find_element_by_id('com.pepper.pay:id/phone_validation_cta_button')
        #     continue_button_2.click()

        # self.onbording()

        # Close page of money #
        # self.driver.find_element_by_id('com.pepper.pay:id/gift_icon').click()

        # History page #
        try:
            history_button = self.driver.find_element_by_class_name('android.widget.TextView')
            if history_button.is_displayed() is False:
                logging.error("history button is missing")
                failed = "time:{time}".format(
                    time=datetime.now().strftime(
                        '%Y-%m-%d %H:%M, History button is missing,'))
                write_to_log_file('pepper', failed)
            history_button.click()
            sleep(4)
            history_list_1 = self.driver.find_elements_by_class_name('android.widget.LinearLayout')[0]
            if history_list_1.is_displayed() is False:
                print('History list is missing')
            history_list_name = {"name": "com.pepper.pay:id/conversation_item_user_initials_image_view",
                                 "image_name": "com.pepper.pay:id/conversation_item_name_text_view"}
            for history in history_list_name:
                if self.driver.find_element_by_id(history_list_name[history]).is_displayed is False:
                    print('pepper- ' + history + ' text is missing ')
                    failed = "time:{time}".format(
                        time=datetime.now().strftime(
                            '%Y-%m-%d %H:%M, name and image are missing in history list,'))
                    write_to_log_file('pepper', failed)
            sleep(2)
            # Click on first user in list #
            self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_name_text_view').click()

            history_conversation = self.driver.find_element_by_id('com.pepper.pay:id/history_recycler_view')
            if history_conversation.is_displayed() is False:
                print('History conversation is missing')
            sleep(3)
        except BaseException as e:
            logging.error('something is wrong in History first sender: %s', e)
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, something is wrong in History first sender'))
            write_to_log_file('pepper', failed)

        self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Back to history list #
        sleep(3)
        self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close history #

        sleep(4)

        # Ask for money #
        try:
            self.driver.find_element_by_id('com.pepper.pay:id/home_one_button').click()  # Tap on 1 shekel #
            self.driver.find_element_by_id('com.pepper.pay:id/home_request_pay_button').click()  # Ask for money #
            sleep(1)
            contact_list_length = self.driver.find_elements_by_class_name('android.widget.LinearLayout')
            for contact in range(len(contact_list_length)):
                contact_list = self.driver.find_elements_by_class_name('android.widget.LinearLayout')[contact]
                if contact_list.is_displayed() is False:
                    print('Contact list is missing')
            self.driver.find_element_by_id(
                'com.pepper.pay:id/contact_item_text_view').click()  # Click on first contact #
            sleep(2)
            add_text = self.driver.find_element_by_id('com.pepper.pay:id/transfer_details_message_edit_text')
            add_text.send_keys('hi dude')
            sleep(2)
            self.driver.hide_keyboard()
            add_mage = self.driver.find_element_by_id('com.pepper.pay:id/gif')
            add_mage.click()
            sleep(2)
            ask_money_button = self.driver.find_element_by_id('com.pepper.pay:id/transfer_details_continue_button')
            ask_money_button.click()
            sleep(3)
            self.driver.find_element_by_id(
                'com.pepper.pay:id/acknowledge_transfer_close_button').click()  # Close page #
        except BaseException as e:
            logging.error('Ask for money is failed first sender: %s', e)
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Ask for money is failed first sender'))
            write_to_log_file('pepper', failed)

        # Check if ask for money is displayed in history #
        try:
            sleep(3)
            history_button = self.driver.find_element_by_class_name('android.widget.TextView')
            history_button.click()
            sleep(5)
            self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_name_text_view').click()
            sleep(3)
            history_displayed = self.driver.find_element_by_id(
                'com.pepper.pay:id/history_item_request_out_linear_layout')
            if history_displayed.is_displayed() is False:
                print('Ask for money is not display in history')
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Back to history list #
            sleep(1)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close history #
            sleep(4)
        except BaseException as e:
            logging.error('Failed-ask for money is NOT displayed in history first sender: %s', e)
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Failed-ask for money is NOT displayed in history first sender'))
            write_to_log_file('pepper', failed)

        # Pay money #
        try:
            self.driver.find_element_by_id('com.pepper.pay:id/home_one_button').click()  # Tap on 1 shekel #
            self.driver.find_element_by_id('com.pepper.pay:id/home_pay_button').click()  # Tap on continue #
            sleep(2)
            contact_list_length = self.driver.find_elements_by_class_name('android.widget.LinearLayout')
            for contact in range(len(contact_list_length)):
                contact_list = self.driver.find_elements_by_class_name('android.widget.LinearLayout')[contact]
                if contact_list.is_displayed() is False:
                    print('Contact list is missing')
            self.driver.find_element_by_id(
                'com.pepper.pay:id/contact_item_text_view').click()  # Click on first contact #
            sleep(2)
            self.driver.find_element_by_id('com.pepper.pay:id/gif').click()  # Click on image #
            self.driver.find_element_by_id(
                'com.pepper.pay:id/transfer_details_continue_button').click()  # Click on pay button #
            sleep(2)
            insert_password = self.driver.find_element_by_id('com.pepper.pay:id/clearable_text_input_number_edit_text')
            insert_password.send_keys('123456')
            pay_continue_button = self.driver.find_element_by_id(
                'com.pepper.pay:id/password_authentication_container_cta_button')
            pay_continue_button.click()
            sleep(5)
            ft = self.driver.find_element_by_id(
                'com.pepper.pay:id/acknowledge_transfer_reference_number_text_view').text
            print(ft)
            sleep(3)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close payment page #
            sleep(3)
        except BaseException as e:
            logging.error('Failed Pay money first sender: %s', e)
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Failed Pay money first sender'))
            write_to_log_file('pepper', failed)

        try:
            history_button = self.driver.find_element_by_class_name('android.widget.TextView')
            history_button.click()
            sleep(4)
            self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_name_text_view').click()
            sleep(4)
            history_check = self.driver.find_element_by_id('com.pepper.pay:id/history_item_value')
            if history_check.is_displayed() is False:
                print('Pay in history is not display')
            sleep(2)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Back to history list #
            sleep(3)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close history #
            sleep(3)
        except BaseException as e:
            logging.error('Failed-Pay money is NOT displayed in history first sender: %s', e)
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Failed-Pay money is NOT displayed in history first sender'))
            write_to_log_file('pepper', failed)

        # Pay money for cancel #
        try:
            self.driver.find_element_by_id('com.pepper.pay:id/home_one_button').click()  # Tap on 1 shekel #
            self.driver.find_element_by_id('com.pepper.pay:id/home_pay_button').click()  # Tap on continue #
            sleep(2)
            self.driver.find_element_by_id(
                'com.pepper.pay:id/contact_item_text_view').click()  # Click on first contact #
            sleep(2)
            self.driver.find_element_by_id('com.pepper.pay:id/gif').click()  # Click on image #
            self.driver.find_element_by_id(
                'com.pepper.pay:id/transfer_details_continue_button').click()  # Click on pay button #
            sleep(2)
            insert_password = self.driver.find_element_by_id('com.pepper.pay:id/clearable_text_input_number_edit_text')
            insert_password.send_keys('123456')
            pay_continue_button = self.driver.find_element_by_id(
                'com.pepper.pay:id/password_authentication_container_cta_button')
            pay_continue_button.click()
            sleep(4)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close payment page #
            sleep(10)
        except BaseException as e:
            logging.error('Failed Second Pay money first sender: %s', e)
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Failed Second Pay money first sender'))
            write_to_log_file('pepper', failed)

        # Settings #
        try:
            sleep(3)
            setting_image = self.driver.find_element_by_class_name('android.widget.ImageButton')
            setting_image.click()
            sleep(2)
            customer_name = self.driver.find_element_by_id('com.pepper.pay:id/drawer_name_text_view')
            if customer_name.is_displayed() is None:
                print('customer name is missing in settings')

            settings_images = {"contact_us": "android.widget.Button",
                               "questions_answers": "android.widget.Button",
                               "invite_freinds": "android.widget.Button"}
            for setting in settings_images:
                if self.driver.find_element_by_class_name(settings_images[setting]).is_displayed() is None:
                    print('pepper- ' + setting + ' settings image is missing')

            history_settings = self.driver.find_element_by_id('com.pepper.pay:id/drawer_history_button')
            if history_settings.is_displayed() is False:
                print('history in settings is missing')
            history_settings.click()
            sleep(4)
            history_list_in_settings = self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_layout')
            if history_list_in_settings.is_displayed() is False:
                print('History list in settings is missing')

            self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_name_text_view').click()
            sleep(4)
            history_conversation = self.driver.find_element_by_id('com.pepper.pay:id/history_recycler_view')
            if history_conversation.is_displayed() is False:
                print('History conversation is missing in settings')
            sleep(3)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Back to history list #
            sleep(1)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close History #
            sleep(2)

            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Open settings #
            sleep(2)
            settings_setting = self.driver.find_element_by_id('com.pepper.pay:id/drawer_settings_button')
            if settings_setting.is_displayed() is False:
                print('setting is missing in settings')
            settings_setting.click()

            settings_menu = {"payment_settings": "com.pepper.pay:id/settings_item_profile_layout",
                             "update_setting": "com.pepper.pay:id/settings_item_sms_notifications_layout",
                             "settings_item_pattern": "com.pepper.pay:id/settings_item_pattern_layout",
                             "disconnect": "com.pepper.pay:id/settings_item_disconnet"}
            for menu in settings_menu:
                if self.driver.find_element_by_id(settings_menu[menu]).is_displayed is False:
                    print('pepper- ' + menu + ' settings image is missing')
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close settings #

            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Open settings #
            about = self.driver.find_element_by_id('com.pepper.pay:id/drawer_about_button')
            if about.is_displayed() is False:
                print('About is missing in settings')
            about.click()
            sleep(2)

            about_menu = {"about_pepper_pay": "com.pepper.pay:id/about_about_text_view",
                          "term_of_use": "com.pepper.pay:id/about_terms_of_use_text_view",
                          "information_security": "com.pepper.pay:id/about_information_security_text_view",
                          "payment_guarantee": "com.pepper.pay:id/about_payment_guarantee_text_view"}
            for menu_about in about_menu:
                if self.driver.find_element_by_id(about_menu[menu_about]).is_displayed is False:
                    print('pepper- ' + menu_about + ' settings is missing')
                self.driver.find_element_by_id(about_menu[menu_about]).click()
                sleep(2)
                self.driver.find_element_by_class_name('android.widget.ImageButton').click()
                sleep(2)
            sleep(5)

        except BaseException as e:
            logging.error('Something is wrong in settings: %s', e)
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Something is wrong in settings'))
            write_to_log_file('pepper', failed)
            print('Something is wrong in settings')
    def test_bla(self):
        assert (True)
