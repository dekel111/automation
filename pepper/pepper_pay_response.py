import os
from time import sleep
import unittest
from appium import webdriver
import os.path
import logging
from datetime import datetime, date
from helper_functions import write_to_log_file
from selenium.common.exceptions import NoSuchElementException

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class SimpleAndroidTests(unittest.TestCase):
    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1'
        desired_caps['deviceName'] = 'Pixel API 22'
        desired_caps['app'] = PATH(
            '../../../Desktop/Android_2.1.5_(23)_2679240.apk'
        )
        desired_caps["newCommandTimeout"] = 500
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        self.driver.quit()

    def write_to_log_file_failed(self, log_record):
        path = "../screenshots/ginseng/android/"

        if not os.path.exists(path):
            os.makedirs(path)

            path += "/screenshot.txt"

            # if not os.path.isfile(path):
            #     open(path, 'w').close()

        with open(path, "a") as log_file:
            log_file.write(log_record + "\n")

    def test_find_elements(self):
        logger = logging.getLogger('log')
        logger.info('Start Sanity')
        sleep(4)

        try:
            history_button = self.driver.find_element_by_class_name('android.widget.TextView')
            if history_button.is_displayed() is False:
                print('history button is missing')
            history_button.click()
            sleep(5)
            history_list_1 = self.driver.find_elements_by_class_name('android.widget.LinearLayout')[0]
            if history_list_1.is_displayed() is False:
                print('History list is missing')
            history_list_name = {"name": "com.pepper.pay:id/conversation_item_user_initials_image_view",
                                 "image_name": "com.pepper.pay:id/conversation_item_name_text_view"}
            for history in history_list_name:
                if self.driver.find_element_by_id(history_list_name[history]).is_displayed is False:
                    print('MM Android- ' + history + ' text is missing ')
            sleep(4)
            # Click on first user in list #
            self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_name_text_view').click()
            sleep(3)
        except:
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, something is wrong while enter to History received'))
            write_to_log_file('pepper', failed)

        # Cancel money #
        # try:
        #     ask_money_display = self.driver.find_element_by_id('com.pepper.pay:id/history_item_in_linear_layout')
        #     sleep(1)
        #     if ask_money_display.is_displayed() is False:
        #         print('ask money is not display in history')
        #     sleep(2)
        #     self.driver.find_element_by_id('com.pepper.pay:id/history_item_gif').click()  # Click on image #
        #     sleep(2)
        #     cancel_money = self.driver.find_element_by_id('com.pepper.pay:id/transaction_info_negative_action_button')
        #     sleep(1)
        #     if cancel_money.is_displayed() is False:
        #         print('דחיית תשלום is not displayed')
        #     cancel_money.click()  # click on כפתור דחיית תשלום #
        #     sleep(4)
        #     are_you_sure_cancel = self.driver.find_element_by_id('android:id/button1')
        #     sleep(1)
        #     if are_you_sure_cancel.is_displayed() is False:
        #         print('Are you sure you want to cancel the money')
        #     are_you_sure_cancel.click()
        #     sleep(4)
        #     canceled = self.driver.find_element_by_id('com.pepper.pay:id/history_item_value')
        #     sleep(1)
        #     if canceled.text == 'דחיתי':
        #         sleep(2)
        #     else:
        #         print('לא בוצע דחיית תשלום')
        #     sleep(1)
        # except:
        #     failed = "time:{time}".format(
        #         time=datetime.now().strftime(
        #             '%Y-%m-%d %H:%M, Cancel payment is failed received device'))
        #     write_to_log_file('pepper', failed)

        # Checking payment in history #
        self.driver.swipe(200, 400, 200, 800, 5000)
        self.driver.swipe(200, 400, 200, 800, 5000)

        sleep(2)

        accepet_money = self.driver.find_element_by_id('com.pepper.pay:id/history_item_in_linear_layout')
        if accepet_money.is_displayed() is False:
            print('Accept money is not display in history')

        accepet_money_text = self.driver.find_element_by_id('com.pepper.pay:id/history_item_value')
        if accepet_money_text.is_displayed() is False:
            print('קיבלתי 1 שקל is missing')

        self.driver.swipe(200, 400, 200, 800, 5000)
        self.driver.swipe(200, 400, 200, 800, 5000)
        self.driver.swipe(200, 400, 200, 800, 5000)

        # Ask money check #
        ask_money = self.driver.find_element_by_id('com.pepper.pay:id/history_item_value')
        sleep(1)
        if ask_money.is_displayed():
            if ask_money.text == 'ביקשו ממך  ₪1':
                sleep(1)
            else:
                print('ביקשו ממך  שקל is missing 1')
        else:
            print('ask money is missing in history')
        text_message = self.driver.find_element_by_id('com.pepper.pay:id/history_item_message')
        if text_message.is_displayed():
            if text_message.text == 'hi dude':
                sleep(2)
            else:
                print('text message that the user assign is NOT equal "hi dude"')
        else:
            print('text message that the user assign is missing "hi dude"')

        image = self.driver.find_element_by_id('com.pepper.pay:id/history_item_gif')
        sleep(1)
        if image.is_displayed() is False:
            print('image gif is missing on ask money in history')
        sleep(4)

        try:
            pay_button = self.driver.find_element_by_id('com.pepper.pay:id/history_item_more_title')
            sleep(1)
            if pay_button.is_displayed() is False:
                print('button is missing שלם')
            pay_button.click()
            sleep(3)

            pay_button_page = self.driver.find_element_by_id('com.pepper.pay:id/transfer_details_continue_button')
            sleep(1)
            pay_button_page.click()
            sleep(3)
            password = self.driver.find_element_by_id('com.pepper.pay:id/clearable_text_input_number_edit_text')
            password.send_keys('042082')
            sleep(3)
            continue_button = self.driver.find_element_by_id('com.pepper.pay:id/password_authentication_container_cta_button')
            continue_button.click()
            sleep(4)
            close_page = self.driver.find_element_by_id('com.pepper.pay:id/acknowledge_transfer_close_button')
            sleep(3)
            close_page.click()
            sleep(3)
        except:
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, pay button is failed received device'))
            write_to_log_file('pepper', failed)

        # send and ask money to other device #

        # Ask for money #
        try:
            self.driver.find_element_by_id('com.pepper.pay:id/home_one_button').click()  # Tap on 1 shekel #
            self.driver.find_element_by_id('com.pepper.pay:id/home_request_pay_button').click()  # Ask for money #
            sleep(1)
            contact_list_length = self.driver.find_elements_by_class_name('android.widget.LinearLayout')
            sleep(1)
            for contact in range(len(contact_list_length)):
                contact_list = self.driver.find_elements_by_class_name('android.widget.LinearLayout')[contact]
                if contact_list.is_displayed() is False:
                    print('Contact list is missing')
            self.driver.find_element_by_id('com.pepper.pay:id/recent_contact_item_text_view').click()  # Click on first contact #
            sleep(2)
            add_text = self.driver.find_element_by_id('com.pepper.pay:id/transfer_details_message_edit_text')
            add_text.send_keys('hi send back')
            sleep(2)
            self.driver.hide_keyboard()
            sleep(1)
            add_image = self.driver.find_element_by_id('com.pepper.pay:id/gif')
            add_image.click()
            sleep(2)
            ask_money_button = self.driver.find_element_by_id('com.pepper.pay:id/transfer_details_continue_button')
            ask_money_button.click()
            sleep(4)
            self.driver.find_element_by_id('com.pepper.pay:id/acknowledge_transfer_close_button').click()  # Close page #
        except:
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Ask money is failed received device to first device'))
            write_to_log_file('pepper', failed)

        # Check if ask for money is displayed in history #
        try:
            sleep(4)
            history_button = self.driver.find_element_by_class_name('android.widget.TextView')
            history_button.click()
            sleep(4)
            self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_name_text_view').click()
            sleep(4)
            history_displayed = self.driver.find_element_by_id('com.pepper.pay:id/history_item_request_out_linear_layout')
            if history_displayed.is_displayed() is False:
                print('Ask for money is not display in history')
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Back to history list #
            sleep(2)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close history #
            sleep(4)
        except:
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Ask money is NOT display in received device to first device'))
            write_to_log_file('pepper', failed)

        # Pay money #

        try:
            self.driver.find_element_by_id('com.pepper.pay:id/home_one_button').click()  # Tap on 1 shekel #
            self.driver.find_element_by_id('com.pepper.pay:id/home_pay_button').click()  # Tap on continue #
            sleep(2)
            contact_list_length = self.driver.find_elements_by_class_name('android.widget.LinearLayout')
            for contact in range(len(contact_list_length)):
                contact_list = self.driver.find_elements_by_class_name('android.widget.LinearLayout')[contact]
                if contact_list.is_displayed() is False:
                    print('Contact list is missing')
            self.driver.find_element_by_id('com.pepper.pay:id/recent_contact_item_text_view').click()  # Click on first contact #
            sleep(2)
            self.driver.find_element_by_id('com.pepper.pay:id/gif').click()  # Click on image #
            self.driver.find_element_by_id('com.pepper.pay:id/transfer_details_continue_button').click()  # Click on pay button #
            sleep(2)
            insert_password = self.driver.find_element_by_id('com.pepper.pay:id/clearable_text_input_number_edit_text')
            insert_password.send_keys('042082')
            pay_continue_button = self.driver.find_element_by_id('com.pepper.pay:id/password_authentication_container_cta_button')
            pay_continue_button.click()
            sleep(4)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close payment page #
            sleep(3)
        except:
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Pay money is failed received device to first device'))
            write_to_log_file('pepper', failed)

        try:
            history_button = self.driver.find_element_by_class_name('android.widget.TextView')
            history_button.click()
            sleep(3)
            self.driver.find_element_by_id('com.pepper.pay:id/conversation_item_name_text_view').click()
            sleep(3)
            history_check = self.driver.find_element_by_id('com.pepper.pay:id/history_item_value')
            if history_check.is_displayed() is False:
                print('Pay in history is not display')
            sleep(2)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Back to history list #
            sleep(3)
            self.driver.find_element_by_class_name('android.widget.ImageButton').click()  # Close history #
            sleep(3)
        except:
            failed = "time:{time}".format(
                time=datetime.now().strftime(
                    '%Y-%m-%d %H:%M, Pay money is NOT display in received device to first device'))
            write_to_log_file('pepper', failed)

        sleep(10)









