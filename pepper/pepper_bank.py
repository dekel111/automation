import os
from time import sleep
import unittest
from appium import webdriver
import os.path
import logging
from datetime import datetime, date
from selenium.common.exceptions import NoSuchElementException

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class SimpleAndroidTests(unittest.TestCase):
    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1'
        desired_caps['deviceName'] = 'Nexus S API 22'
        desired_caps['app'] = PATH(
            '../../../Desktop/digital-x86-debug.apk'
        )
        desired_caps["newCommandTimeout"] = 500
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        self.driver.quit()

    def test_find_elements(self):
        # Tutorial #
        logger = logging.getLogger('log')
        logger.info('Start Sanity')
        sleep(2)

        on_bording_text_1 = self.driver.find_elements_by_class_name('android.widget.TextView')
        for onb_1 in range(len(on_bording_text_1)):
            if on_bording_text_1[onb_1].is_displayed() is False:
                print('Text is missing in first page')

        self.driver.swipe(50, 100, 800, 100, 5000)
        sleep(3)

        on_bording_text_2 = self.driver.find_elements_by_class_name('android.widget.TextView')
        for onb_2 in range(len(on_bording_text_2)):
            if on_bording_text_1[onb_2].is_displayed() is False:
                print('Text is missing in Second page')

        self.driver.swipe(50, 100, 800, 100, 5000)
        sleep(3)

        on_bording_text_3 = self.driver.find_elements_by_class_name('android.widget.TextView')
        for onb_3 in range(len(on_bording_text_3)):
            if on_bording_text_1[onb_3].is_displayed() is False:
                print('Text is missing in Third page')

        self.driver.swipe(50, 100, 800, 100, 5000)
        sleep(3)

        on_bording_text_4 = self.driver.find_elements_by_class_name('android.widget.TextView')
        for onb_4 in range(len(on_bording_text_4)):
            if on_bording_text_1[onb_4].is_displayed() is False:
                print('Text is missing in Third page')

        sleep(3)

        # Click on i have account #
        have_account_button = self.driver.find_element_by_id('com.pepper.ldb:id/intro_log_in_button')
        if have_account_button.is_displayed() is False:
            print('Have account button is missing')
        have_account_button.click()
        sleep(5)
        # Login with phone number #
        phone_number = self.driver.find_element_by_id('com.pepper.ldb:id/clearable_text_input_number_edit_text')
        phone_number.send_keys('0547515457')

        password = self.driver.find_element_by_id('com.pepper.ldb:id/login_password_password_edit_text')
        password.send_keys('707770')

        continue_1 = self.driver.find_element_by_id('com.pepper.ldb:id/login_password_cta')
        continue_1.click()

        sleep(20)

        sleep(5)

        sleep(10)

        logger.info('Finish Sanity MM Android Successful')

        #
        # # Scroll UP Trending page #
        # self.driver.swipe(50, 800, 50, 100, 5000)
        # self.driver.swipe(50, 800, 50, 100, 5000)

        # scroll page Down #
        # self.driver.swipe(200, 400, 200, 800, 5000)
