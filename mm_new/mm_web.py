import logging
import sys
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from helper_functions import write_to_log_file
from selenium.common.exceptions import TimeoutException
import requests
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime, date
from random import randint
from selenium.webdriver.support.ui import Select
import boto.ses
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


_log = logging.getLogger(__name__)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def local_storage_uid():
    global user_id_from_local
    if user_id_from_local == driver.execute_script("return localStorage['uid']"):
        print('local storage UID is OK ')
        print(user_id_from_local)
    else:
        uid_changed = driver.execute_script("return localStorage['uid']")
        print('local storage UID is changed')
        print(uid_changed)
        # text = 'Landing Ginseng page 3 to 5 - Local storage UID is changed'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)


def sign_in():
    # Sign in #
    time.sleep(18)
    sign_in = driver.find_element_by_class_name('my-btn--outline')
    if sign_in.is_displayed() is False:
        print('MM WEB Production- Sign in button is missing')
        now = datetime.now().strftime('MM WEB Production- Sign in button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
    else:
        sign_in.click()
    time.sleep(1)
    driver.find_elements_by_class_name('form-control')[0].send_keys('dekela@shellanoo.com')
    password = driver.find_elements_by_class_name('form-control')[1]
    password.send_keys('123456')
    sign_in_pop_up = driver.find_elements_by_class_name('my-btn')[4]
    if sign_in_pop_up.is_displayed():
        sign_in_pop_up.click()
    else:
        now = datetime.now().strftime('MM WEB Production- Sign in pop up window button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
    time.sleep(10)


def start_sanity_chrome():
    global driver
    global user_id_from_local
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get(site)
    # driver.set_window_size(1750, 1750)
    # user_id_from_local = driver.execute_script("return localStorage['uid']")

    sign_in()  # Call to func sign in #

    # Sign out #
    element_to_hover_over = driver.find_element_by_css_selector(".user-block__name")
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()
    sign_out = driver.find_elements_by_css_selector('.user-block-menu a')[2]
    sign_out.click()
    time.sleep(2)

    sign_in()  # Call to func sign in #

    # Trending #
    i = 1
    while i < 4:
        element_to_hover_over = driver.find_element_by_id("single-button")
        hover = ActionChains(driver).move_to_element(element_to_hover_over)
        hover.click()
        hover.perform()
        time.sleep(2)
        trending_menu = driver.find_elements_by_css_selector('.dropdown-item')[i]
        trending_menu.click()
        time.sleep(2)
        content = driver.find_element_by_class_name('largeTrackList')
        if content.is_displayed() is False:
            print(trending_menu.text)
            print('MM WEB Production- content is missing')
            now = datetime.now().strftime('MM WEB Production- Content trending menu is missing %Y-%m-%d %H:%M')
            driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        button = driver.find_elements_by_class_name('play-btn')[0]
        driver.execute_script("(arguments[0]).click();", button)
        time.sleep(10)
        button = driver.find_elements_by_class_name('play-btn')[0]
        time.sleep(2)
        driver.execute_script("(arguments[0]).click();", button)
        driver.find_element_by_class_name('playing-item-li__performer').click()
        time.sleep(2)
        driver.find_element_by_class_name('track-list-li__image').click()
        time.sleep(10)
        driver.back()
        i += 1
        time.sleep(2)

    time.sleep(15)

    # Add to Playlist #
    driver.find_elements_by_css_selector('.mm-ico-menu')[20].click()
    time.sleep(2)
    driver.find_element_by_css_selector('.open .dropdown-menu a').click()
    time.sleep(2)
    driver.find_element_by_class_name('playlist-tab__name').click()
    time.sleep(15)

    # Follow #
    driver.find_element_by_class_name('playing-item-li__performer').click()
    time.sleep(2)
    follow_button = driver.find_elements_by_css_selector('.my-btn')[4]
    if follow_button.is_displayed():
        follow_button.click()
    else:
        print('MM WEB Production- Follow button is missing')
        now = datetime.now().strftime('MM WEB Production- Follow button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
    time.sleep(5)
    driver.back()
    time.sleep(5)
    element_to_hover_over = driver.find_element_by_id("single-button")
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.click()
    hover.perform()
    time.sleep(2)
    trending_menu = driver.find_elements_by_css_selector('.dropdown-item')[4]
    trending_menu.click()
    time.sleep(2)
    content_follow = driver.find_element_by_class_name('largeTrackList')
    if content_follow.is_displayed() is False:
        print('MM WEB Production- Following content is missing')
        now = datetime.now().strftime('MM WEB Production- Following content is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

    # Unfollow #
    driver.find_element_by_class_name('playing-item-li__performer').click()
    time.sleep(4)
    driver.find_elements_by_css_selector('.my-btn')[4].click()
    time.sleep(4)
    driver.back()
    time.sleep(5)
    element_to_hover_over = driver.find_element_by_id("single-button")
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.click()
    hover.perform()
    time.sleep(2)
    trending_menu = driver.find_elements_by_css_selector('.dropdown-item')[4]
    trending_menu.click()
    time.sleep(5)
    not_follow_text = driver.find_element_by_css_selector('.following-block__text-f')
    if not_follow_text.is_displayed():
        time.sleep(1)
    else:
        print('MM WEB Production- You are not following anyone yet, is missing')

    but_when_you = driver.find_element_by_css_selector('.following-block__text_s')
    if but_when_you.is_displayed():
        time.sleep(1)
    else:
        print('MM WEB Production- but when you do tracks will appear here, is missing')
    time.sleep(5)

    # Upload file #
    upload_your_own = driver.find_element_by_css_selector('.header-wrap__upload')
    if upload_your_own.is_displayed():
        upload_your_own.click()
    else:
        print('MM WEB Production- Upload your own button is missing')
        now = datetime.now().strftime('MM WEB Production- Upload your own button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

    # Upload Audio file #
    time.sleep(3)
    driver.find_elements_by_css_selector('input[type="file"]')[0].send_keys(
        "/Users/automator/Desktop/Songs/dekel123.mp3")
    time.sleep(8)
    audio_file_name = driver.find_elements_by_css_selector('.edit-song-name')[0]
    if audio_file_name.is_displayed() is False:
        print('MM WEB Production- Audio file is not uploaded')
        now = datetime.now().strftime('MM WEB Production- Audio file is not uploaded %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

    driver.find_element_by_css_selector('.ng2-tag-input__text-input').send_keys('#pop')
    driver.find_element_by_css_selector('.ng2-menu-item').click()

    # Upload image file #
    # driver.find_element_by_css_selector('.upload-btn__text').click()
    time.sleep(2)
    driver.find_elements_by_css_selector('input[type="file"]')[1].send_keys("/Users/automator/Desktop/Songs/lana.jpeg")
    time.sleep(2)
    driver.find_elements_by_css_selector('.my-btn')[4].click()  # Click on Next #
    time.sleep(5)

    driver.find_elements_by_css_selector('.checkbox label')[0].click()
    time.sleep(2)
    driver.find_elements_by_css_selector('.checkbox label')[1].click()
    time.sleep(1)
    driver.find_elements_by_css_selector('.checkbox label')[2].click()
    time.sleep(1)
    driver.find_element_by_css_selector('.edit-song-footer__right .my-btn--yellow').click()  # Click on Next #
    time.sleep(2)
    driver.find_element_by_css_selector('.upload-publish-btn .my-btn--yellow').click()  # Click on Publish #
    time.sleep(15)
    driver.find_element_by_css_selector('.logotype').click()
    time.sleep(15)

    # Upload Playlist #
    upload_your_own = driver.find_elements_by_css_selector('.my-btn')[0]
    upload_your_own.click()
    time.sleep(5)
    driver.find_element_by_css_selector('.checkbox label').click()
    time.sleep(2)
    driver.find_elements_by_css_selector('input[type="file"]')[0].send_keys(
        "/Users/automator/Desktop/Songs/dekel123.mp3")
    time.sleep(5)
    driver.find_elements_by_css_selector('input[type="file"]')[0].send_keys("/Users/automator/Desktop/Songs/ram.mp3")
    time.sleep(5)
    driver.find_elements_by_css_selector('input[type="file"]')[0].send_keys(
        "/Users/automator/Desktop/Songs/Lana Del Rey - Love.mp3")
    time.sleep(5)
    driver.find_elements_by_css_selector('.form-control')[0].send_keys('playlist 3')
    time.sleep(2)
    driver.find_element_by_css_selector('.ng2-tag-input__text-input').send_keys('#Rock')
    time.sleep(2)
    driver.find_element_by_css_selector('.ng2-menu-item').click()
    time.sleep(3)
    driver.find_elements_by_css_selector('input[type="file"]')[1].send_keys("/Users/automator/Desktop/Songs/lana.jpeg")
    time.sleep(3)
    driver.find_elements_by_css_selector('.my-btn')[4].click()  # Click on Next #
    time.sleep(2)
    driver.find_elements_by_css_selector('.checkbox label')[0].click()
    time.sleep(1)
    driver.find_elements_by_css_selector('.checkbox label')[1].click()
    time.sleep(1)
    driver.find_elements_by_css_selector('.checkbox label')[2].click()
    time.sleep(1)
    driver.find_element_by_css_selector('.edit-song-footer__right .my-btn--yellow').click()  # Click on Next #
    time.sleep(2)
    driver.find_element_by_css_selector('.upload-publish-btn .my-btn--yellow').click()  # Click on Publish #
    time.sleep(20)

    # My profile #
    element_to_hover_over = driver.find_element_by_css_selector(".user-block__name")
    hover = ActionChains(driver).move_to_element(element_to_hover_over)
    hover.perform()
    my_profile = driver.find_elements_by_css_selector('.user-block-menu a')[1]
    my_profile.click()
    time.sleep(2)
    driver.find_elements_by_css_selector('.nav-link')[1].click()
    time.sleep(2)

    playlist_exist = driver.find_elements_by_css_selector('.playlist-tab__name')[2]
    time.sleep(1)
    if playlist_exist.is_displayed() is False:
        print('MM WEB Production- Uploaded Playlist is missing')
        now = datetime.now().strftime('MM WEB Production- Uploaded Playlist is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

    # Delete playlist #
    driver.find_elements_by_css_selector('.my-btn')[5].click()
    time.sleep(2)
    driver.find_elements_by_css_selector('.my-btn')[16].click()
    time.sleep(4)
    driver.find_elements_by_css_selector('.my-btn')[17].click()
    time.sleep(8)

    # Share to Facebook #
    driver.find_element_by_css_selector('.logotype').click()  # Click  on Logo #
    time.sleep(10)
    driver.find_elements_by_css_selector('.mm-ico-menu')[20].click()
    time.sleep(2)
    driver.find_elements_by_css_selector('.open .dropdown-menu a')[1].click()
    time.sleep(2)
    driver.find_elements_by_css_selector('.share-pop-up-social')[0].click()
    time.sleep(4)
    driver.switch_to.window(driver.window_handles[1])
    time.sleep(2)
    driver.find_element_by_id('email').send_keys('qa1@shellanoo.com')
    time.sleep(1)
    driver.find_element_by_id('pass').send_keys('shellanoo1$')
    time.sleep(1)
    driver.find_element_by_id('loginbutton').click()
    time.sleep(4)
    driver.find_element_by_id('u_0_1x').click()
    time.sleep(3)
    driver.switch_to.window(driver.window_handles[0])
    time.sleep(2)
    driver.find_elements_by_css_selector('.close')[3].click()
    time.sleep(5)

    # Share to Twitter #
    driver.find_elements_by_css_selector('.mm-ico-menu')[20].click()
    time.sleep(2)
    driver.find_elements_by_css_selector('.open .dropdown-menu a')[1].click()
    time.sleep(2)
    driver.find_elements_by_css_selector('.share-pop-up-social')[1].click()
    time.sleep(2)
    driver.switch_to.window(driver.window_handles[1])
    time.sleep(1)
    driver.find_element_by_id('username_or_email').send_keys('qa1@shellanoo.com')
    time.sleep(1)
    driver.find_element_by_id('password').send_keys('musicme1$')
    time.sleep(1)
    driver.find_element_by_class_name('button').click()
    time.sleep(3)
    driver.switch_to.window(driver.window_handles[0])
    time.sleep(3)
    driver.find_elements_by_css_selector('.close')[3].click()
    time.sleep(3)

    # Write to log file if success #
    success = datetime.now().strftime('%Y-%m-%d %H:%M, MM WEB production- successful')
    write_to_log_file('mm', success)
    print('Sanity Chrome MM WEB Production is done')


if __name__ == '__main__':

    sites = ["https://musicmessenger.co/"]

    for site in sites:
        try:
            logging.basicConfig(level=logging.INFO)
            logging.info('Starting automation on %s Site' % site)
            _log.info('Running sanity_chrome')
            # driver = None
            # is_run_firefox = True
            is_run_chrome = True

            # if is_run_firefox:
            #     start_sanity_firefox()
            #     is_run_firefox = False
            # driver.close()
            if is_run_chrome:
                start_sanity_chrome()
                is_run_chrome = False
        except BaseException as e:
            logging.error('Failed to do something: %s', e)
            import traceback
            traceback.print_exc()
            # post_to_slack("MM WEB- production failed")
            # post_to_opsgenie("Ginseng landing page 3 to 5 payment failed")
            # text = 'Ginseng landing page 3 to 5 payment failed'
            # recipients = ['dekela@shellanoo.com']
            # post_to_mail(recipients, text)
            traceback.print_exc()
            m_now = datetime.now().strftime('MM WEB Production- Failed %Y-%m-%d %H:%M')
            driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % m_now)

        _log.info('All done.')
        driver.close()
