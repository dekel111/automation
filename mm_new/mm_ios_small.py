import unittest
import os
from random import randint
from appium import webdriver
from time import sleep
import sys
import logging
import os.path
from selenium.webdriver.common.touch_actions import TouchActions
from helper_functions import write_to_log_file
from datetime import datetime, date
import boto.ses
import os.path
from selenium.common.exceptions import NoSuchElementException


_log = logging.getLogger(__name__)


class SimpleIOSTests(unittest.TestCase):

    def setUp(self):
        # set up appium
        app = os.path.abspath('/Users/automator/Desktop/MusicMessenger.app')
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4723/wd/hub',
            desired_capabilities={
                'app': app,
                'platformName': 'iOS',
                'platformVersion': '9.2',
                'deviceName': 'iPhone 6'
            })

    def tearDown(self):
        self.driver.quit()

    def tutorial(self):
        # Tutorial pages #

        first_join_text1 = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]')
        print(first_join_text1.is_displayed())
        print(first_join_text1.text)
        first_join_text2 = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[2]')
        print(first_join_text2.is_displayed())
        print(first_join_text2.text)
        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #

        second_join_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]')
        print(second_join_text.is_displayed())
        print(second_join_text.text)
        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #

        third_join_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]')
        print(third_join_text.is_displayed())
        print(third_join_text.text)
        let_start_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[1]')
        let_start_button.click()
        sleep(2)
        # self.driver.background_app(1)

    def test_ui_computation(self):

        try:
            try:
                self.tutorial()  # Tutorial methode #
            except NoSuchElementException as e:
                logging.error('Failed to do something: %s', e)
                now = datetime.now().strftime('MM IOS- Tutorial failed, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

                # Tap on Trending menu and check if content displayed #
            i = 1
            while i < 4:
                self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[3]').click()
                xpath = '//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[1]' % i
                group = self.driver.find_element_by_xpath(xpath)
                print(group.is_displayed())
                print(group.text)
                group.click()
                song_list = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
                if song_list is not None:
                    i += 1
                else:
                    print('songs is missing in timeline')
                self.driver.swipe(50, 300, 50, -50, 2000)  # Swipe page Up #
            sleep(2)

            # Move between Timeline/Search/Playlist #
            x = 1
            while x < 4:
                xpath = '//UIAApplication[1]/UIAWindow[1]/UIAButton[%d]' % x
                group_1 = self.driver.find_element_by_xpath(xpath)
                if group_1.is_displayed() is False:
                    print('icon pages Home/Search/Playlist are missing')
                group_1.click()
                x += 1
            self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click()  # Move to Trending page #
            sleep(2)

            # Trending - Checking song image #
            artist_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[1]')
            print(artist_name.is_displayed())
            print('Artist name:', artist_name.text)
            song_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]')
            print(song_name.is_displayed())
            print('Song name:', song_name.text)
            counter_play = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[3]')
            print(counter_play.is_displayed())
            print('Counter play:', counter_play.text)
            song_time = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[4]')
            print(song_time.is_displayed())
            print('Song time:', song_time.text)
            heart = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[2]')
            print(heart.is_displayed())
            three_dots_menu = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]').is_displayed()
            print(three_dots_menu)
            heart_number = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[5]')
            print(heart_number.is_displayed())
            print('Heart number before click:', heart_number.text)

            artist_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]')
            artist_button.click()    # Enter to artist page #
            sleep(2)
            self.driver.swipe(50, 300, 50, -300, 2000)  # Swipe page Up #
            self.driver.swipe(50, 300, 50, -300, 2000)  # Swipe page Up #
            back_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]')  # Back button #
            back_button.click()

            # Click on heart #
            heart_enabled = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[2]')
            heart_enabled.click()
            heart_after_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[5]')
            print('Heart number after click:', heart_after_click.text)
            heart_enabled.click()
            heart_before_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[5]')
            print('Heart number before click:', heart_before_click.text)

            # 3 dots menu #
            three_dots_menu_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]')
            three_dots_menu_button.click()
            add_to_playlist_1 = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]')
            print(add_to_playlist_1.is_displayed())
            print(add_to_playlist_1.text)
            add_to_playlist_1.click()
            sleep(2)

            # Add to playlist #
            add_to_playlist_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATextField[1]')  # Name for Playlist #
            sleep(2)
            add_to_playlist_name.send_keys('Develop Playlist')
            sleep(2)
            self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[5]').click()  # Done button #
            sleep(3)
            self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]').click() # Move to Playlist page#
            playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]').is_displayed()
            if playlist is not None:
                print(playlist)
            else:
                print('Playlist is missing')
            playlist_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]')
            if playlist_name is not None:
                print('Playlist name:', playlist_name.text)
            else:
                print('Playlist name is missing')

                # Search page #
            self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[2]').click() # Move to Search page #

            # Check if image songs ODD or EVEN #
            table_image_song_sum = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
            cells_images_sum = len(table_image_song_sum.find_elements_by_class_name('UIACollectionCell')) - 5
            if cells_images_sum % 2 != 0:
                print('Song images are ODD:', cells_images_sum)

            try:
                z = 1
                while z < 6:
                    table_categories = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
                    sub_table_categories = len(table_categories .find_elements_by_class_name('UIACollectionCell'))
                    if sub_table_categories != 0:
                        xpath = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]' % z
                        group_3 = self.driver.find_element_by_xpath(xpath)
                        print(group_3.text)
                        group_3.click()
                        title = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]')
                        print('Title page:', title.text)
                        list_view = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[3]')
                        list_view.click()
                        # self.driver.swipe(50, 300, 50, -300, 2000)  # Swipe page Up #
                        sleep(2)
                        tail_list_view = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[3]')
                        tail_list_view.click()
                        t = 1
                        while t < 2:
                            table = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
                            cells = len(table.find_elements_by_class_name('UIACollectionCell'))
                            if cells != 0:
                                xpath_3 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAStaticText[2]' % t
                                sleep(1)
                                group_4 = self.driver.find_element_by_xpath(xpath_3)
                                sleep(1)
                                print(group_4.text)  # print Song name #
                                group_4.click()  # Tap on image for playing song #
                                self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #
                                sleep(5)
                                hide = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[3]')
                                sleep(10)
                                hide.click()
                                sleep(3)
                                t += 1
                            else:
                                print('No songs images in categories')
                                break
                        z += 1
                        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]').click()  # Back to search page #
                    else:
                        print('Search Categories are missing')
            except BaseException as e:
                logging.error('Failed Search page: %s', e)
                pass

            first_search_image = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[6]/UIAStaticText[2]')
            first_search_image.click()  # Click on first image search song #
            heart_before_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[6]')
            print('Heart number before click:', heart_before_click.text)  # Print Heart number before click #
            heart_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]')
            heart_click.click()  # Click on Heart #
            heart_after_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[6]')
            print('Heart number after click:', heart_after_click.text)  # Print Heart number after click #
            search_menu_dots = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[2]')
            search_menu_dots.click()  # Click on 3 dots menu #

            # Add to playlist  #
            add_to_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]')
            add_to_playlist.click()
            add_new_song_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[1]/UIAStaticText[2]')
            add_new_song_playlist.click()  # Add a new song to exist playlist #
            hide = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]')
            hide.click()  # Click on Hide #
            playlist_page = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]')
            playlist_page.click()  # Move to playlist page #
            open_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]')
            open_playlist.click()  # Tap on image to open playlist #
            self.driver.swipe(50, 300, 100, -100, 2000)  # Swipe page up #
            heart_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[5]')
            if heart_playlist.text == heart_after_click.text:  # Check if heart is enable #
                print('Heart number is OK', heart_playlist.text)
            else:
                print('Heart in playlist is not increase')

                # Search page - Search bar #
            search_page = self.driver.find_element_by_xpath(' //UIAApplication[1]/UIAWindow[1]/UIAButton[2]')
            search_page.click()  # Move to search page #
            search_bar = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIASearchBar[1]/UIASearchBar[1]')
            sleep(2)
            search_bar.send_keys('bad')  # Search the words bad #
            first_result = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]/UIAStaticText[1]')
            first_result.click()  # Click on the first result search #
            song_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]')
            print(song_name.text)  # Print first result song name #
            heart_number_before_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[6]')
            print('Heart number before click-Search bar:', heart_number_before_click.text)  # Print heart number before click #
            heart_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]')
            heart_click.click()  # Click on Heart #
            heart_number_after_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[6]')
            print('Heart number after click-Search bar:', heart_number_after_click.text)
            search_menu_dots = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[2]')
            search_menu_dots.click()  # Click on menu 3 dots #
            add_to_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]')
            add_to_playlist.click()  # Click on Add to playlist #
            playlist_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIACollectionCell[1]/UIAStaticText[2]')
            playlist_name.click()  # Select playlist (exist) to add song #
            hide = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]')
            hide.click()  # Click on Hide and back to search page #
            playlist_page = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]')
            playlist_page.click()  # Move to playlist page #

            # open_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]')
            # open_playlist.click()  # Tap on image to open playlist #
            self.driver.swipe(50, 300, 100, -100, 2000)  # Swipe page up #
            self.driver.swipe(50, 300, 100, -100, 2000)  # Swipe page up #
            sleep(3)

            heart_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAStaticText[5]')
            if heart_playlist.text == heart_number_after_click.text:  # Check if heart is enable #
                print('Heart number is OK', heart_number_after_click.text)
            else:
                print('Heart in playlist is not increase')
        except NoSuchElementException as e:
            import traceback
            logging.error('Failed to do something: %s', e)
            now = datetime.now().strftime('MM IOS Small- Something is wrong, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        _log.info('Sanity finished successfully')
        sleep(5)

if __name__ == '__main__':
        suite = unittest.TestLoader().loadTestsFromTestCase(SimpleIOSTests)
        unittest.TextTestRunner(verbosity=2).run(suite)


