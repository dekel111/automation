import unittest
import os
from random import randint
from appium import webdriver
from time import sleep
import sys
import os.path
from selenium.webdriver.common.touch_actions import TouchActions
import logging
from datetime import datetime, date
from selenium.common.exceptions import NoSuchElementException

_log = logging.getLogger(__name__)


class SimpleIOSTests(unittest.TestCase):

    def setUp(self):
        # set up appium
        app = os.path.abspath('/Users/dekel/Desktop/MusicMessenger.app')
        self.driver = webdriver.Remote(
            command_executor='http://127.0.0.1:4723/wd/hub',
            desired_capabilities={
                'app': app,
                'platformName': 'iOS',
                'platformVersion': '9.2',
                'deviceName': 'iPhone 6'
            })

    def tearDown(self):
        self.driver.quit()

    def tutorial(self):
        # Tutorial pages #
        first_tutorial = {"first_join_text1": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]",
                          "first_join_text2": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[2]",
                          "first_join_logo": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAImage[1]",
                          "first_join_three_dots": "//UIAApplication[1]/UIAWindow[1]/UIAElement[1]"}
        for first_tutorial_selector in first_tutorial:
            if self.driver.find_element_by_xpath(first_tutorial[first_tutorial_selector]).is_displayed() is False:
                print(' ' + first_tutorial_selector + ' is missing in the first tutorial page')
                now = datetime.now().strftime('MM IOS- ' + first_tutorial_selector + ' is missing in the first tutorial page, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #

        second_tutorial = {"second_join_text": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]",
                           "second_join_three_dots": "//UIAApplication[1]/UIAWindow[1]/UIAElement[1]"}
        for second_tutorial_selector in second_tutorial:
            if self.driver.find_element_by_xpath(second_tutorial[second_tutorial_selector]).is_displayed() is False:
                print(' ' + second_tutorial_selector + ' is missing in the second tutorial page')
                now = datetime.now().strftime('MM IOS- "' + second_tutorial_selector + ' is missing in the second tutorial page, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #


        third_tutorial = {"8m": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]",
                          "registered_accounts": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[2]",
                          "36m": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[3]",
                          "listeners_per_mont": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[4]",
                          "500m": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[5]",
                          "total_plays_per_mont": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[6]",
                          "thank you for": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[7]"}

        for third_tutorial_selector in third_tutorial:
            if self.driver.find_element_by_xpath(third_tutorial[third_tutorial_selector]).is_displayed() is False:
                print(' ' + third_tutorial_selector + ' is missing in the third tutorial page')
                now = datetime.now().strftime('MM IOS- ' + third_tutorial_selector + ' is missing in the third tutorial page, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #

        forth_tutorial = {"third_join_text": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]",
                          "third_join_logo": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAImage[1]",
                          "let_go_button": "//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[1]"}
        for forth_tutorial_selector in forth_tutorial:
            if self.driver.find_element_by_xpath(forth_tutorial[forth_tutorial_selector]).is_displayed() is False:
                print(' ' + forth_tutorial_selector + ' is missing in the third tutorial page')
                now = datetime.now().strftime('MM IOS- ' + forth_tutorial_selector + ' is missing in the third tutorial page, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        let_go_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAButton[1]')
        let_go_button.click()
        sleep(2)
        self.driver.background_app(1)  # App in Background #
        sleep(4)

    def tab_bar_images(self):
        x = 1
        while x < 4:
            xpath = '//UIAApplication[1]/UIAWindow[1]/UIAButton[%d]' % x
            group_1 = self.driver.find_element_by_xpath(xpath)
            if group_1.is_displayed() is False:
                print('icon pages Home/Search/Playlist are missing')
            group_1.click()
            x += 1
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click()  # Move to Trending page #
        sleep(2)

    def settings(self):
        # Settings Page #
        settings = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]')
        if settings.is_displayed():
            settings.click()
        else:
            print('Settings icon is missing')
            now = datetime.now().strftime('MM IOS- Settings icon is missing, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        settings_cancel = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]')
        if settings_cancel.is_displayed():
            settings_cancel.click()
        else:
            print('Settings cancel button is missing')
            now = datetime.now().strftime('MM IOS- Settings cancel button is missing, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]').click()  # Click on Settings #

        table_settings = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]')
        sub_table_settings = len(table_settings.find_elements_by_class_name('UIATableCell'))
        g = 1
        if sub_table_settings != 0:
            while g < sub_table_settings:
                xpath = '//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[1]' % g
                settings_cells = self.driver.find_element_by_xpath(xpath)
                if settings_cells.is_displayed() is False:
                    print('Settings page- Content is missing')
                if g > 1:
                    g += 1
                    settings_cells.click()
                else:
                    g += 1
                    ############ Back button is missing need to be added ########################################

        else:
            print('Content is missing in Settings page')






    def test_ui_computation(self):   # START #

        try:
            self.tutorial()   # Tutorial method #
        except NoSuchElementException as e:
            logging.error('MM IOS- Tutorial failed: %s', e)
            now = datetime.now().strftime('MM IOS- Tutorial failed, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        # self.settings()  # Will be enabled after develop settings page #

        # Timeline page #
        # Tap on Trending menu and check if content displayed #
        i = 1
        while i < 5:
            self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[3]').click()  # Click on Trending menu #
            xpath = '//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[1]' % i
            group = self.driver.find_element_by_xpath(xpath)
            if group.is_displayed() is False:
                print('Menu Trending/This Month/Staff Picks are missing')
                now = datetime.now().strftime('MM IOS- Menu Trending/This Month/Staff Picks are missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
            group.click()
            sleep(2)
            table_menu = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
            table_menu_title = self.driver.find_element_by_xpath(' //UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]').text
            cells_menu = len(table_menu.find_elements_by_class_name('UIACollectionCell'))
            if cells_menu != 0:
                i += 1
            else:
                print(table_menu_title)
                print('Songs are missing in Trending Drop down Menu')
                now = datetime.now().strftime('MM IOS- Songs are missing in Timeline Menu, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                i += 1
            self.driver.swipe(50, 300, 50, -50, 2000)  # Swipe page Up #
        sleep(2)

        # Now playing button #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]').click()  # Click on first song #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]').click()  # Hide first song #
        now_playing = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[4]')
        if now_playing.is_displayed() is False:
            print('MM IOS- Trending- Now playing button is missing in search bar')


        # Move between Timeline/Search/Playlist and check if displayed #
        try:
            self.tab_bar_images()
        except NoSuchElementException as e:
            logging.error('MM IOS- ab bar is missing failed %s', e)
            now = datetime.now().strftime('MM IOS- Tab bar is missing failed, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        # Trending - Checking song image #
        trending = {"song_name": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[1]",
                    "counter_play": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]",
                    "song_time": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[3]",
                    "heart": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]",
                    "three_dots_menu": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[2]",
                    "heart_number": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[4]"}

        for selector in trending:
            if self.driver.find_element_by_xpath(trending[selector]).is_displayed() is False:
                print('MM IOS- ' + selector + ' is missing in Trending page image song')
                now = datetime.now().strftime('MM IOS- ' + selector + ' is missing in Trending page image song, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        artist_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[1]')
        artist_button.click()    # Enter to Artist page #
        song_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[1]')
        song_name.click()
        sleep(1)
        artist_title = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]')
        if artist_title.is_displayed() is False:
            print('Artist title name is missing in artist page')
        f = 1
        while f < 5:
            sleep(2)
            artist = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[2]/UIAStaticText[%d]' % f
            artist_page = self.driver.find_element_by_xpath(artist)
            if artist_page.is_displayed() is False:
                print('Artist page- The text "Total Plays:" and Total Loves: are missing')
            f += 1
            sleep(1)
        sleep(2)

        self.driver.swipe(50, 300, 50, -300, 2000)  # Swipe page Up #
        self.driver.swipe(50, 300, 50, -300, 2000)  # Swipe page Up #
        back_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]')  # Back button #
        back_button.click()
        sleep(1)

        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]').click()   # Click on Trending menu #
        sleep(1)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]').click()   # Move to Trending #
        sleep(2)


        # Click on heart #
        heart_before_click_element = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[4]')
        heart_before_click_score = heart_before_click_element.text
        heart_enabled = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]')
        heart_enabled.click()
        heart_after_click = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[4]').text
        if 'K' in heart_before_click_score:
            if 'K' in heart_before_click_score or '.' in heart_before_click_score:
                heart_before = float(heart_before_click_score.strip('K'))
            if 'K' in heart_after_click or '.' in heart_after_click:
                heart_after = float(heart_after_click.strip('K'))
                if heart_before != heart_after:
                    print('Heart number is not increase in Trending page image song')
                    now = datetime.now().strftime('MM IOS- Heart number is not increase in Trending page image song, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        elif float(heart_before_click_score) >= float(heart_after_click):
            print('Heart number is not increase in Trending page image song')
            now = datetime.now().strftime('MM IOS- Heart number is not increase in Trending page image song, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)


        # 3 dots menu #
        three_dots_menu_button = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[2]')
        three_dots_menu_button.click()
        add_to_playlist_1 = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]')
        if add_to_playlist_1.is_displayed():
            add_to_playlist_1.click()
        else:
            print('Add to playlist button is missing')
            now = datetime.now().strftime('MM IOS- Add to playlist button is missing, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        sleep(4)

        # Add to Playlist #
        add_to_playlist_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATextField[1]')
        sleep(2)
        add_to_playlist_name.send_keys('Develop Playlist')  # Create playlist name #
        sleep(2)
        done = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[6]') # Done button #
        if done.is_displayed():
            done.click()
        else:
            print('Done button is missing')
            now = datetime.now().strftime('MM IOS- Done button is missing for create playlist, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        sleep(3)

        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]').click()  # Move to Playlist page #
        sleep(2)
        playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]')
        if playlist.is_displayed():
            if playlist is not None:
                sleep(1)
            else:
                print('Playlist is missing')
                now = datetime.now().strftime('MM IOS- Playlist is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            playlist_name = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]')
            if playlist_name.is_displayed():
                sleep(1)
            else:
                print('Playlist name is missing')
                now = datetime.now().strftime('MM IOS- Playlist name is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]').click()  # Click on playlist #
            sleep(2)
            heart_number_in_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[4]')
            heart_number_in_playlist_score = heart_number_in_playlist.text   # check heart number song in playlist #
            if 'K' in heart_number_in_playlist_score:
                if 'K' in heart_number_in_playlist_score or '.' in heart_number_in_playlist_score:
                    heart_number_playlist = float(heart_number_in_playlist_score.strip('K'))
                    if heart_after != heart_number_playlist:
                        print('MM IOS- Heart number is not equal between Trending page to Playlist')

            elif float(heart_number_in_playlist_score) == float(heart_after_click):  # Check if heart number song in trending is equal to playlist #
                sleep(1)
            else:
                print('MM IOS- Heart number is not equal between Trending page to Playlist')

        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]').click()  # Back to playlist page #
        sleep(1)
        back_to_timeline_page = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]')  # Back to timeline page #
        back_to_timeline_page.click()
        # three_dots_menu = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]')
        # three_dots_menu.click() # Tap on 3 dots menu #
        # Need to add Share and report Test ############################


        # Search page #
        search_page = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[2]') # Move to Search page #
        search_page.click()
        search = {"popular_searches": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIAStaticText[1]",
                  "suggested_tracks": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIAStaticText[2]"}
        for search_selectors in search:
            if self.driver.find_element_by_xpath(search[search_selectors]).is_displayed() is False:
                print(' ' + search_selectors + ' text is missing on Search page')
                now = datetime.now().strftime('MM IOS- ' + search_selectors + ' text is missing on Search page, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)


        # Tap on Search Categories Top100/Beats etc...#
        z = 1
        while z < 3:
            table_categories = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
            sub_table_categories = len(table_categories .find_elements_by_class_name('UIACollectionCell'))
            if sub_table_categories != 0:
                xpath = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]' % z
                group_3 = self.driver.find_element_by_xpath(xpath)
                group_3.click()
                title = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]')
                category_missing = title
                if title.is_displayed():
                    sleep(1)
                else:
                    print('Categories title is missing on Search page')
                    now = datetime.now().strftime('MM IOS- Categories title is missing on Search page, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

                list_view = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[3]')
                if list_view.is_displayed():
                    list_view.click()
                else:
                    print('List view button is missing in Categories search page')
                    now = datetime.now().strftime('MM IOS- List view button is missing in Categories search page, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                # self.driver.swipe(50, 300, 50, -300, 2000)  # Swipe page Up #
                sleep(2)
                tail_list_view = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[3]')
                if tail_list_view.is_displayed():
                    tail_list_view.click()
                else:
                    print('Tail view button is missing in Categories search page')
                    now = datetime.now().strftime('MM IOS- Tail view button is missing in Categories search page, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                t = 1
                q = 3
                a = 1
                c = 2
                while t < 3:
                    table = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
                    cells = len(table.find_elements_by_class_name('UIACollectionCell'))
                    if cells != 0:
                        xpath_3 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAStaticText[2]' % t
                        sleep(1)
                        group_4 = self.driver.find_element_by_xpath(xpath_3)
                        sleep(1)
                        group_4.click()  # Tap on image for playing song #
                        table_swipe = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
                        cell_swipe = len(table_swipe.find_elements_by_class_name('UIACollectionCell'))
                        if cell_swipe < 1:
                            print('MM IOS- Songs for swipe is missing in Categories search page')
                            print(category_missing.text)
                            hide_1 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[3]' % a
                            sleep(2)
                            hide = self.driver.find_element_by_xpath(hide_1)
                            hide.click()
                            sleep(2)
                            break
                        if cell_swipe <= 2:
                            self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #
                            print(category_missing.text)
                            print('MM IOS- Songs for swipe is missing in Categories search page( only 1 song to swipe )')
                            hide_1 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[3]' % c
                            hide = self.driver.find_element_by_xpath(hide_1)
                            hide.click()
                            sleep(2)
                            break
                        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #
                        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #
                        sleep(3)
                        hide_1 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[3]' % q
                        sleep(1)
                        hide = self.driver.find_element_by_xpath(hide_1)
                        sleep(2)
                        if hide.is_displayed():
                            sleep(2)
                            hide.click()
                        else:
                            print('Hide button is missing in image song search page')
                            now = datetime.now().strftime('MM IOS- Hide button is missing in image song search page, %Y-%m-%d %H:%M')
                            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                        sleep(2)
                        t += 1
                        q += 1
                    else:
                        print(category_missing.text)
                        print('No songs images in categories search page')
                        now = datetime.now().strftime('MM IOS- No songs images in categories search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                        break
                z += 1
                sleep(3)
                back_to_search_page = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]')
                if back_to_search_page.is_displayed():
                    back_to_search_page.click()   # Back to search page #
                else:
                    print('Back to search page is missing')
                    now = datetime.now().strftime('MM IOS- Back to search page is missing, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            else:
                print('Search Categories are missing')
                now = datetime.now().strftime('MM IOS- Search Categories are missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)


        # Check if count image songs ODD or EVEN #
        table_image_song_sum = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
        cells_images_sum = len(table_image_song_sum.find_elements_by_class_name('UIACollectionCell')) - 11
        if cells_images_sum % 2 != 0:
            print('Song images are ODD:', cells_images_sum)

        # Tap on song image in search page #
        image_song = 12
        inc = 1
        while image_song < 16:   # Image song need to be 17 ################################################################################
            table_image_song = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]')
            cells_images = len(table_image_song.find_elements_by_class_name('UIACollectionCell'))
            if cells_images != 0:
                xpath_4 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]' % image_song
                group_5 = self.driver.find_element_by_xpath(xpath_4)
                if image_song > 18:
                    if image_song == 10 or image_song == 14 or image_song == 12:
                        self.driver.swipe(50, 300, 100, -100, 2000)  # Swipe page up #
                        sleep(1)
                    image_song_display = group_5.is_displayed()
                    if image_song_display is False:
                        self.driver.swipe(50, 300, 100, -100, 2000)  # Swipe to element #

                    self.driver.scroll(group_5, group_5)  # Scroll to element #
                    sleep(2)

                    song = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAStaticText[1]' % inc
                    song_name = self.driver.find_element_by_xpath(song)
                    if song_name.is_displayed() is False:
                        print('Song name is missing in image song on search page')
                        now = datetime.now().strftime('MM IOS- Song name is missing in image song on search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

                    counter_play = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAStaticText[2]' % inc
                    counter_play_name = self.driver.find_element_by_xpath(counter_play)
                    if counter_play_name.is_displayed() is False:
                        print('Counter play number is missing in image song on search page')
                        now = datetime.now().strftime('MM IOS- Counter play number is missing in image song on search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

                    heart = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAStaticText[5]' % inc
                    heart_number = self.driver.find_element_by_xpath(heart)
                    if heart_number.is_displayed() is False:
                        print('Heart number is missing in image song on search page')
                        now = datetime.now().strftime('MM IOS- Heart number is missing in image song on search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                    sleep(2)

                    hide_2 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[3]' % inc
                    hide = self.driver.find_element_by_xpath(hide_2)  # Hide page and back to search page #
                    sleep(6)
                    if hide.is_displayed():
                        hide.click()
                        image_song += 1
                        inc += 1
                    else:
                        print('Hide button is missing in image song on search page')
                        now = datetime.now().strftime('MM IOS- Hide button is missing in image song on search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                        image_song += 1
                        inc += 1
                else:
                    sleep(2)
                    self.driver.scroll(group_5, group_5)  # Scroll to element #
                    sleep(2)
                    song_name = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAStaticText[1]' % inc
                    song_name = self.driver.find_element_by_xpath(song_name)
                    if song_name.is_displayed() is False:
                        print('Song name is missing in image song on search page')
                        now = datetime.now().strftime('MM IOS- Song name is missing in image song on search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

                    counter_play = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAStaticText[2]' % inc
                    sleep(2)
                    counter_play_name = self.driver.find_element_by_xpath(counter_play)
                    if counter_play_name.is_displayed() is False:
                        print('Counter play number is missing in image song on search page')
                        now = datetime.now().strftime('MM IOS- Counter play number is missing in image song on search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                    sleep(2)

                    hide_2 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[3]' % inc
                    sleep(1)
                    hide = self.driver.find_element_by_xpath(hide_2)  # Hide page and back to search page #
                    sleep(3)
                    if hide.is_displayed():
                        hide.click()
                        image_song += 1
                        inc += 1
                    else:
                        print('Hide button is missing in image song on search page')
                        now = datetime.now().strftime('MM IOS- Hide button is missing in image song on search page, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                        image_song += 1
                        inc += 1
            else:
                print('Images song in search page are missing')
                now = datetime.now().strftime('MM IOS- Images song in search page are missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                break

        back_to_search_page = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[2]')
        back_to_search_page.click()  # Move to Search page #

        # Search Bar #
        search_bar = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIASearchBar[1]/UIASearchBar[1]')
        search_bar.send_keys('bad')   # Search the words bad #
        search_result_list = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]')
        cells_search_result_list = len(search_result_list.find_elements_by_class_name('UIATableCell'))
        if cells_search_result_list != 0:
            first_result = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[2]')
            if first_result.is_displayed():
                first_result.click()  # Tap on the first song in search result list #
            else:
                print('Search bar- First result in search page is missing')
                now = datetime.now().strftime('MM IOS- Search bar- First result in search page is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
            sleep(2)

            search = {"song_name": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[1]",
                      "counter_play": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]",
                      "heart_number": "//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[5]"}

            for search_selector in search:
                if self.driver.find_element_by_xpath(search[search_selector]).is_displayed() is False:
                    print('MM IOS- Search Bar- ' + search_selector + ' is missing in first result')
                    now = datetime.now().strftime('MM IOS- ' + search_selector + ' is missing in image song on search page, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            # self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #

            sleep(3)
            hide = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]')
            sleep(1)
            if hide.is_displayed():
                hide.click()  # Tap on Hide button and back to Search page #
                sleep(1)
            else:
                print('Search Bar- Hide button is missing in first result')
                now = datetime.now().strftime('MM IOS- Hide button is missing in first result, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
            sleep(1)
            second_result = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[3]/UIAStaticText[1]')
            if second_result.is_displayed():
                second_result.click()  # Tap on second song from search result page #
            else:
                print('Search Bar- Second result in search page is missing')
                now = datetime.now().strftime('MM IOS- Search bar- Second result in search page is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            hide = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[3]')
            if hide.is_displayed():
                hide.click()  # Tap on Hide button and back to Search page #
            else:
                print('Search Bar- Hide button is missing in second result')
                now = datetime.now().strftime('MM IOS- Hide button is missing in second result, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            show_all_results_button = self.driver.find_element_by_xpath(' //UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[8]/UIAStaticText[1]')
            if show_all_results_button.is_displayed():
                show_all_results_button.click()  # Click on Show all results search list #
            else:
                print('Search Bar- Show all results text is missing')
                now = datetime.now().strftime('MM IOS- Search Bar- Show all results text is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
            sleep(3)

            search_bar_cancel = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]')
            if search_bar_cancel.is_displayed():
                search_bar_cancel.click()   # Click on Cancel and back to Search page #
            else:
                print('Search Bar- Cancel button is missing')
                now = datetime.now().strftime('MM IOS- Search Bar- Cancel button is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        else:
            print('Search bar- No search results list')
            now = datetime.now().strftime('MM IOS- Search Bar- Cancel button is missing, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        search_bar = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIASearchBar[1]/UIASearchBar[1]')
        search_bar.send_keys('kjehdsfkjgfskdfjlksfldksfjldskjf')
        sleep(1)
        no_results = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]')
        if no_results.is_displayed() is False:
            print('The text No results for ... is missing')
            now = datetime.now().strftime('MM IOS- Search Bar- The text "No results for ..." is missing, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        search_button_keyboard = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]')
        if search_button_keyboard is not None:
            search_button_keyboard.click()
        else:
            print('search keyboard button is missing')

        no_search_result_page_first_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]')
        if no_search_result_page_first_text.is_displayed() is False:
            print('Empty Playlist page text "No search results" is missing')  # Print if the text "No search results" is missing #

        no_search_result_page_second_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]')
        if no_search_result_page_second_text.is_displayed() is False:
            print('Empty Playlist page text "No search results" is missing')  # Print if the text "Try changing your search query" is missing #

        search_bar_cancel = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[1]')
        if search_bar_cancel.is_displayed():
            search_bar_cancel.click()
        else:
            print('Cancel button is missing in search bar')

        # Playlist Page #
        playlist_page = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]')
        playlist_page.click()
        tap_on_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]')
        if tap_on_playlist.is_displayed():
            tap_on_playlist.click()  # Click on playlist #
        else:
            print('Playlist is missing')

        playlist_menu = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]')
        if playlist_menu.is_displayed():
            playlist_menu.click()  # Click on playlist menu name #
        else:
            print('Playlist menu is missing')
            now = datetime.now().strftime('MM IOS- Playlist menu is missing, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        # Delete playlist and check texts for empty page #
        delete_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[4]/UIAStaticText[1]')
        if delete_playlist.is_displayed():
            delete_playlist.click()  # Delete playlist#
        else:
            print('Delete button is missing in playlist menu')
            now = datetime.now().strftime('MM IOS- Delete button is missing in playlist menu, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        are_you_sure_to_delete_playlist = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAAlert[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[1]')
        if are_you_sure_to_delete_playlist.is_displayed():
            are_you_sure_to_delete_playlist.click()
        else:
            print('Are you sure to delete pop up window is missing in playlist page')
            now = datetime.now().strftime('MM IOS- Are you sure to delete pop up window is missing in playlist page, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        empty_playlist = {"playlist_title": "//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAStaticText[1]",
                    "no_playlist_text_1": "//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]",
                    "no_playlist_text_2": "//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]"}

        for playlist_selector in empty_playlist:
            if self.driver.find_element_by_xpath(empty_playlist[playlist_selector]).is_displayed() is False:
                print('MM IOS- ' + playlist_selector + ' is missing for empty playlist page')
                now = datetime.now().strftime('MM IOS- ' + playlist_selector + ' is missing for empty playlist page, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        # Create First Playlist #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click()  # Move to Timeline page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]').click()  # Click on first song #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[2]').click()   # 3 dots menu #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]').click() # Click on Add to playlist #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATextField[1]').send_keys('hy')  # Create playlist name #
        sleep(1)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[2]').click()  # Click on Done #
        sleep(3)
        self.driver.swipe(200, 300, -200, 0, 5000)  # Swipe right to left #
        sleep(3)

        # Create Second Playlist #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[2]').click()  # 3 dots menu #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]').click()  # Click on Add to playlist #
        sleep(1)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]').click()  # Click on New Playlist #
        sleep(1)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATextField[1]').send_keys('hello')  # Create playlist name #
        sleep(1)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[2]').click()  # Click on Done #
        sleep(2)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[3]').click()  # Click on Hide #
        sleep(1)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]').click()   # Move to Playlist page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAStaticText[2]').click()  # Click on playlist #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[3]').click()  # Click on playlist menu #

        # Check if drop down playlist menu displayed #
        e = 1
        while e < 5:
            playlist_menu = '//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[%d]/UIAStaticText[1]' % e
            sleep(1)
            playlist_menu_drop_down = self.driver.find_element_by_xpath(playlist_menu)
            if playlist_menu_drop_down.is_displayed() is False:
                print('Playlist drop down menu is missing " Rename/Share etc.."')
            e += 1
        # Rename playlist name #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATableView[1]/UIATableCell[1]/UIAStaticText[1]').click()  # Click on Rename #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIATextField[1]').send_keys('you')  # Change playlist name to "you" #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[6]').click()  # Click on Done #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIANavigationBar[1]/UIAButton[2]').click()  # Click on Back button #
        sleep(10)
        _log.info('Sanity finished successfully')
        sleep(10)







        sleep(5)

if __name__ == '__main__':
   suite = unittest.TestLoader().loadTestsFromTestCase(SimpleIOSTests)
   unittest.TextTestRunner(verbosity=2).run(suite)


