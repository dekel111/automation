import os
from time import sleep
import unittest
from appium import webdriver
import os.path
import logging
from datetime import datetime, date
from selenium.common.exceptions import NoSuchElementException

# Returns abs path relative to this file and not cwd
PATH = lambda p: os.path.abspath(
    os.path.join(os.path.dirname(__file__), p)
)


class SimpleAndroidTests(unittest.TestCase):
    def setUp(self):
        desired_caps = {}
        desired_caps['platformName'] = 'Android'
        desired_caps['platformVersion'] = '5.1'
        desired_caps['deviceName'] = 'Nexus S API 22'
        desired_caps['app'] = PATH(
            '../../../Desktop/MMAndroid-Prod  APK v1.6.apk'
        )
        desired_caps["newCommandTimeout"] = 500
        self.driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)

    def tearDown(self):
        self.driver.quit()

    def test_find_elements(self):
        # Tutorial #
        logger = logging.getLogger('log')
        logger.info('Start Sanity')
        sleep(2)
        first_page_lenght_class = self.driver.find_elements_by_class_name('android.widget.TextView')
        for i in range(len(first_page_lenght_class)):
            sleep(4)
            first_page_tutorial = self.driver.find_elements_by_class_name('android.widget.TextView')[i]
            sleep(1)
            if first_page_tutorial.is_displayed() is False:
                print('First tutorial- The text Welcome to Music Messenger is missing')
            i += 1
        # self.driver.press_keycode(3)   # App in background #
        # sleep(3)
        # self.driver.launch_app()  # Relaunch app from background #
        sleep(3)
        self.driver.swipe(800, 100, 50, 100, 5000)  # Swipe Right to left #
        sleep(3)

        second_page_lenght_class = self.driver.find_elements_by_class_name('android.widget.TextView')
        for y in range(len(second_page_lenght_class)):
            second_page_tutorial = self.driver.find_elements_by_class_name('android.widget.TextView')[y]
            if second_page_tutorial.is_displayed() is False:
                print(
                    'Second tutorial- The text Mix-tapes, remixes and originally created tracks Totally FREE, is missing')
            y += 1

        sleep(3)
        self.driver.swipe(800, 100, 50, 100, 5000)  # Swipe Right to left #
        sleep(3)

        z = 0
        while z < 4:
            third_page_tutorial = self.driver.find_elements_by_class_name('android.widget.TextView')[z]
            if third_page_tutorial.is_displayed() is False:
                print('Third tutorial- The text "Totally FREE, no ads or interruptions. Enjoy!" is missing')
            z += 1

        # CLick on Let's go #
        let_try = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewDone')
        if let_try.is_displayed():
            let_try.click()
        else:
            print('Lets go button is missing')
        sleep(10)

        # Trending page #
        # Move between Trending/Search/Playlist #
        w = 1
        while w < 4:
            xpath = '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.ImageView[%d]' % w
            group_3 = self.driver.find_element_by_xpath(xpath)
            group_3.click()
            sleep(2)
            if w == 3:
                no_playlist_yet = self.driver.find_element_by_id(
                    'com.musicmessenger.android:id/textViewEmptyViewTitle')
                if no_playlist_yet.is_displayed() is False:
                    print('MM Android- text "No playlist yet" is missing in empty playlist page')
                try_creating_a_new_one = self.driver.find_element_by_id(
                    'com.musicmessenger.android:id/textViewEmptyViewText')
                if try_creating_a_new_one.is_displayed() is False:
                    print('MM Android- text "Select tracks to add to playlist" is missing in empty playlist page')

            w += 1
        self.driver.find_element_by_xpath(
            '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.ImageView[1]').click()

        # Timeline page #
        # Tap on Trending menu and check if content displayed #
        i = 0
        while i < 4:
            self.driver.find_element_by_id(
                'com.musicmessenger.android:id/textViewTitle').click()  # Click on Trending menu #
            trending_menu = self.driver.find_elements_by_class_name('android.widget.TextView')[i]
            if trending_menu.is_displayed() is False:
                print('MM Android- Menu Trending/This Month/Staff Picks are missing')
                now = datetime.now().strftime(
                    'MM IOS- Menu Trending/This Month/Staff Picks are missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
            trending_menu.click()
            sleep(2)

            table_menu = self.driver.find_element_by_xpath(
                '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager[1]/android.widget.RelativeLayout[1]/android.view.View[1]/android.support.v7.widget.RecyclerView[1]/android.widget.RelativeLayout[1]')
            cells_menu = len(table_menu.find_elements_by_class_name('android.widget.RelativeLayout'))
            if cells_menu != 0:
                i += 1
            else:
                print('MM Android- Songs are missing in Timeline Menu')
                now = datetime.now().strftime('MM IOS- Songs are missing in Timeline Menu, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
                i += 1

        trending_menu = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTitle')
        trending_menu.click()  # Click on Trending menu #
        trending = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewMenuTrending')
        trending.click()  # Click on Trending #


        # Report in Trending page #
        sleep(2)
        self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewItemMenu').click()  # Click on song 3 dots menu #
        sleep(1)
        self.driver.find_element_by_id('com.musicmessenger.android:id/textViewMenuReport').click()  # Click on Report #

        report_list = self.driver.find_elements_by_class_name('android.widget.TextView')
        w = 0
        while w < 4:
            if report_list[w].is_displayed() is False:
                print(report_list[w].text, 'is missing')
                w += 1
            w += 1

        self.driver.find_element_by_id('com.musicmessenger.android:id/reportCopyright').click()  # Click on Copyright Infringement  in Trending page #
        sleep(1)
        thanks_report = {"image": "com.musicmessenger.android:id/imageViewReport",
                        "thanks_for_reporting": "com.musicmessenger.android:id/textViewReportTitle",
                        "your feedback is": "com.musicmessenger.android:id/robotoTextView2",
                        "to_make_a_full": "com.musicmessenger.android:id/robotoTextView",
                        "update_me_link": "com.musicmessenger.android:id/textViewUpdateMe"}
        for thanks in thanks_report:
            if self.driver.find_element_by_id(thanks_report[thanks]).is_displayed is False:
                print('MM Android- ' + thanks + ' text is missing ')

        self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewClose').click()   # Click on X (Close) #
        sleep(1)

        self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewItemMenu').click()  # Click on song 3 dots menu #
        not_click = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewMenuReport').text
        if not_click == 'Reported':
            sleep(1)
        else:
            print('MM Android- Trending page- reported text is not displayed')
        sleep(1)
        self.driver.press_keycode(4)

        # Share need to be add #
        # self.driver.find_element_by_id('com.musicmessenger.android.alpha:id/textViewMenuShare').click()  # Click on Share #
        # self.driver.find_element_by_xpath('//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/com.android.internal.widget.ResolverDrawerLayout[1]/android.widget.ListView[1]/android.widget.LinearLayout[1]/android.widget.LinearLayout[1]/android.widget.TextView[1]').click()


        sleep(2)

        # Trending - Checking song image #
        elements_array = {
                          "song_name": "com.musicmessenger.android:id/textViewSongName",
                          "counter_play": "com.musicmessenger.android:id/textViewSongPlayCount",
                          "song_time": "com.musicmessenger.android:id/textViewSongDuration",
                          "heart": "com.musicmessenger.android:id/imageViewItemHeartImage",
                          "three_dots_menu": "com.musicmessenger.android:id/imageViewItemMenu",
                          "heart_number": "com.musicmessenger.android:id/textViewItemLovedCount"}

        for selector in elements_array:
            sleep(2)
            if self.driver.find_element_by_id(elements_array[selector]).is_displayed() is False:
                print('MM Android - ' + selector + ' is missing in Trending page image song')
                now = datetime.now().strftime(
                    'MM Android- ' + selector + ' is missing in Trending page image song, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        artist_button = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewSongName')
        artist_button.click()  # Enter to Artist page #
        sleep(4)
        artist_title = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTitle')
        if artist_title.is_displayed() is False:
            print('MM Android- Artist title name is missing in artist page')
        sleep(4)
        total_plays = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTotalPlay')
        if total_plays.is_displayed() is False:
            print('MM Android- Artist page- The text "Total Plays:" is missing')
        total_loves = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTotalLove')
        if total_loves.is_displayed() is False:
            print('MM Android- Artist page- The text Total Loves: is missing')

        self.driver.swipe(50, 800, 50, 100, 5000)  # Swipe page down #
        sleep(2)
        back_button = self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewBack')
        if back_button.is_displayed():
            back_button.click()  # Back to Trending page #
        else:
            print('MM Android- Artist page- Back button is missing')

        # Click on heart #
        heart_before_click_element = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/textViewItemLovedCount')
        heart_before_click_score = heart_before_click_element.text
        heart_enabled = self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewItemHeartImage')
        heart_enabled.click()
        heart_after_click = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewItemLovedCount')
        heart_after_click = heart_after_click.text
        if 'k' in heart_before_click_score:
            if 'k' in heart_before_click_score or '.' in heart_before_click_score:
                heart_before = float(heart_before_click_score.strip('k'))

            if 'k' in heart_after_click or '.' in heart_after_click:
                heart_after = float(heart_after_click.strip('k'))
                if heart_before != heart_after:
                    print('MM Android- Heart number is not increase in Trending page image song')
                    now = datetime.now().strftime(
                        'MM Android- Heart number is not increase in Trending page image song, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        elif float(heart_before_click_score) >= float(heart_after_click.text):
            print('MM Android- Heart number is not increase in Trending page image song')
            now = datetime.now().strftime(
                'MM Android- Heart number is not increase in Trending page image song, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        # 3 dots menu #
        three_dots_menu_button = self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewItemMenu')
        three_dots_menu_button.click()
        add_to_playlist_1 = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/textViewMenuAddToPlaylist')
        if add_to_playlist_1.is_displayed():
            add_to_playlist_1.click()
        else:
            print('MM Android- Add to playlist button is missing')
            now = datetime.now().strftime('MM Android- Add to playlist button is missing, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        sleep(2)

        # Add to Playlist #
        creat_new_play_list = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/createNewPlaylistTextView')
        creat_new_play_list.click()
        insert_playlist_name = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/createNewPlaylistEditText')
        insert_playlist_name.send_keys('Develop Playlist')  # Create playlist name #
        sleep(2)
        done = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewDone')  # Done button #
        if done.is_displayed():
            done.click()
        else:
            print('MM Android- Done button is missing')
            now = datetime.now().strftime('MM Android- Done button is missing for craete playlist, %Y-%m-%d %H:%M')
            self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
        sleep(3)

        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/playlist_page').click()  # Move to Playlist page#
        playlist = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewPlaylisName')
        if playlist.is_displayed():
            if playlist is not None:
                sleep(1)
            else:
                print('MM Android- Playlist is missing')
                now = datetime.now().strftime('MM Android- Playlist is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            playlist_name = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewPlaylisName')
            if playlist_name.is_displayed():
                sleep(1)
            else:
                print('MM Android- Playlist name is missing')
                now = datetime.now().strftime('MM Android- Playlist name is missing, %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)
            self.driver.find_element_by_id(
                'com.musicmessenger.android:id/textViewPlaylisName').click()  # Click on playlist #
            heart_number_in_playlist = self.driver.find_element_by_id(
                'com.musicmessenger.android:id/textViewItemLovedCount')
            heart_number_in_playlist_score = heart_number_in_playlist.text  # check heart number song in playlist #

            if 'k' in heart_number_in_playlist_score:
                if 'k' in heart_number_in_playlist_score or '.' in heart_number_in_playlist_score:
                    heart_number_playlist = float(heart_number_in_playlist_score.strip('k'))
                    if heart_after != heart_number_playlist:
                        print('MM Android- Heart number is not equal between Trending page to Playlist')
                        now = datetime.now().strftime(
                            'MM Android- Heart number is not equal between Trending page to Playlist, %Y-%m-%d %H:%M')
                        self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

            elif float(heart_number_in_playlist_score) == float(
                    heart_after_click.text):  # Check if heart number song in trending is equal to playlist #
                sleep(1)
            else:
                print('MM Android- Heart number is not equal between Trending page to Playlist')

            self.driver.find_element_by_id(
                'com.musicmessenger.android:id/imageViewBack').click()  # Back to playlist page #
        sleep(1)
        back_to_timeline_page = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/main_page')  # Back to timeline page #
        back_to_timeline_page.click()
        # three_dots_menu = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[3]')
        # three_dots_menu.click() # Tap on 3 dots menu #
        # Need to add Share and report Test ############################

        # Search page #
        search_page = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/search_page')  # Move to Search page #
        search_page.click()

        popular_searches = self.driver.find_element_by_id('com.musicmessenger.android:id/header_hashtag_tv')
        if popular_searches.is_displayed() is False:
            print('MM Android- Search page- Popular Searches text is missing')

        # Tap on Search Categories Top100/Beats etc...#
        z = 1
        while z < 6:
            first_3_categories = self.driver.find_elements_by_class_name('android.widget.TextView')[z]
            if first_3_categories.is_displayed():
                first_3_categories.click()  # Click on Top 100/Midnight/electro #
            else:
                print('MM Android- Search page- first three categories "Top100/Midnight/electro" are missing')

            sleep(2)
            search_title = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTitle')
            if search_title.is_displayed() is False:
                print('MM Android- Search page Title is missing')

            table_menu = self.driver.find_element_by_xpath(
                '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[2]/android.widget.RelativeLayout[1]/android.widget.ViewFlipper[1]/android.widget.FrameLayout[1]/android.support.v7.widget.RecyclerView[1]')
            cells_menu = len(table_menu.find_elements_by_class_name('android.widget.RelativeLayout'))
            if cells_menu != 0:
                list_view = self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewChangeToList')
                if list_view.is_displayed():
                    list_view.click()  # Change to list view #
                else:
                    print('MM Android- Search page categories list view icon is missing')
            else:
                print(search_title.text)
                print('MM Android- No songs images in categories search page')
            z += 1
            back_button_search_page = self.driver.find_element_by_id(
                'com.musicmessenger.android:id/imageViewBack')
            back_button_search_page.click()  # Back to search page #

        suggested_tracks = self.driver.find_element_by_id('com.musicmessenger.android:id/header_suggested_tv')
        if suggested_tracks.is_displayed() is False:
            print('MM Android- search page- Suggested Tracks text is missing')

        # Tap on song image in search page #
        inc = 1
        while inc < 4:
            image_song = self.driver.find_elements_by_class_name('android.widget.ImageView')[inc]
            if image_song != 0:
                image_song.click()
                sleep(2)
                image_search = {"song_name": "com.musicmessenger.android:id/songTitleTextView",
                                "counter_play_name": "com.musicmessenger.android:id/viewCountsTextView",
                                "heart_number": "com.musicmessenger.android:id/lovedCountTextView"}

                for image_search_selector in image_search:
                    sleep(1)
                    if self.driver.find_element_by_id(image_search[image_search_selector]).is_displayed() is False:
                        print('MM Android- ' + image_search_selector + ' is missing in image song on search page')


                hide = self.driver.find_element_by_id(
                    'com.musicmessenger.android:id/hideTextView')  # Hide page and back to search page #
                sleep(3)
                if hide.is_displayed():
                    hide.click()
                    inc += 1
                    pulse_icon = self.driver.find_element_by_id('com.musicmessenger.android:id/now_playing_page')
                    if pulse_icon.is_displayed() is False:
                        print('MM Android- Search page- Pulse icon is missing')
                else:
                    print('MM Android- Hide button is missing in image song on search page')
                    inc += 1
            else:
                print('MM Android- Images song in search page are missing')
                inc += 1
        sleep(2)
        self.driver.swipe(50, 800, 50, 100, 5000)  # Swipe page up #
        self.driver.swipe(50, 800, 50, 100, 5000)  # Swipe page up #
        sleep(2)

        # Search bar #
        search_bar_field = self.driver.find_element_by_id('com.musicmessenger.android:id/searchEditText')
        search_bar_field.send_keys('bad')
        search_results_list = len(self.driver.find_elements_by_class_name(
            'android.widget.RelativeLayout'))  # Check if search results displayed #

        if search_results_list != 0:
            sleep(1)
            first_search_result = self.driver.find_element_by_id(
                'com.musicmessenger.android:id/resultNameTextView')
            first_search_result.click()  # Click on first result in list #

            search_results = {"song_name_search_bar_list": "com.musicmessenger.android:id/songTitleTextView",
                              "counter_search_bar_list": "com.musicmessenger.android:id/viewCountsTextView",
                              "heart_icon_search_bar_list": "com.musicmessenger.android:id/heartImageView",
                              "heart_number_search_bar_list": "com.musicmessenger.android:id/lovedCountTextView"}

            for selectors in search_results:
                if self.driver.find_element_by_id(search_results[selectors]).is_displayed() is False:
                    print('MM Android - ' + selectors + ' is missing in search results list (player)')

            tap_three_dots_menu_search_bar_list = self.driver.find_element_by_id(
                'com.musicmessenger.android:id/imageViewMenu')
            tap_three_dots_menu_search_bar_list.click()  # Click on 3 dots menu in player#
            tap_add_to_playlist = self.driver.find_element_by_id(
                'com.musicmessenger.android:id/textViewMenuAddToPlaylist')
            if tap_add_to_playlist.is_displayed():
                tap_add_to_playlist.click()  # Click on Add to playlist #
            else:
                print('MM Android- Add to Playlist is missing in search results list (player)')
            create_new_playlist = self.driver.find_element_by_id(
                'com.musicmessenger.android:id/createNewPlaylistTextView')
            if create_new_playlist.is_displayed():
                create_new_playlist.click()  # click on Create new playlist #
            else:
                print('MM Android- Create new Playlist is missing in search results list (player)')
            create_playlist_name = self.driver.find_element_by_id(
                'com.musicmessenger.android:id/createNewPlaylistEditText')
            create_playlist_name.send_keys('hello')
            done = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewDone')
            if done.is_displayed():
                done.click()  # Click on Done #
            else:
                print('MM Android- Create new Playlist- Done button is missing in search results list (player)')
            self.driver.find_element_by_id(
                'com.musicmessenger.android:id/hideTextView').click()  # Click on Hide #
        else:
            print('MM Android- No results in search bar list')

        clear_icon_search_bar = self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewClear')
        if clear_icon_search_bar.is_displayed():
            clear_icon_search_bar.click()  # Click on X (clear) in search bar field #
        else:
            print('MM Android- Clear button X is missing in search bar field')
        back_button_search_bar = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/searchButtonImageView')
        if back_button_search_bar.is_displayed():
            back_button_search_bar.click()
        else:
            print('MM Android- Back button is missing in search bar field')
        now_playing_icon = self.driver.find_element_by_id('com.musicmessenger.android:id/now_playing_page')
        if now_playing_icon.is_displayed() is False:
            print('MM Android- Now playing icon pulse is missing in Search page')

        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/playlist_page').click()  # Move to Playlist page #
        playlist_name = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewPlaylisName')
        if playlist_name.is_displayed() is False:
            print('MM Android- Playlist is missing in Playlist page')
        now_playing_icon_playlist_page = self.driver.find_element_by_id(
            'com.musicmessenger.android:id/now_playing_page')
        if now_playing_icon_playlist_page.is_displayed() is False:
            print('MM Android- Now playing icon pulse is missing in Playlist page')
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/search_page').click()  # Move to search page #

        # Search with no results #
        search_bar = self.driver.find_element_by_id('com.musicmessenger.android:id/searchEditText')
        search_bar.send_keys('wsedrftgyhu')
        sleep(3)
        no_results = self.driver.find_element_by_id('com.musicmessenger.android:id/searchForTextView')
        if no_results.is_displayed() is False:
            print('MM Android- "No results for" text is missing in search bar')

        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/imageViewClear').click()  # Click on Clear X #
        search_bar_field = self.driver.find_element_by_id('com.musicmessenger.android:id/searchEditText')
        search_bar_field.send_keys('bad')
        sleep(2)

        # See more for "bad" #
        see_more_results = self.driver.find_element_by_id('com.musicmessenger.android:id/searchForTextView')
        if see_more_results.is_displayed():
            see_more_results.click()  # Click on "See more for bad" #
            sleep(2)
        else:
            print('MM Android- See more for "Bad" text is missing in search page')
        self.driver.swipe(50, 800, 50, 100, 5000)  # Swipe page up #
        self.driver.swipe(50, 800, 50, 100, 5000)  # Swipe page up #
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/imageViewClear').click()  # Click on clear X #
        self.driver.press_keycode(4)  # Back native #
        self.driver.press_keycode(4)  # Back native #
        sleep(3)
        self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewClear').click()
        sleep(2)
        self.driver.find_element_by_id('com.musicmessenger.android:id/searchEditText').send_keys(
            'g')  # Search the word g #
        sleep(2)
        self.driver.find_element_by_id('com.musicmessenger.android:id/resultNameTextView').click()   # Click on first image song from results list #
        sleep(2)
        self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewMenu').click()  # Click on 3 dots menu #
        sleep(2)
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/textViewMenuAddToPlaylist').click()  # Click on add to playlist #
        sleep(2)
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/textViewPlaylistName').click()  # Add song to exist playlist #
        sleep(2)
        self.driver.find_element_by_id('com.musicmessenger.android:id/hideTextView').click()  # Click on Hide #
        sleep(3)
        self.driver.press_keycode(4)  # Back native #
        self.driver.press_keycode(4)  # Back native #
        sleep(3)
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/playlist_page').click()  # Move to playlist page #
        sleep(2)

        # Playlist page #
        playlist_title_page = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTitle')
        if playlist_title_page.is_displayed() is False:  # Check if title "Playlist" displayed #
            print('MM Android- Playlist  title text page is missing')
        sleep(2)
        p = 1
        while p < 3:
            playlist_xpath = '//android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.view.View[1]/android.widget.RelativeLayout[1]/android.widget.LinearLayout[1]/android.widget.FrameLayout[1]/android.widget.LinearLayout[1]/android.support.v4.view.ViewPager[1]/android.widget.RelativeLayout[1]/android.support.v7.widget.RecyclerView[1]/android.widget.LinearLayout[%d]' % p
            sleep(2)
            playlist_len = self.driver.find_element_by_xpath(playlist_xpath)
            if playlist_len is None:
                print('MM Android- Playlists is missing in playlist page')
            else:
                playlist_len.click()  # Click on Playlist #
                sleep(2)
                self.driver.find_element_by_id(
                    'com.musicmessenger.android:id/imageViewChangeToList').click()  # Change to list view #
                sleep(1)
                self.driver.find_element_by_id(
                    'com.musicmessenger.android:id/imageViewChangeToList').click()  # Change to tile view #
                sleep(1)
                playlist_title = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTitle')
                if playlist_title.is_displayed() is False:
                    print('MM Android- Playlist name is missing')
                self.driver.find_element_by_id(
                    'com.musicmessenger.android:id/imageViewBack').click()  # Back to playlist page #
                p += 1

        sleep(2)
        # Rename Playlist #
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/textViewPlaylisName').click()  # Click on first playlist #
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/imageViewOverflow').click()  # Click on playlist menu #

        menu_playlist = self.driver.find_elements_by_class_name('android.widget.TextView')
        for d in range(len(menu_playlist)):
            if menu_playlist[d].is_displayed() is False:
                print('MM Android- Playlist Menu content is missing (rename/share/edit etc...)')
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/textViewRename').click()  # Click on Rename #
        sleep(2)
        self.driver.find_element_by_id(
            'com.musicmessenger.android:id/renamePlaylistEditText').clear()  # Clear playlist name #
        sleep(2)
        self.driver.find_element_by_id('com.musicmessenger.android:id/renamePlaylistEditText').send_keys(
            'shellanoo')  # Change playlist name to shellano #
        sleep(2)
        self.driver.find_element_by_id('com.musicmessenger.android:id/textViewDone').click()  # Click on Done #
        sleep(2)
        rename_title = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewTitle').text
        if rename_title != 'shellanoo':
            print('MM Android- rename title is not changed')

        self.driver.find_element_by_id('com.musicmessenger.android:id/imageViewItemHeartImage').click()     # Checked love icon #
        unchecked_love_number_playlist = self.driver.find_element_by_id('com.musicmessenger.android:id/textViewItemLovedCount')    # Love number in playlist #
        unchecked_love_number_playlist_score = unchecked_love_number_playlist.text
        self.driver.find_element_by_id('com.musicmessenger.android:id/textViewSongPlayCount').click()     # Click on first song in playlist #
        unchecked_love_number_playlist_player = self.driver.find_element_by_id('com.musicmessenger.android:id/lovedCountTextView')     # Love number song in playlist #
        unchecked_love_number_playlist_player_score = unchecked_love_number_playlist_player.text

        if 'k' in unchecked_love_number_playlist_score:
            if 'k' in unchecked_love_number_playlist_score or '.' in unchecked_love_number_playlist_score:
                unchecked_love_number_playlist_score = float(unchecked_love_number_playlist_score.strip('k'))
            if 'k' in unchecked_love_number_playlist_player_score or '.' in unchecked_love_number_playlist_player_score:
                unchecked_love_number_playlist_player_score = float(unchecked_love_number_playlist_player_score.strip('k'))
                if unchecked_love_number_playlist_score != unchecked_love_number_playlist_player_score:
                    print('MM Android- Heart number is not equal between Playlist to song in playlist player')
                    now = datetime.now().strftime(
                    'MM Android- Heart number is not equal between Playlist to song in playlist player, %Y-%m-%d %H:%M')
                    self.driver.save_screenshot('../screenshots/mm/screenshot-%s.png' % now)

        else:
            if float(unchecked_love_number_playlist_score) != float(unchecked_love_number_playlist_player_score):
                print('MM Android- Heart number is not equal between Playlist to song in playlist player')






        sleep(10)

        logger.info('Finish Sanity MM Android Successful')

        #
        # # Scroll UP Trending page #
        # self.driver.swipe(50, 800, 50, 100, 5000)
        # self.driver.swipe(50, 800, 50, 100, 5000)

        # scroll page Down #
        # self.driver.swipe(200, 400, 200, 800, 5000)
