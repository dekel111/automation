import string
from random import choice


def random_str(size):
    lettersDigits = string.ascii_letters + string.digits
    return ''.join([choice(lettersDigits) for i in range(size)])


def random_digits(size):
    """
    return random number as string
    :type size: int
    :param size: number of digits in the number
    :rtype: str
    :return: the number
    """
    return ''.join([choice(string.digits) for i in range(size)])


def merge_dicts(a, b, path=None):
    """merges b into a"""
    if path is None: path = []
    for key in b:
        if key in a:
            if isinstance(a[key], dict) and isinstance(b[key], dict):
                merge_dicts(a[key], b[key], path + [str(key)])
            elif a[key] == b[key]:
                pass  # same leaf value
            else:
                a[key] = b[key]
        else:
            a[key] = b[key]
    return a