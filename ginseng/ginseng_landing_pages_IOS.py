import os
import gspread
import unittest
from time import sleep
from selenium import webdriver
from datetime import datetime, date
from oauth2client.service_account import ServiceAccountCredentials
from random import randint
import logging
from selenium.webdriver.support.ui import Select
import requests
import boto.ses
from helper_functions import write_to_log_file
from selenium.webdriver.common.action_chains import ActionChains

class WebViewIOSTests(unittest.TestCase):
    capabilities = {'browserName': 'Safari', "deviceName": "iPhone 6", "platformVersion": "9.2"}
    driver = webdriver.Remote('http://localhost:4723/wd/hub', capabilities)

    def tearDown(self):
        self.driver.quit()

    def test_get_url(self):
        try:
            self.driver.get('http://www.ginseng-life.com/wllp/')
            # First landing page #
            sleep(10)
            button = self.driver.find_element_by_css_selector('.second-m')
            button.click()
            self.driver.find_element_by_id("Weight").send_keys('70')
            self.driver.find_element_by_id("Height").send_keys('1.72')
            self.driver.find_element_by_class_name('first-step-btn').click()
            sleep(2)
            self.driver.find_element_by_class_name('btn').click()
            sleep(5)

            # Second landing page #
            if len(self.driver.find_elements_by_class_name('btn-my')):
                learn_more = randint(0, len(self.driver.find_elements_by_class_name('btn-my'))-1)
                learn_more_button = self.driver.find_elements_by_css_selector('.btn-my')[learn_more]
                self.driver.execute_script("arguments[0].scrollIntoView(false);", learn_more_button)
                learn_more_button.click()
                sleep(10)
            else:
                # post_to_slack("Landing Automation Ginseng IOS page Failed-learn more button")
                # post_to_opsgenie("Landing Automation Ginseng page IOS Failed-learn more button")
                # text = 'Landing Automation Ginseng page IOS Failed-learn more button'
                # recipients = ['dekela@shellanoo.com']
                # post_to_mail(recipients, text)
                now = datetime.now().strftime('Mobile Landing page IOS %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

            # driver.find_elements_by_class_name('btn-my')[0].click()
            # time.sleep(2)
            # driver.back()
            # time.sleep(2)
            # driver.find_elements_by_class_name('btn-my')[1].click()
            # time.sleep(5)

            # Third landing page #
            element = self.driver.find_element_by_id('name')
            self.driver.execute_script("arguments[0].scrollIntoView(false);", element)
            element.send_keys('Dekel Amram')
            sleep(1)
            self.driver.find_element_by_id('email').send_keys('dekel@shellanoo.com')
            button = self.driver.find_element_by_css_selector('.form-btn')
            button.click()
            sleep(10)
            pop_up = self.driver.find_element_by_css_selector('.btn')
            pop_up.click()
            sleep(3)

            # Forth landing page #
            if len(self.driver.find_elements_by_class_name('open_dialog')):
                buy_now = self.driver.find_elements_by_class_name('btn-my')[1]
                self.driver.execute_script("arguments[0].scrollIntoView(false);", buy_now)
                sleep(10)
                buy_now.click()
                sleep(10)
            else:
                # post_to_slack("Landing Automation Ginseng IOS page Failed-Buy Now button")
                # post_to_opsgenie("Landing Automation Ginseng page IOS Failed-Buy Now button")
                # text = 'Landing Automation Ginseng page IOS Failed-Buy Now button'
                # recipients = ['dekela@shellanoo.com']
                # post_to_mail(recipients, text)
                now = datetime.now().strftime('Mobile Landing page IOS %Y-%m-%d %H:%M')
                self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

                # Ginseng website #
            promo_code = self.driver.find_element_by_class_name('promo-code-check')
            self.driver.execute_script("arguments[0].scrollIntoView(false);", promo_code)
            promo_code.click()


            sleep(2)
            code = get_code_from_google_sheet()
            sleep(2)
            self.driver.find_element_by_id("stripe_promo_code").send_keys(code)
            sleep(5)
            self.driver.find_element_by_id('stripe_promo_code_button').click()
            sleep(2)
            button = self.driver.find_elements_by_class_name('strip_open')[0]
            action = webdriver.common.action_chains.ActionChains(self.driver)
            action.move_to_element_with_offset(button, 5, 5)
            # Have to scroll to the top in order for the checkout button to be in the screen bounds.
            self.driver.execute_script("window.scrollTo(0, 0);")
            action.click()
            action.perform()

            sleep(10)


            customer_details = get_customer_from_google_sheet()

            for i, detail in enumerate(customer_details):
                if i == 5:
                    Select(self.driver.find_elements_by_class_name('control')[i].find_element_by_tag_name(
                            'select')).select_by_visible_text(detail)
                    continue
                self.driver.find_elements_by_class_name('control')[i].send_keys(detail)

                # self.driver.find_elements_by_class_name('control')[0].send_keys('dekela@shellanoo.com')
                # self.driver.find_elements_by_class_name('control')[1].send_keys('Dekel Amram')
                # self.driver.find_elements_by_class_name('control')[2].send_keys('Maskit 25')
                # self.driver.find_elements_by_class_name('control')[3].send_keys('4600')
                # self.driver.find_elements_by_class_name('control')[4].send_keys('Herzelia')

            self.driver.find_element_by_class_name('iconContinue').click()

            self.driver.find_element_by_id('card_number').send_keys('4594')
            self.driver.find_element_by_id('card_number').send_keys('4083')
            self.driver.find_element_by_id('card_number').send_keys('3115')
            self.driver.find_element_by_id('card_number').send_keys('9870')
            self.driver.find_element_by_id('cc-exp').send_keys('03')
            self.driver.find_element_by_id('cc-exp').send_keys('20')
            self.driver.find_element_by_id('cc-csc').send_keys('680')
            self.driver.find_element_by_id('submitButton').click()
            sleep(15)

            if len(self.driver.find_elements_by_class_name('os-order-number__')):

                order_element = self.driver.find_element_by_class_name('os-order-number__')


                if order_element is not None:
                    is_ord_display = order_element.is_displayed()
                    if is_ord_display:
                        success = "Order Number, {order_id}, time:{time}".format(
                                order_id=order_element.text,
                                time=datetime.now().strftime('%Y-%m-%d %H:%M, Ginseng mobile successful,'))
                        write_to_log_file('ginseng', success)
                else:
                    # post_to_slack("Landing page-payment mobile successfully order id is missing")  # Slack is send if not succeed #
                    # post_to_opsgenie("Landing page-payment mobile failed")
                    # text = 'Landing page-Ginseng mobile IOS Failed'
                    # recipients = ['dekela@shellanoo.com']
                    # post_to_mail(recipients, text)
                    # now = datetime.now().strftime('Mobile Landing page IOS %Y-%m-%d %H:%M')
                    # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
                    sleep(10)
            else:
                # post_to_slack("Landing page-Automation Ginseng MOBILE Purchase failed order id is missing")  # Slack is send if not succeed #
                # post_to_opsgenie("Landing page-Automation Ginseng MOBILE Purchase failed order id is missing")
                # text = 'Landing page-Automation Ginseng MOBILE IOS Purchase failed order id is missing '
                # recipients = ['dekela@shellanoo.com']
                # post_to_mail(recipients, text)
                now = datetime.now().strftime('Landing page-Ginseng mobile IOS Failed')
                self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

            sleep(30)
        except BaseException as w:
            import traceback
            logging.error('Failed to do something: %s', w)
            traceback.print_exc()
            # post_to_slack("payment mobile is failed")  # Slack is send if not succeed #
            # post_to_opsgenie("payment failed")
            # text = 'Automation Mobile Ginseng Failed email'
            # recipients = ['dekela@shellanoo.com']
            # post_to_mail(recipients, text)
            # now = datetime.now().strftime('Mobile, %Y-%m-%d %H:%M')
            # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)


# self.driver.find_elements_by_class_name('field__input')[2].send_keys('Dekel')
#        self.driver.find_elements_by_class_name('field__input')[3].send_keys('Amram')
#        self.driver.find_elements_by_class_name('field__input')[4].send_keys('Mascit 25')
#        self.driver.find_elements_by_class_name('field__input')[6].send_keys('Herzelia')
#        self.driver.execute_script("window.scrollTo(0, 600);")
#        self.driver.find_elements_by_class_name('field__input')[9].send_keys('4600')
#        self.driver.find_elements_by_class_name('field__input')[10].send_keys('0547515457')
#        sleep(2)
#        self.driver.find_element_by_class_name('step__footer__continue-btn').click()
#        sleep(2)
#        self.driver.execute_script("window.scrollTo(0, 600);")
#        self.driver.find_element_by_class_name('step__footer__continue-btn').click()
#
#        sleep(4)
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('4594')
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('4001')
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('0346')
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('3705')
#        self.driver.find_elements_by_class_name('field__input')[3].send_keys('passportcard enabled')
#        self.driver.find_elements_by_class_name('field__input')[6].send_keys('10')
#        self.driver.find_elements_by_class_name('field__input')[6].send_keys('19')
#        self.driver.find_elements_by_class_name('field__input')[7].send_keys('782')
#        self.driver.execute_script("window.scrollTo(0, 600);")
#        self.driver.find_element_by_class_name('step__footer__continue-btn').click()
#        self.driver.execute_script("window.scrollTo(600, 0);")
#        sleep(40)


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


if __name__ == '__main__':
    try:
        suite = unittest.TestLoader().loadTestsFromTestCase(WebViewIOSTests)
        unittest.TextTestRunner(verbosity=2).run(suite)
        logging.basicConfig(level=logging.INFO)
        import traceback

    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        traceback.print_exc()
        # post_to_slack("payment mobile is failed")  # Slack is send if not succeed #
        # post_to_opsgenie("payment failed")
        # text = 'Automation Mobile Ginseng Failed email'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)