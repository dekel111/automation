import logging
import sys
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from helper_functions import write_to_log_file
from selenium.common.exceptions import TimeoutException
import requests
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os
from datetime import datetime, date
from random import randint
from selenium.webdriver.support.ui import Select
from landing_payment import pay_with_shopify, pay_with_stripe
import boto.ses

_log = logging.getLogger(__name__)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def get_order_id_strip(count=5):
    global driver
    y = 0
    order_element = None
    while y < count:
        y += 1
    try:
        order_element = WebDriverWait(driver, 10).until(
            lambda drive: driver.find_element_by_class_name('os-order-number__'))
    except TimeoutException as t_e:
        print ("Didn't found order ID {e}".format(e=t_e))

    return order_element


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def local_storage_uid():
    global user_id_from_local
    if user_id_from_local == driver.execute_script("return localStorage['uid']"):
        print ('local storage UID is OK ')
        print (user_id_from_local)
    else:
        print ('local storage UID is changed')
        # text = 'Landing Automation Ginseng-Local storage UID is changed'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)


def start_sanity_firefox():
    global driver
    global user_id_from_local
    driver = webdriver.Firefox()
    driver.get(site)
    driver.maximize_window()
    user_id_from_local = driver.execute_script("return localStorage['uid']")

    # First landing page #
    local_storage_uid()
    button = driver.find_element_by_css_selector('.second-m')
    button.click()
    driver.find_element_by_id("Weight").send_keys('70')
    driver.find_element_by_id("Height").send_keys('1.72')
    calculate_button = driver.find_element_by_class_name('first-step-btn')
    time.sleep(2)
    if calculate_button is not None:
        calculate_button.click()
        time.sleep(4)
    else:
        # post_to_slack("landing page Firefox- Calculate button is not clickable")
        # post_to_opsgenie("landing page Firefox- Calculate button is not clickable")
        # text = 'landing page Firefox- Calculate button is not clickable'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime('landing page Firefox-Calculate button is not clickable %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    # Second landing page #
    local_storage_uid()
    help_me_lose_weight_button = driver.find_element_by_class_name('btn').click()
    if help_me_lose_weight_button is not None:
        help_me_lose_weight_button.click()
        time.sleep(2)
    else:
        # post_to_slack("landing page Firefox- Help me lose weight button is missing")
        # post_to_opsgenie("landing page Firefox- Help me lose weight button is missing")
        # text = 'landing page Firefox- Help me lose weight button is missing'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime('landing page Firefox- Help me lose weight button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    # # Second landing page #
    # local_storage_uid()
    # if len(driver.find_elements_by_class_name('btn-my')):
    #     learn_more = randint(0, len(driver.find_elements_by_class_name('btn-my')) - 1)
    #     time.sleep(1)
    #     learn_more_button = driver.find_elements_by_css_selector('.btn-my')[learn_more]
    #     driver.execute_script("arguments[0].scrollIntoView(false);", learn_more_button)
    #     time.sleep(2)
    #     learn_more_button.click()
    # else:
    #     # post_to_slack("Landing Automation Ginseng page Failed-learn more button")
    #     # post_to_opsgenie("Landing Automation Ginseng page Failed-learn more button")
    #     # text = 'Landing Automation Ginseng page Failed-learn more button'
    #     # recipients = ['dekela@shellanoo.com']
    #     # post_to_mail(recipients, text)
    #     now = datetime.now().strftime('Landing page %Y-%m-%d %H:%M')
    #     driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    # Third landing page #
    local_storage_uid()
    element = driver.find_element_by_id('name')
    driver.execute_script("arguments[0].scrollIntoView(false);", element)
    element.send_keys('Dekel Amram')
    time.sleep(1)
    driver.find_element_by_id('email').send_keys('dekel@shellanoo.com')
    take_it_button = driver.find_element_by_class_name('form-btn')
    if take_it_button is not None:
        take_it_button.click()
    else:
        # post_to_slack("Landing Firefox Ginseng page Failed- Ill Take it button is not clickable")
        # post_to_opsgenie("Landing Firefox Ginseng page Failed- Ill Take it button is not clickable")
        # text = 'Landing Firefox Ginseng page Failed- Ill Take it button is not clickable'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime(
            'Landing Firefox Ginseng page Failed- Ill Take it button is not clickable %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
    time.sleep(3)
    pop_up = driver.find_element_by_css_selector('.btn')
    pop_up.click()
    time.sleep(5)

    # Fourth landing page #
    local_storage_uid()
    if len(driver.find_elements_by_class_name('btn')):
        # buy_now = randint(0, len(driver.find_elements_by_class_name('open_dialog')) - 1)
        element = driver.find_elements_by_css_selector('.btn')[1]
        driver.execute_script("arguments[0].scrollIntoView(false);", element)
        element.click()
        time.sleep(5)
    else:
        # post_to_slack("Landing Firefox Ginseng page Failed- Buy now button")
        # post_to_opsgenie("Landing Firefox Ginseng page Failed- Buy now button")
        # text = 'Landing Firefox Ginseng page Failed- Buy now button'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime('Landing Firefox Ginseng page Failed-Buy now button %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    # Fifth landing page #
    local_storage_uid()
    buy_now_packages = driver.find_elements_by_class_name('btn')
    if len(driver.find_elements_by_class_name('btn')):
        buy_now_package = randint(0, len(buy_now_packages) - 1)
        element = buy_now_packages[buy_now_package]
        driver.execute_script("arguments[0].scrollIntoView(false);", element)
        time.sleep(1)
        element.click()
    else:
        # post_to_slack("Landing Firefox Ginseng page Failed- Buy Now package is missing")
        # post_to_opsgenie("Landing Firefox Ginseng page Failed- Buy Now package is missing")
        # text = 'Landing Firefox Ginseng page Failed- Buy Now package is missing'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime(
            'Landing Firefox Ginseng page Failed Buy Now package is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    ## Ginseng website #
    # # Check price #
    # time.sleep(2)
    # price = str(driver.find_element_by_id('P_1').text).replace('$', '')
    # time.sleep(2)
    # price = float(price)
    # time.sleep(2)
    # price = float(price) - 10
    # sale_price = str(driver.find_element_by_id('P_11').text).replace('$', '')
    # sale_price = float(sale_price)
    # if price == sale_price:
    #     print ('Price after sale is OK')
    # else:
    #     print ('Price after sale is incorrect')
    # driver.find_element_by_class_name('sh-label').click()  # No need in Chrome ************ #
    # sale_price = float(sale_price) + 9
    # total = str(driver.find_element_by_class_name('stripe-total-amount').text).replace('$', '')
    # total = float(total)
    # if sale_price == total:
    #     print ('Total price after Express shipping is OK')
    # else:
    #     print ('Total price after Express shipping is Incorrect')
    # local_storage_uid()

    # Promo Code in pop up #
    # driver.find_element_by_class_name('promo-code-check').click()
    # code = get_code_from_google_sheet()
    # driver.find_element_by_id("stripe_promo_code").send_keys(code)
    # time.sleep(5)
    # driver.find_element_by_id("stripe_promo_code_button").click()


    # Ginseng website #

    current_url = driver.current_url

    checkout_button = driver.find_elements_by_class_name('check_out')[0]
    checkout_button.click()
    time.sleep(5)

    if current_url == driver.current_url:
        # Strip open and insert email/name/street etc.. #
        pay_with_stripe(driver)
    else:
        pay_with_shopify(driver)

    driver.close()


def start_sanity_chrome():
    global driver
    global user_id_from_local
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get(site)
    driver.set_window_size(1750, 1750)
    user_id_from_local = driver.execute_script("return localStorage['uid']")

    # First landing page #
    local_storage_uid()
    button = driver.find_element_by_css_selector('.second-m')
    button.click()
    driver.find_element_by_id("Weight").send_keys('70')
    driver.find_element_by_id("Height").send_keys('1.72')
    calculate_button = driver.find_element_by_class_name('first-step-btn')
    if calculate_button is not None:
        calculate_button.click()
        time.sleep(4)
    else:
        # post_to_slack("landing page Chrome-Calculate button is not clickable")
        # post_to_opsgenie("landing page Chrome-Calculate button is not clickable")
        # text = 'landing page Chrome-Calculate button is not clickable'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime('landing page Chrome-Calculate button is not clickable %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    # Second landing page #
    local_storage_uid()
    help_me_lose_weight_button = driver.find_element_by_class_name('btn').click()
    if help_me_lose_weight_button is not None:
        help_me_lose_weight_button.click()
        time.sleep(2)
    else:
        # post_to_slack("landing page Chrome-Help me lose weight button is missing")
        # post_to_opsgenie("landing page Chrome-Help me lose weight button is missing")
        # text = 'landing page Chrome-Help me lose weight button is missing'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime('landing page Chrome-Help me lose weight button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    # Third landing page #
    local_storage_uid()
    element = driver.find_element_by_id('name')
    driver.execute_script("arguments[0].scrollIntoView(false);", element)
    element.send_keys('Dekel Amram')
    time.sleep(1)
    driver.find_element_by_id('email').send_keys('dekel@shellanoo.com')
    take_it_button = driver.find_element_by_class_name('form-btn')

    if take_it_button is not None:
        take_it_button.click()
    else:
        # post_to_slack("Landing Chrome Ginseng page Failed- Ill Take it button is not clickable")
        # post_to_opsgenie("Landing Chrome Ginseng page Failed- Ill Take it button is not clickable")
        # text = 'Landing Chrome Ginseng page Failed- Ill Take it button is not clickable'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime(
            'Landing Chrome Ginseng page Failed- Ill Take it button is not clickable %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
    time.sleep(3)
    pop_up = driver.find_element_by_css_selector('.btn')
    pop_up.click()
    time.sleep(3)

    # Fourth landing page #
    local_storage_uid()
    if len(driver.find_elements_by_class_name('btn')):
        # buy_now = randint(0, len(driver.find_elements_by_class_name('open_dialog')) - 1)
        element = driver.find_elements_by_css_selector('.btn')[1]
        driver.execute_script("arguments[0].scrollIntoView(false);", element)
        element.click()
        time.sleep(5)
    else:
        # post_to_slack("Landing Chrome Ginseng page Failed- Buy now button")
        # post_to_opsgenie("Landing Chrome Ginseng page Failed- Buy now button")
        # text = 'Landing Chrome Ginseng page Failed- Buy now button'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime('Landing Chrome Ginseng page Failed- Buy now button %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)


    # Fifth landing page #
    local_storage_uid()
    buy_now_packages = driver.find_elements_by_class_name('btn')
    if len(driver.find_elements_by_class_name('btn')):
        buy_now_package = randint(0, len(buy_now_packages) - 1)
        element = buy_now_packages[buy_now_package]
        driver.execute_script("arguments[0].scrollIntoView(false);", element)
        time.sleep(1)
        element.click()
    else:
        # post_to_slack("Landing Chrome Ginseng page Failed- Buy Now package is missing")
        # post_to_opsgenie("Landing Chrome Ginseng page Failed- Buy Now package is missing")
        # text = 'Landing Chrome Ginseng page Failed- Buy Now package is missing'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
        now = datetime.now().strftime(
            'Landing Chrome Ginseng page Failed Buy Now package is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)


    # Ginseng website #
    local_storage_uid()
    # # Added QTY #
    # time.sleep(2)
    # driver.find_element_by_class_name('quantity-up').click()  # QTY = 2 #
    # time.sleep(2)
    # qty = driver.find_element_by_id('stripe-quantity').get_attribute("value")
    # qty = float(qty)
    # if qty != 2:
    #     print ('QTY is not ok, QTY should be 2')

    current_url = driver.current_url

    checkout_button = driver.find_elements_by_class_name('check_out')[0]
    checkout_button.click()

    time.sleep(5)

    if current_url == driver.current_url:
        pay_with_stripe(driver)

    else:
        pay_with_shopify(driver)

    driver.close()
    exit()


if __name__ == '__main__':
    try:
        sites = ["http://www.ginseng-life.com/wllp/", "http://www.ginseng-life.com/wllp/second-step-B.html",
                 "http://www.ginseng-life.com/wllp/third-step-A.html",
                 "http://www.ginseng-life.com/wllp/fourth-step.html"]

        for site in sites:
            logging.basicConfig(level=logging.INFO)
            logging.info('Starting automation on %s Site' % site)
            _log.info('Running both sanity_firefox and sanity_chrome')
            driver = None
            is_run_firefox = True
            is_run_chrome = True

            if is_run_firefox:
                start_sanity_firefox()
                is_run_firefox = False
            if is_run_chrome:
                start_sanity_chrome()
                is_run_chrome = False

        _log.info('All done.')
    except BaseException as e:
        logging.error('Failed to do something: %s', e)
        import traceback

        traceback.print_exc()
        m_now = datetime.now().strftime('Landing page %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % m_now)
