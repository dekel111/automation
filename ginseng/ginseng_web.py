import logging
import time
# import requests
import sys
from selenium import webdriver

_log = logging.getLogger(__name__)


def sanity_firefox():
    site = "https://ginseng.life/"

    driver = webdriver.Firefox()
    driver.get(site)

    logging.info('Starting automation on %s Site' % site)
    driver.maximize_window()
    # Product page #
    driver.find_elements_by_class_name("hover")[0].click()

    # Add product to cart#
    driver.find_element_by_id("product-add-to-cart").click()
    driver.find_elements_by_css_selector(".btn-secondary")[1].click()

    driver.find_elements_by_class_name("field__input")[1].send_keys("dekela@shellanoo.com")
    driver.find_elements_by_class_name("field__input")[2].send_keys("Dekel")
    driver.find_elements_by_class_name("field__input")[3].send_keys("Amram")
    driver.find_elements_by_class_name("field__input")[4].send_keys("Mascit 25")
    driver.find_elements_by_class_name("field__input")[6].send_keys("Herzelia")
    driver.find_elements_by_class_name("field__input")[9].send_keys("4600")
    driver.find_elements_by_class_name("field__input")[10].send_keys("0547515457")
    driver.find_element_by_class_name("step__footer__continue-btn").click()
    time.sleep(3)
    driver.find_element_by_class_name("step__footer__continue-btn ").click()

    time.sleep(5)

    # PAYMENT INFORMATION #
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("4594")
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("4001")
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("0346")
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("3705")

    # Insert Name of Card #
    driver.find_elements_by_css_selector(".field__input")[3].send_keys("passportcard enabled")

    # Insert Expiry #
    driver.find_elements_by_css_selector(".field__input")[6].send_keys("10")
    driver.find_elements_by_css_selector(".field__input")[6].send_keys("19")

    # Insert security code 3 digits #
    driver.find_elements_by_css_selector(".field__input")[7].send_keys("782")
    driver.find_element_by_class_name("step__footer__continue-btn").click()
    time.sleep(20)

    # r = requests.get("https://api.ginseng.life/send_email_on_error")  # Send email#
    driver.close()
    _log.info('Done with sanity_firefox')


def sanity_chrome():
    site = 'https://ginseng.life'

    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get(site)

    logging.info('Starting automation on %s Sites' % site)

    # Product page #
    driver.find_elements_by_class_name("hover")[0].click()

    # Add product to cart#
    driver.find_element_by_id("product-add-to-cart").click()
    time.sleep(1)
    driver.find_elements_by_class_name("btn-secondary")[1].click()

    # Shipping information #
    driver.find_elements_by_class_name("field__input")[1].send_keys("dekela@shellanoo.com")
    driver.find_elements_by_class_name("field__input")[2].send_keys("Dekel")
    driver.find_elements_by_class_name("field__input")[3].send_keys("Amram")
    driver.find_elements_by_class_name("field__input")[4].send_keys("Mascit 25")
    driver.find_elements_by_class_name("field__input")[6].send_keys("Herzelia")
    driver.find_elements_by_class_name("field__input")[9].send_keys("4600")
    driver.find_elements_by_class_name("field__input")[10].send_keys("0547515457")

    time.sleep(1)
    driver.find_element_by_class_name("step__footer__continue-btn").click()
    time.sleep(2)
    driver.find_element_by_class_name("step__footer__continue-btn ").click()

    # PAYMENT INFORMATION #
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("4594")
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("4001")
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("0346")
    driver.find_elements_by_css_selector(".field__input")[2].send_keys("3705")

    # Insert Name of Card #
    driver.find_elements_by_css_selector(".field__input")[3].send_keys("passportcard enabled")

    # Insert Expiry #
    driver.find_elements_by_css_selector(".field__input")[6].send_keys("10")
    driver.find_elements_by_css_selector(".field__input")[6].send_keys("19")

    # Insert security code 3 digits #
    driver.find_elements_by_css_selector(".field__input")[7].send_keys("782")
    time.sleep(4)
    driver.find_element_by_class_name("step__footer__continue-btn").click()
    time.sleep(20)
    # r = requests.get("https://api.ginseng.life/send_email_on_error")  # Send email#
    driver.close()

    _log.info('Done with sanity_chrome')


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    run_firefox = False
    run_chrome = False

    if len(sys.argv) > 1:
        if sys.argv[1] == 'firefox':
            _log.info('Running sanity_firefox')
            run_firefox = True
        elif sys.argv[1] == 'chrome':
            _log.info('Running sanity_chrome')
            run_chrome = True
        else:
            raise Exception('Unknown check {}'.format(sys.argv[1]))
    else:
        _log.info('Running both sanity_firefox and sanity_chrome')
        run_firefox = True
        run_chrome = True

    if run_firefox:
        sanity_firefox()

    if run_chrome:
        sanity_chrome()

    _log.info('All done.')
