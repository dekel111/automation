import os
import gspread
import unittest
from time import sleep
from selenium import webdriver
from datetime import datetime, date
from oauth2client.service_account import ServiceAccountCredentials
from random import randint
import logging
from selenium.webdriver.support.ui import Select
import requests
import boto.ses
from helper_functions import write_to_log_file
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import boto.ses
import random
import grequests
from config import app_config

PLATFORM_VERSION = '5.1.1'


def write_to_log_file_failed(log_record):
    path = "../screenshots/ginseng/android/"

    if not os.path.exists(path):
        os.makedirs(path)

    path += "/screenshot.txt"

    # if not os.path.isfile(path):
    #     open(path, 'w').close()

    with open(path, "a") as log_file:
        log_file.write(log_record + "\n")


def get_code_from_google_sheet():
    global driver
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def local_storage_uid(self, ):
    # global user_id_from_local
    if user_id_from_local == self.driver.execute_script("return localStorage['uid']"):
        print('local storage UID is OK')
        print(user_id_from_local)
    else:
        uid_changed = self.driver.execute_script("return localStorage['uid']")
        print('local storage UID is changed')
        print(uid_changed)
        # text = 'Landing Android Lollipop page 5 to Thank you- Local storage UID is changed'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)


class AndroidWebViewTests(unittest.TestCase):
    def setUp(self):
        global PLATFORM_VERSION
        desired_caps = {
            'browserName': 'Browser',
            'platformName': 'Android',
            'platformVersion': PLATFORM_VERSION,
            'deviceName': 'Nexus 5X API 22',
            'takesScreenshot': 'true'
            # 'appPackage': 'com.android.chrome',
            # 'appActivity': 'com.google.android.apps.chrome.ChromeTabbedActivity'
        }

        # if (PLATFORM_VERSION != '5.1.1'):
        #     desired_caps['automationName'] = 'selendroid'

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                       desired_caps)

    def test_get_url(self):

        try:
            global user_id_from_local
            sites = app_config['aws.fifth.urls']
            for site in sites:
                self.driver.get(site)
            # No need- displayed in config.yml file#
            # self.driver.get('https://www.ginseng-life.com/wllp/fifth.html')
            # self.driver.get('http://ginsenglanding.mmtst.com/wllp/')      # Test environment #
            sleep(4)
            user_id_from_local = self.driver.execute_script("return localStorage['uid']")
            sleep(3)

            # Fifth landing page #
            local_storage_uid(self, )
            buy_now_packages_buttons = self.driver.find_elements_by_class_name('btn')
            buy_buttons_len = len(buy_now_packages_buttons) - 1
            sleep(1)
            test_env = 0
            for index in range(buy_buttons_len):
                buy_now_packages_buttons = self.driver.find_elements_by_class_name('btn')
                buy_button = buy_now_packages_buttons[index]

                if buy_button.is_displayed():
                    buy_button.click()
                    sleep(2)
                    if app_config['stats.debug']:
                        if test_env == 0:
                            sleep(2)
                            self.driver.find_element_by_id('password').send_keys('shellanoo1$')
                            self.driver.find_element_by_class_name('btn').click()
                            self.driver.get(site)
                            test_env = 1
                    self.driver.get(site)

            ran_button = []
            buy_now_packages_buttons = self.driver.find_elements_by_class_name('btn')

            for button in buy_now_packages_buttons:
                if button.is_displayed():
                    sleep(2)
                    ran_button.append(button)
            current_url = self.driver.current_url
            sleep(1)
            buy_now_button = random.choice(ran_button)

            sleep(2)
            buy_now_button.click()
            sleep(7)

            # Only on Test environment ##############################################

            # sleep(4)
            # self.driver.find_element_by_id('password').send_keys('shellanoo1$')
            # sleep(2)
            # self.driver.find_element_by_class_name('btn').click()

            #########################################################################


            # Ginseng website #
            local_storage_uid(self, )
            sleep(3)

            if current_url != self.driver.current_url:
                iframe = len(self.driver.find_elements_by_tag_name("iframe"))
                if iframe == 1 or iframe == 0:
                    sleep(3)
                    local_storage_uid(self, )
                    sleep(2)
                    self.driver.find_elements_by_class_name('field__input')[1].send_keys('dekela@shellanoo.com')
                    self.driver.find_elements_by_class_name('field__input')[2].send_keys('Dekel')
                    self.driver.find_elements_by_class_name('field__input')[3].send_keys('Amram')
                    self.driver.find_elements_by_class_name('field__input')[4].send_keys('Maskit 25')
                    self.driver.find_elements_by_class_name('field__input')[6].send_keys('Herzelia')
                    self.driver.find_elements_by_class_name('field__input')[9].send_keys('4600')
                    self.driver.find_elements_by_class_name('field__input')[10].send_keys('0547515457')
                    sleep(1)
                    continue_to_shipping_method_button = self.driver.find_element_by_css_selector(
                        '.step__footer__continue-btn')
                    sleep(1)
                    self.driver.execute_script("arguments[0].scrollIntoView(true);", continue_to_shipping_method_button)
                    sleep(2)
                    if continue_to_shipping_method_button.is_displayed():
                        continue_to_shipping_method_button.click()
                    else:
                        post_to_slack(
                            "Landing Android Lollipop page 5 to Thank you Failed, Continue to Shipping Method button is missing")
                        post_to_opsgenie(
                            "Landing Android Lollipop page 5 to Thank you Failed, Continue to Shipping Method button is missing")
                        text = 'Landing Android Lollipop page 5 to Thank you Failed, Continue to Shipping Method button is missing'
                        recipients = ['dekela@shellanoo.com']
                        post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Landing Android Lollipop page 5 to Thank you Failed, Continue to Shipping Method button is missing %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)
                    sleep(3)

                    self.driver.find_elements_by_css_selector('.input-radio')[1].click()
                    sleep(2)

                    continue_to_payment_information = self.driver.find_element_by_css_selector(
                        '.step__footer__continue-btn')
                    if continue_to_payment_information.is_displayed():
                        continue_to_payment_information.click()
                    else:
                        post_to_slack(
                            "Landing Android Lollipop page 5 to Thank you Failed, Continue_to_payment_information button is missing")
                        post_to_opsgenie(
                            "Landing Android Lollipop page 5 to Thank you Failed, Continue_to_payment_information button is missing")
                        text = 'Landing Android Lollipop page 5 to Thank you Failed, Continue_to_payment_information button is missing'
                        recipients = ['dekela@shellanoo.com']
                        post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Landing Android Lollipop page 5 to Thank you Failed, Continue_to_payment_information button is missing %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)

                    sleep(5)
                    local_storage_uid(self, )
                    sleep(3)
                    code = get_code_from_google_sheet()
                    sleep(5)
                    self.driver.find_elements_by_class_name("field__input")[1].send_keys(code)
                    sleep(2)
                    apply_coupon_button = self.driver.find_elements_by_css_selector('.field__input-btn')[1]
                    sleep(1)
                    if apply_coupon_button.is_displayed():
                        apply_coupon_button.click()
                    else:
                        post_to_slack(
                            "Landing Android Lollipop page 5 to Thank you Failed, Apply Coupon button is missing")
                        post_to_opsgenie(
                            "Landing Android Lollipop page 5 to Thank you Failed, Apply Coupon button is missing")
                        text = 'Landing Android Lollipop page 5 to Thank you Failed, Apply Coupon button is missing'
                        recipients = ['dekela@shellanoo.com']
                        post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Landing Android Lollipop page 5 to Thank you Failed, Apply Coupon button is missing %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)
                    sleep(7)

                    if len(self.driver.find_elements_by_css_selector("#number")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[0])
                    sleep(2)
                    for digists in app_config['card.digits']:
                        self.driver.find_element_by_id('number').send_keys(digists)
                    # self.driver.find_element_by_id('number').send_keys(4594)
                    # self.driver.find_element_by_id('number').send_keys(4083)
                    # self.driver.find_element_by_id('number').send_keys(3115)
                    # self.driver.find_element_by_id('number').send_keys(9870)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#name")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[1])
                        sleep(1)
                    self.driver.find_element_by_id('name').send_keys("passportcard enabled")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#expiry")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[2])
                        sleep(1)
                    self.driver.find_element_by_id('expiry').send_keys("03")
                    self.driver.find_element_by_id('expiry').send_keys("20")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#expiry")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[3])
                        sleep(1)
                    self.driver.find_element_by_id('verification_value').send_keys("680")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(2)
                    self.driver.execute_script("window.scrollTo(0, 600);")
                    sleep(1)

                    place_order = self.driver.find_element_by_class_name('step__footer__continue-btn')
                    self.driver.execute_script("arguments[0].scrollIntoView(true);", place_order)
                    sleep(2)
                    if place_order.is_displayed():
                        place_order.click()
                    else:
                        post_to_slack(
                            "Landing Android Lollipop page 5 to Thank you Failed, Place Order button is missing")
                        post_to_opsgenie(
                            "Landing Android Lollipop page 5 to Thank you Failed, Place Order button is missing")
                        text = 'Landing Android Lollipop page 5 to Thank you Failed, Place Order button is missing'
                        recipients = ['dekela@shellanoo.com']
                        post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Landing Android Lollipop page 5 to Thank you Failed, Place Order button is missing %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)
                    sleep(18)

                    order_element = self.driver.find_element_by_class_name('os-order-number')
                    sleep(3)
                    local_storage_uid(self, )
                    sleep(2)
                    if order_element is not None:
                        is_ord_display = order_element.is_displayed()
                        if is_ord_display:
                            success = "{order_id}, time:{time}".format(
                                order_id=order_element.text,
                                time=datetime.now().strftime(
                                    '%Y-%m-%d %H:%M, Ginseng landing mobile Android Lollipop Shopify successful,'))
                            write_to_log_file('ginseng', success)
                    else:
                        post_to_slack(
                            "Payment Landing Android Lollipop page 5 to Thank You Failed, Shopify successfully order id is missing")
                        post_to_opsgenie(
                            "Payment Landing Android Lollipop page 5 to Thank You Failed, Shopify successfully order id is missing")
                        text = 'Payment Landing Android Lollipop page 5 to Thank You Failed, Shopify successfully order id is missing'
                        recipients = ['dekela@shellanoo.com']
                        post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Payment Landing Android Lollipop page 5 to Thank You Failed, Shopify Failed, %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)
                        # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #
                        sleep(10)
            else:
                customer_details = get_customer_from_google_sheet()
                if customer_details is not None:
                    self.driver.switch_to.window(self.driver.window_handles[1])
                    sleep(2)
                else:
                    # post_to_slack(
                    #     "Customer details is missing, Landing Android Lollipop page 5 to Thank you Failed")
                    # post_to_opsgenie("Customer details is missing, Landing Android Lollipop page 5 to Thank you Failed")
                    # text = 'Customer details is missing, Landing Android Lollipop page 5 to Thank you Failed'
                    # recipients = ['dekela@shellanoo.com']
                    # post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Customer details is missing-Landing Android Lollipop page 5 to Thank you Failed, %Y-%m-%d %H:%M')
                    write_to_log_file_failed(now)

                # print (self.driver.page_source)

                for i, detail in enumerate(customer_details):
                    if i == 5:
                        Select(self.driver.find_elements_by_class_name('control')[i].find_element_by_tag_name(
                            'select')).select_by_visible_text(detail)
                        continue
                    self.driver.find_elements_by_class_name('control')[i].send_keys(detail)

                self.driver.find_element_by_class_name('iconContinue').click()

                self.driver.find_element_by_id('card_number').send_keys('4594')
                self.driver.find_element_by_id('card_number').send_keys('4083')
                self.driver.find_element_by_id('card_number').send_keys('3115')
                self.driver.find_element_by_id('card_number').send_keys('9870')
                self.driver.find_element_by_id('cc-exp').send_keys('03')
                self.driver.find_element_by_id('cc-exp').send_keys('20')
                self.driver.find_element_by_id('cc-csc').send_keys('680')
                self.driver.find_element_by_id('submitButton').click()
                sleep(12)
                self.driver.switch_to.window(self.driver.window_handles[0])
                local_storage_uid(self, )
                if len(self.driver.find_elements_by_class_name('os-order-number__')):
                    order_element = self.driver.find_element_by_class_name('os-order-number__')

                    if order_element is not None:
                        is_ord_display = order_element.is_displayed()
                        if is_ord_display:
                            success = "{order_id}, time:{time}".format(
                                order_id=order_element.text,
                                time=datetime.now().strftime(
                                    '%Y-%m-%d %H:%M, Ginseng landing Mobile Android Lollipop Stripe successful,'))
                            write_to_log_file('ginseng', success)
                    else:
                        # post_to_slack(
                        #     "Payment Landing Mobile Stripe page 5 to Thank You Android Lollipop successfully order id is missing")
                        # post_to_opsgenie(
                        #     "Payment Landing Mobile Stripe page 5 to Thank You Android Lollipop successfully order id is missing")
                        # text = 'Payment Landing Mobile Stripe page 5 to Thank You Android Lollipop successfully order id is missing'
                        # recipients = ['dekela@shellanoo.com']
                        # post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Payment Landing Mobile Stripe page 5 to Thank You Android Lollipop successfully order id is missing, %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)
                        # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #
                        sleep(10)
                else:
                    # post_to_slack(
                    #     "Landing Mobile Android page 5 to Thank You Lollipop Purchase failed, order id is missing")
                    # post_to_opsgenie(
                    #     "Landing Mobile Android page 5 to Thank You Lollipop Purchase failed, order id is missing")
                    # text = 'Landing Mobile Android page 5 to Thank You Lollipop Purchase failed, order id is missing'
                    # recipients = ['dekela@shellanoo.com']
                    # post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing Mobile Android page 5 to Thank You Lollipop Purchase failed, OrderID is missing, %Y-%m-%d %H:%M')
                    write_to_log_file_failed(now)
                    # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #
            grequests.get('http://api.ginseng.life/orders/delete-shellanoo-orders-from-shopify')
            sleep(10)
        except BaseException as w:
            import traceback
            logging.error('Failed to do something: %s', w)
            traceback.print_exc()
            post_to_slack("Payment landing Mobile Android Lollipop page 5 to Thank You is failed")
            post_to_opsgenie("Payment landing Mobile Android Lollipop page 5 to Thank You is failed")
            text = 'Payment landing Mobile Android Lollipop page 5 to Thank You is failed'
            recipients = ['dekela@shellanoo.com']
            post_to_mail(recipients, text)
            now = datetime.now().strftime(
                'Payment landing Mobile Android Lollipop page 5 to Thank You is failed, %Y-%m-%d %H:%M')
            write_to_log_file_failed(now)
            # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #
            sleep(15)
            self.driver.quit()

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AndroidWebViewTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
