import os
import gspread
import unittest
from time import sleep
from selenium import webdriver
from datetime import datetime, date
from oauth2client.service_account import ServiceAccountCredentials
from random import randint
import logging
from selenium.webdriver.support.ui import Select
import requests
import boto.ses
from helper_functions import write_to_log_file
from selenium.webdriver.common.action_chains import ActionChains


class WebViewIOSTests(unittest.TestCase):
    capabilities = {'browserName': 'Safari', "deviceName": "iPhone 6", "platformVersion": "9.2"}
    driver = webdriver.Remote('http://localhost:4723/wd/hub', capabilities)

    # driver = webdriver.Remote('http://ginseng.life', capabilities)

    def tearDown(self):
        self.driver.quit()

    def test_get_url(self):
        try:
            self.driver.get('http://ginseng.life')
            sleep(4)

            #        self.driver.find_element_by_class_name('toggle_menu').click()
            #        sleep(10)

            self.driver.execute_script("window.scrollTo(0, 600);")
            sleep(2)

            # self.driver.find_elements_by_class_name('add-to-cart-btn-link')[24].click()
            prod_list = self.driver.find_elements_by_css_selector('.widget-new-product .add-to-cart-btn-link')
            if prod_list is not None:
                prod_index = randint(len(prod_list) / 2, len(prod_list) - 1)
                element = prod_list[prod_index]
                element.click()
            else:
                # post_to_slack("Products are missing, mobile IOS is failed")  # Slack is send if not succeed #
                # post_to_opsgenie("Products are missing mobile IOS failed")
                text = 'Automation Mobile IOS Ginseng, Products are missing'
                # recipients = ['dekela@shellanoo.com']
                # post_to_mail(recipients, text)

            sleep(2)
            self.driver.execute_script("window.scrollTo(0, 600);")
            sleep(2)
            self.driver.find_element_by_id('product-add-to-cart1').click()
            sleep(2)
            # self.driver.execute_script("window.scrollTo(0, 800);")
            # sleep(2)
            # self.driver.find_element_by_class_name('promo_code_toggle').click()
            # sleep(2)
            # self.driver.find_element_by_id('checkout_reduction_code').send_keys(get_code_from_google_sheet())
            # self.driver.find_element_by_id("promo_code_button").click()
            sleep(3)

            current_url = self.driver.current_url
            sleep(2)

            button = self.driver.find_elements_by_class_name('check_out')[0]

            action = webdriver.common.action_chains.ActionChains(self.driver)
            action.move_to_element_with_offset(button, 5, 5)
            # Have to scroll to the top in order for the checkout button to be in the screen bounds.
            self.driver.execute_script("window.scrollTo(0, 0);")
            action.click()
            action.perform()
            sleep(7)
            if current_url != self.driver.current_url:
                # self.driver.find_elements_by_class_name('btn')[2].click()
                iframe = len(self.driver.find_elements_by_tag_name("iframe"))
                if iframe == 1 or iframe == 0:
                    sleep(1)
                    self.driver.find_elements_by_class_name('field__input')[1].send_keys('dekela@shellanoo.com')
                    self.driver.find_elements_by_class_name('field__input')[2].send_keys('Dekel')
                    self.driver.find_elements_by_class_name('field__input')[3].send_keys('Amram')
                    self.driver.find_elements_by_class_name('field__input')[4].send_keys('Maskit 25')
                    self.driver.find_elements_by_class_name('field__input')[6].send_keys('Herzelia')
                    self.driver.find_elements_by_class_name('field__input')[9].send_keys('4600')
                    self.driver.find_elements_by_class_name('field__input')[10].send_keys('0547515457')
                    sleep(1)
                    continue_button = self.driver.find_elements_by_css_selector('.btn__content')[2]
                    sleep(1)
                    self.driver.execute_script("arguments[0].scrollIntoView(true);", continue_button)
                    continue_button.click()
                    sleep(6)
                    self.driver.find_element_by_css_selector('.step__footer__continue-btn').click()
                    sleep(5)

                    code = get_code_from_google_sheet()
                    sleep(5)
                    self.driver.find_elements_by_class_name("field__input")[1].send_keys(code)
                    sleep(2)
                    self.driver.find_elements_by_class_name("btn")[1].click()
                    sleep(5)
                    # self.driver.switch_to.default_content()
                    # Paypal#
                    iframe = len(self.driver.find_elements_by_tag_name("iframe"))
                    self.driver.find_element_by_id('checkout_payment_gateway_82445830').click()
                    self.driver.find_elements_by_class_name('btn')[2].click()
                    sleep(10)
                    # self.driver.find_element_by_id('email').send_keys('q1@shellanoo.com')
                    # self.driver.find_element_by_id('password').send_keys('qa1')
                    self.driver.find_element_by_id('btnLogin').click()




                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#number")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[0])

                    sleep(2)
                    self.driver.find_element_by_id('number').send_keys(4594)
                    self.driver.find_element_by_id('number').send_keys(4083)
                    self.driver.find_element_by_id('number').send_keys(3115)
                    self.driver.find_element_by_id('number').send_keys(9870)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#name")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[1])
                        sleep(1)
                    self.driver.find_element_by_id('name').send_keys("passportcard enabled")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#expiry")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[2])
                        sleep(1)
                    self.driver.find_element_by_id('expiry').send_keys("03")
                    self.driver.find_element_by_id('expiry').send_keys("20")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#expiry")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[3])
                        sleep(1)
                    self.driver.find_element_by_id('verification_value').send_keys("680")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(2)
                    self.driver.execute_script("window.scrollTo(0, 600);")
                    self.driver.find_element_by_class_name('step__footer__continue-btn').click()
                    sleep(25)

                    order_element = self.driver.find_element_by_class_name('os-order-number__')

                    if order_element is not None:
                        is_ord_display = order_element.is_displayed()
                        if is_ord_display:
                            success = "{order_id}, time:{time}".format(
                                order_id=order_element.text,
                                time=datetime.now().strftime('%Y-%m-%d %H:%M, Ginseng mobile IOS Shopify successful,'))
                            write_to_log_file('ginseng', success)
                    else:
                        # post_to_slack("Payment mobile IOS Shopify successfully order id is missing")
                        # post_to_opsgenie("Payment mobile IOS Shopify failed")
                        text = 'Automation Ginseng mobile IOS Shopify Failed'
                        # recipients = ['dekela@shellanoo.com']
                        # post_to_mail(recipients, text)
                        now = datetime.now().strftime('Mobile IOS, %Y-%m-%d %H:%M')
                        # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
                        sleep(10)




            else:
                customer_details = get_customer_from_google_sheet()

                for i, detail in enumerate(customer_details):
                    if i == 5:
                        Select(self.driver.find_elements_by_class_name('control')[i].find_element_by_tag_name(
                            'select')).select_by_visible_text(detail)
                        continue
                    self.driver.find_elements_by_class_name('control')[i].send_keys(detail)

                    # self.driver.find_elements_by_class_name('control')[0].send_keys('dekela@shellanoo.com')
                # self.driver.find_elements_by_class_name('control')[1].send_keys('Dekel Amram')
                # self.driver.find_elements_by_class_name('control')[2].send_keys('Maskit 25')
                # self.driver.find_elements_by_class_name('control')[3].send_keys('4600')
                # self.driver.find_elements_by_class_name('control')[4].send_keys('Herzelia')

                self.driver.find_element_by_class_name('iconContinue').click()

                self.driver.find_element_by_id('card_number').send_keys('4594')
                self.driver.find_element_by_id('card_number').send_keys('4083')
                self.driver.find_element_by_id('card_number').send_keys('3115')
                self.driver.find_element_by_id('card_number').send_keys('9870')
                self.driver.find_element_by_id('cc-exp').send_keys('03')
                self.driver.find_element_by_id('cc-exp').send_keys('20')
                self.driver.find_element_by_id('cc-csc').send_keys('680')
                self.driver.find_element_by_id('submitButton').click()
                sleep(25)

                if len(self.driver.find_elements_by_class_name('os-order-number__')):

                    order_element = self.driver.find_element_by_class_name('os-order-number__')

                    if order_element is not None:
                        is_ord_display = order_element.is_displayed()
                        if is_ord_display:
                            success = "{order_id}, time:{time}".format(
                                order_id=order_element.text,
                                time=datetime.now().strftime('%Y-%m-%d %H:%M, Ginseng mobile IOS successful,'))
                            write_to_log_file('ginseng', success)
                    else:
                        # post_to_slack("Payment mobile IOS successfully order id is missing")  # Slack is send if not succeed #
                        # post_to_opsgenie("Payment mobile IOS failed")
                        text = 'Automation Ginseng mobile IOS Failed'
                        # recipients = ['dekela@shellanoo.com']
                        # post_to_mail(recipients, text)
                        now = datetime.now().strftime('Mobile IOS, %Y-%m-%d %H:%M')
                        # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
                        sleep(10)
                else:
                    # post_to_slack("Automation Ginseng MOBILE IOS Purchase failed order id is missing")  # Slack is send if not succeed #
                    # post_to_opsgenie("Automation Ginseng MOBILE IOS Purchase failed order id is missing")
                    text = 'Automation Ginseng MOBILE IOS Purchase failed order id is missing '
                    # recipients = ['dekela@shellanoo.com']
                    # post_to_mail(recipients, text)
                    now = datetime.now().strftime('Mobile IOS, %Y-%m-%d %H:%M')
                    # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

            sleep(30)
        except BaseException as w:
            import traceback
            logging.error('Failed to do something: %s', w)
            # traceback.print_exc()
            # post_to_slack("Payment mobile IOS is failed")  # Slack is send if not succeed #
            # post_to_opsgenie("Payment mobile IOS failed")
            text = 'Automation Mobile IOS Ginseng Failed email'
            # recipients = ['dekela@shellanoo.com']
            # post_to_mail(recipients, text)
            now = datetime.now().strftime('Mobile IOS, %Y-%m-%d %H:%M')
            # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)


# self.driver.find_elements_by_class_name('field__input')[2].send_keys('Dekel')
#        self.driver.find_elements_by_class_name('field__input')[3].send_keys('Amram')
#        self.driver.find_elements_by_class_name('field__input')[4].send_keys('Mascit 25')
#        self.driver.find_elements_by_class_name('field__input')[6].send_keys('Herzelia')
#        self.driver.execute_script("window.scrollTo(0, 600);")
#        self.driver.find_elements_by_class_name('field__input')[9].send_keys('4600')
#        self.driver.find_elements_by_class_name('field__input')[10].send_keys('0547515457')
#        sleep(2)
#        self.driver.find_element_by_class_name('step__footer__continue-btn').click()
#        sleep(2)
#        self.driver.execute_script("window.scrollTo(0, 600);")
#        self.driver.find_element_by_class_name('step__footer__continue-btn').click()
#
#        sleep(4)
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('4594')
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('4001')
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('0346')
#        self.driver.find_elements_by_class_name('field__input')[2].send_keys('3705')
#        self.driver.find_elements_by_class_name('field__input')[3].send_keys('passportcard enabled')
#        self.driver.find_elements_by_class_name('field__input')[6].send_keys('10')
#        self.driver.find_elements_by_class_name('field__input')[6].send_keys('19')
#        self.driver.find_elements_by_class_name('field__input')[7].send_keys('782')
#        self.driver.execute_script("window.scrollTo(0, 600);")
#        self.driver.find_element_by_class_name('step__footer__continue-btn').click()
#        self.driver.execute_script("window.scrollTo(600, 0);")
#        sleep(40)


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


if __name__ == '__main__':
    try:
        suite = unittest.TestLoader().loadTestsFromTestCase(WebViewIOSTests)
        unittest.TextTestRunner(verbosity=2).run(suite)
        logging.basicConfig(level=logging.INFO)
        import traceback

    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        # traceback.print_exc()
        # post_to_slack("Payment mobile IOS is failed")  # Slack is send if not succeed #
        # post_to_opsgenie("Payment mobile IOS failed")
        text = 'Automation Mobile IOS Ginseng Failed email'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)
