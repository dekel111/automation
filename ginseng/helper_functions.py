import os
import gspread
import requests
from oauth2client.service_account import ServiceAccountCredentials


def get_google_sheet_values_by_id(doc_id):
    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    return worksheet.get_all_values()


def write_to_log_file(project, log_record):
    path = "../log/{project}/log.log".format(project=project)
    # if not os.path.isfile(path):
    #     open(path, 'w').close()

    with open(path, "a") as log_file:
        log_file.write(log_record + "\n")


def read_log_file():
    path = "../log/{project}/log.log".format(project='ginseng')
    thefile = open(path, 'r')
    file = thefile.readlines()
    thefile.close()

    for line in file:
        line_array = line.split(',')
        for col in line_array:
            print (col)
