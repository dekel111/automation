import time
from selenium.webdriver.support.wait import WebDriverWait
from helper_functions import write_to_log_file
from selenium.common.exceptions import TimeoutException
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os
from datetime import datetime, date
from random import randint
from selenium.webdriver.support.ui import Select
import requests
import boto.ses
from config import app_config


def pay_with_stripe_5_new(driver):
    iframe = len(driver.find_elements_by_tag_name("iframe"))
    if iframe is not None:
        driver.switch_to.frame(
            driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 1])
        time.sleep(4)
    else:
        customer_details = get_customer_from_google_sheet()

        keys = ["Email", "Name", "Street", "Postcode", "City", "Country"]

        inputs_array = driver.find_elements_by_class_name('Fieldset-input')

        for index, current_input in enumerate(inputs_array):
            if index == 5:
                Select(current_input).select_by_visible_text(customer_details[5])
                continue
            current_input.send_keys(customer_details[keys.index(current_input.get_attribute("placeholder"))])
            time.sleep(2)
            driver.find_element_by_class_name('Button-content').click()

    time.sleep(4)
    customer_details_1 = get_customer_from_google_sheet()

    keys = ["Email", "Name", "Street", "Postcode", "City", "Country"]

    inputs_array = driver.find_elements_by_class_name('Fieldset-input')

    for index, current_input in enumerate(inputs_array):
        if index == 5:
            Select(current_input).select_by_visible_text(customer_details_1[5])
            continue
        current_input.send_keys(customer_details_1[keys.index(current_input.get_attribute("placeholder"))])

    time.sleep(2)
    driver.find_element_by_class_name('Button-content').click()
    driver.switch_to.default_content()
    time.sleep(2)

    # Credit card #
    iframe_credit = len(driver.find_elements_by_tag_name("iframe"))
    if iframe_credit is not None:
        driver.switch_to.frame(
            driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 1])
        time.sleep(2)
    else:
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4594)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4083)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(3115)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(9870)
        driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('03')
        driver.find_elements_by_class_name('Fieldset-input')[1].send_keys(20)
        driver.find_elements_by_class_name('Fieldset-input')[2].send_keys(680)
        time.sleep(2)
        driver.find_element_by_class_name('Button-content').click()
        driver.switch_to.default_content()
        time.sleep(20)

    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4594)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4083)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(3115)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(9870)
    driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('03')
    driver.find_elements_by_class_name('Fieldset-input')[1].send_keys(20)
    driver.find_elements_by_class_name('Fieldset-input')[2].send_keys(680)
    time.sleep(2)
    driver.find_element_by_class_name('Button-content').click()
    driver.switch_to.default_content()
    time.sleep(20)

    # Get Order ID #
    local_storage_uid(user_id_from_local, driver)
    order_element = get_order_id_strip(5)
    if order_element is not None:
        is_ord_display = order_element.is_displayed()
        if is_ord_display:
            success = "{order_id}, time:{time}".format(
                order_id=order_element.text,
                time=datetime.now().strftime('%Y-%m-%d %H:%M, Landing Ginseng Stripe page 5 NEW successful,'))
            write_to_log_file('ginseng', success)
    else:
        post_to_slack("Landing Stripe page 5 NEW payment successfully- order id is missing")
        post_to_opsgenie("Landing Stripe page 5 NEW payment successfully- order id is missing")
        text = 'Landing Stripe page 5 NEW payment successfully- order id is missing'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)
        now = datetime.now().strftime(
            'Landing Stripe page 5 NEW payment successfully- order id is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
        time.sleep(5)


def pay_with_shopify_5_new(driver, user_id_from_local):
    time.sleep(2)
    iframe = len(driver.find_elements_by_tag_name("iframe"))
    if iframe is not None:
        # if iframe == 2:
        #     driver.switch_to.frame(
        #         driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 1])
        time.sleep(4)
        # elif iframe == 3:
        #     driver.switch_to.frame(
        #         driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 2])
        if iframe == 0 or iframe == 1 or iframe == 2 or iframe == 3:
            time.sleep(2)
            local_storage_uid(user_id_from_local, driver)
            time.sleep(2)
            driver.find_element_by_id('checkout_email').send_keys('dekela@shellanoo.com')
            driver.find_element_by_id('checkout_shipping_address_first_name').send_keys('Dekel')
            driver.find_element_by_id('checkout_shipping_address_last_name').send_keys('Amram')
            driver.find_element_by_id('checkout_shipping_address_address1').send_keys('Maskit 25')
            driver.find_element_by_id('checkout_shipping_address_city').send_keys('Herzelia')
            driver.find_element_by_id('checkout_shipping_address_zip').send_keys('4600')
            driver.find_element_by_id('checkout_shipping_address_phone').send_keys('0547515457')
            driver.execute_script("window.scrollTo(0, 9000)")
            time.sleep(2)

            continue_to_shipping_method_button = driver.find_element_by_css_selector('.step__footer__continue-btn')
            time.sleep(3)
            if continue_to_shipping_method_button.is_displayed():
                continue_to_shipping_method_button.click()
            else:
                if not app_config['stats.debug']:
                    post_to_slack(
                        "Landing Chrome Ginseng page 5 NEW, Continue to Shipping Method button is missing")
                    post_to_opsgenie(
                        "Landing Chrome Ginseng page 5 NEW, Continue to Shipping Method button is missing")
                    text = 'Landing Chrome Ginseng page 5 NEW, Continue to Shipping Method button is missing'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing Chrome Ginseng page 5 NEW, Continue to Shipping Method button is missing %Y-%m-%d %H:%M')
                    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

            time.sleep(4)
            browser_name = str(driver.capabilities['browserName'])
            time.sleep(2)
            if browser_name == 'chrome':
                time.sleep(2)
                driver.find_elements_by_css_selector('.input-radio')[1].click()
            else:
                time.sleep(2)

            continue_to_payment_information_button = driver.find_element_by_css_selector('.step__footer__continue-btn')
            time.sleep(2)
            if continue_to_payment_information_button.is_displayed():
                continue_to_payment_information_button.click()
            else:
                if not app_config['stats.debug']:
                    post_to_slack(
                        "Landing Chrome Ginseng page 5 NEW, Continue to Payment Information button is missing")
                    post_to_opsgenie(
                        "Landing Chrome Ginseng page 5 NEW, Continue to Payment Information button is missing")
                    text = 'Landing Chrome Ginseng page 5 NEW, Continue to Payment Information button is missing'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing Chrome Ginseng page 5 NEW, Continue to Payment Information button is missing %Y-%m-%d %H:%M')
                    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

            time.sleep(3)
            local_storage_uid(user_id_from_local, driver)
            time.sleep(3)

            code = get_code_from_google_sheet()
            driver.find_element_by_id("checkout_reduction_code").send_keys(code)
            apply_coupon_button = driver.find_elements_by_css_selector('.field__input-btn')[0]
            time.sleep(1)
            if apply_coupon_button.is_displayed():
                apply_coupon_button.click()
            else:
                if not app_config['stats.debug']:
                    post_to_slack("Landing Chrome Ginseng page 5 NEW, Apply Coupon button is missing")
                    post_to_opsgenie("Landing Chrome Ginseng page 5 NEW, Apply Coupon button is missing")
                    text = 'Landing Chrome Ginseng page 5 NEW, Apply Coupon button is missing'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing Chrome Ginseng page 5 NEW, Apply Coupon button is missing %Y-%m-%d %H:%M')
                    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

            time.sleep(5)

            local_storage_uid(user_id_from_local, driver)
            time.sleep(3)
            # Checking if prod or test

            # driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[0])  # No Need for netpay #

            for digists in app_config['card.digits']:
                driver.find_element_by_id('CardNum').send_keys(digists)

            # driver.switch_to.default_content()  # No need for netpay #
            time.sleep(2)

            # if len(driver.find_elements_by_css_selector("#name")) == 0:
            #     driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[1])
            time.sleep(2)
            driver.find_element_by_id('Member').send_keys(app_config['card.name'])
            # driver.switch_to.default_content()  # No need for netpay #
            time.sleep(2)

            # if len(driver.find_elements_by_css_selector("#expiry")) == 0: # No need for netpay #
            # driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[1]) # No need for netpay #
            #     time.sleep(2)
            driver.find_element_by_css_selector('.card-date-field #expiry').send_keys('0320')
            # driver.switch_to.default_content()  # No need for netpay #
            time.sleep(2)

            # if len(driver.find_elements_by_css_selector("#expiry")) == 0: # No need for netpay #
            #     driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[3])  # No need for netpay #
            time.sleep(2)
            driver.find_element_by_id('CVV2').send_keys(app_config['cw.code'])
            # driver.find_element_by_id('verification_value').send_keys(680)
            time.sleep(1)
            # driver.switch_to.default_content()  # No need for netpay #
            time.sleep(2)
            driver.execute_script("window.scrollTo(0, 9000)")
            time.sleep(2)

            place_order_button = driver.find_element_by_css_selector('.step__footer__continue-btn')
            time.sleep(1)
            if place_order_button.is_displayed():
                place_order_button.click()
            else:
                if not app_config['stats.debug']:
                    post_to_slack("Landing Chrome Ginseng page 5 NEW, Place Order button is missing")
                    post_to_opsgenie("Landing Chrome Ginseng page 5 NEW, Place Order button is missing")
                    text = 'Landing Chrome Ginseng page 5 NEW, Place Order button is missing'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing Chrome Ginseng page 5 NEW, Place Order button is missing %Y-%m-%d %H:%M')
                    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

            time.sleep(40)

            local_storage_uid(user_id_from_local, driver)
            time.sleep(2)
            order_element = driver.find_element_by_class_name('os-order-number__')
            time.sleep(2)
            if order_element is not None:
                time.sleep(1)
                is_ord_display = order_element.is_displayed()
                time.sleep(1)
                if is_ord_display:
                    success = "{order_id}, time:{time}".format(
                        order_id=order_element.text,
                        time=datetime.now().strftime(
                            '%Y-%m-%d %H:%M, Landing Ginseng page 5 NEW successful,'))
                    write_to_log_file('ginseng', success)
            else:
                if not app_config['stats.debug']:
                    post_to_slack("Landing page 5 NEW payment successfully- order id is missing")
                    post_to_opsgenie("Landing page 5 NEW payment successfully- order id is missing")
                    text = 'Landing page 5 NEW payment successfully- order id is missing'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing page 5 NEW payment successfully- order id is missing%Y-%m-%d %H:%M')
                    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
                    time.sleep(5)

    else:
        customer_details = get_customer_from_google_sheet()

        keys = ["Email", "Name", "Street", "Postcode", "City", "Country"]

        inputs_array = driver.find_elements_by_class_name('Fieldset-input')

        for index, current_input in enumerate(inputs_array):
            if index == 5:
                Select(current_input).select_by_visible_text(customer_details[5])
                continue
            current_input.send_keys(customer_details[keys.index(current_input.get_attribute("placeholder"))])
            time.sleep(2)
            driver.find_element_by_class_name('Button-content').click()

        time.sleep(4)
        customer_details_1 = get_customer_from_google_sheet()

        keys = ["Email", "Name", "Street", "Postcode", "City", "Country"]

        inputs_array = driver.find_elements_by_class_name('Fieldset-input')

        for index, current_input in enumerate(inputs_array):
            if index == 5:
                Select(current_input).select_by_visible_text(customer_details_1[5])
                continue
            current_input.send_keys(customer_details_1[keys.index(current_input.get_attribute("placeholder"))])

        time.sleep(2)
        driver.find_element_by_class_name('Button-content').click()
        driver.switch_to.default_content()
        time.sleep(2)

        # Credit card #
        iframe_credit = len(driver.find_elements_by_tag_name("iframe"))
        if iframe_credit is not None:
            if iframe == 2:
                driver.switch_to.frame(
                    driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 1])
                time.sleep(2)
            else:
                driver.switch_to.frame(
                    driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 2])
        else:
            driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4594)
            driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4083)
            driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(3115)
            driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(9870)
            driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('03')
            driver.find_elements_by_class_name('Fieldset-input')[1].send_keys(20)
            driver.find_elements_by_class_name('Fieldset-input')[2].send_keys(680)
            time.sleep(2)
            driver.find_element_by_class_name('Button-content').click()
            driver.switch_to.default_content()
            time.sleep(20)

        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4594)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4083)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(3115)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(9870)
        driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('03')
        driver.find_elements_by_class_name('Fieldset-input')[1].send_keys(20)
        driver.find_elements_by_class_name('Fieldset-input')[2].send_keys(680)
        time.sleep(2)
        driver.find_element_by_class_name('Button-content').click()
        driver.switch_to.default_content()
        time.sleep(20)

        # Get Order ID #
        local_storage_uid(user_id_from_local, driver)
        order_element = get_order_id_strip(5)
        if order_element is not None:
            is_ord_display = order_element.is_displayed()
            if is_ord_display:
                success = "{order_id}, time:{time}".format(
                    order_id=order_element.text,
                    time=datetime.now().strftime('%Y-%m-%d %H:%M, Landing Ginseng page 5 NEW successful,'))
                write_to_log_file('ginseng', success)
        else:
            post_to_slack("Landing page 5 NEW payment successfully- order id is missing")
            post_to_opsgenie("Landing page 5 NEW payment successfully- order id is missing")
            text = 'Landing page 5 NEW payment successfully- order id is missing'
            recipients = ['dekela@shellanoo.com']
            post_to_mail(recipients, text)
            now = datetime.now().strftime(
                'Landing page 5 NEW payment successfully- order id is missing %Y-%m-%d %H:%M')
            driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
            time.sleep(5)


def local_storage_uid(uid, driver):
    if 'user_id_from_local' in globals():
        global user_id_from_local
        uid = user_id_from_local

    if uid == driver.execute_script("return localStorage['uid']"):
        print('local storage UID is OK ')
        print(uid)
    else:
        uid_changed = driver.execute_script("return localStorage['uid']")
        print('local storage UID is changed')
        print(uid_changed)
        text = 'Landing Ginseng page 5 NEW- Local storage UID is changed'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def get_order_id_strip(count=5):
    global driver
    y = 0
    order_element = None
    while y < count:
        y += 1
    try:
        order_element = WebDriverWait(driver, 10).until(
            lambda drive: driver.find_element_by_class_name('os-order-number__'))
    except TimeoutException as t_e:
        print("Didn't found order ID {e}".format(e=t_e))

    return order_element


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)
