import requests
__author__ = 'dibits'


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def main():
    text = 'This is a test'
    post_to_opsgenie(text)


if __name__ == '__main__':
    main()