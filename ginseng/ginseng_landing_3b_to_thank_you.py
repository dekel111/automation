import logging
import sys
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from helper_functions import write_to_log_file
from selenium.common.exceptions import TimeoutException
import requests
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os
from datetime import datetime, date
from random import randint
from selenium.webdriver.support.ui import Select
from payment_landing_3b_to_thank_you import pay_with_shopify_3b_to_thank_you, pay_with_stripe_3b_to_thank_you
import boto.ses

_log = logging.getLogger(__name__)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def get_order_id_strip(count=5):
    global driver
    y = 0
    order_element = None
    while y < count:
        y += 1
    try:
        order_element = WebDriverWait(driver, 10).until(
            lambda drive: driver.find_element_by_class_name('os-order-number__'))
    except TimeoutException as t_e:
        print ("Didn't found order ID {e}".format(e=t_e))

    return order_element


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def local_storage_uid():
    global user_id_from_local
    if user_id_from_local == driver.execute_script("return localStorage['uid']"):
        print ('local storage UID is OK ')
        print (user_id_from_local)
    else:
        uid_changed = driver.execute_script("return localStorage['uid']")
        print ('local storage UID is changed')
        print (uid_changed)
        text = 'Landing Ginseng page 3B to Thank You page- Local storage UID changed'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)


def start_sanity_firefox():
    global driver
    global user_id_from_local
    driver = webdriver.Firefox()
    driver.get(site)
    driver.maximize_window()
    user_id_from_local = driver.execute_script("return localStorage['uid']")

    # Third a landing page #
    time.sleep(3)
    local_storage_uid()
    time.sleep(3)
    buy_now_top = driver.find_elements_by_class_name('f24')[0]
    buy_now_bottom = driver.find_elements_by_class_name('f24')[4]
    buttons = [buy_now_bottom] + [buy_now_top]
    time.sleep(2)
    if len(buttons):
        buy_now_button = randint(0, len(buttons) - 1)
        element = buttons[buy_now_button]
        time.sleep(2)
        driver.execute_script("arguments[0].scrollIntoView(false);", element)
        time.sleep(2)
        element.click()
    else:
        post_to_slack("Landing Firefox Ginseng page 3B to Thank You page Failed- Buy Now button is missing")
        post_to_opsgenie("Landing Firefox Ginseng page 3B to Thank You page Failed- Buy Now button is missing")
        text = 'Landing Firefox Ginseng page 3B to Thank You page Failed- Buy Now button is missing'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)
        now = datetime.now().strftime(
            'Landing Firefox Ginseng page 3B to Thank You page Failed- Buy Now button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    # Ginseng website #
    time.sleep(7)
    current_url = driver.current_url
    time.sleep(2)
    checkout_button = driver.find_elements_by_class_name('check_out')[0]
    time.sleep(2)
    checkout_button.click()
    time.sleep(5)

    if current_url == driver.current_url:
        # Strip open and insert email/name/street etc.. #
        pay_with_stripe_3b_to_thank_you(driver)
    else:
        pay_with_shopify_3b_to_thank_you(driver, user_id_from_local=user_id_from_local)


def start_sanity_chrome():
    global driver
    global user_id_from_local
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get(site)
    driver.set_window_size(1750, 1750)
    user_id_from_local = driver.execute_script("return localStorage['uid']")

    # Third A landing page #
    time.sleep(3)
    local_storage_uid()
    time.sleep(3)
    buy_now_top = driver.find_elements_by_class_name('f24')[0]
    buy_now_bottom = driver.find_elements_by_class_name('f24')[4]
    buttons = [buy_now_bottom] + [buy_now_top]

    if len(buttons):
        buy_now_button = randint(0, len(buttons) - 1)
        element = buttons[buy_now_button]
        driver.execute_script("arguments[0].scrollIntoView(false);", element)
        time.sleep(2)
        element.click()
    else:
        post_to_slack("Landing Chrome Ginseng page 3B to Thank You page Failed- Buy Now button is missing")
        post_to_opsgenie("Landing Chrome Ginseng page 3B to Thank You page Failed- Buy Now button is missing")
        text = 'Landing CHrome Ginseng page 3B to Thank You page Failed- Buy Now button is missing'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)
        now = datetime.now().strftime(
            'Landing CHrome Ginseng page 3B to Thank You page Failed- Buy Now button is missing %Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)

    current_url = driver.current_url
    time.sleep(2)
    checkout_button = driver.find_elements_by_class_name('check_out')[0]
    time.sleep(2)
    checkout_button.click()

    time.sleep(5)

    if current_url == driver.current_url:
        # Strip open and insert email/name/street etc.. #
        pay_with_stripe_3b_to_thank_you(driver)
    else:
        pay_with_shopify_3b_to_thank_you(driver, user_id_from_local=user_id_from_local)


if __name__ == '__main__':

    sites = ["https://www.ginseng-life.com/wllp/third-step-buy-now-b"]

    for site in sites:
        try:
            logging.basicConfig(level=logging.INFO)
            logging.info('Starting automation on %s Site' % site)
            _log.info('Running both sanity_firefox and sanity_chrome')
            # driver = None
            is_run_firefox = True
            is_run_chrome = True

            if is_run_firefox:
                start_sanity_firefox()
                is_run_firefox = False
            driver.close()
            if is_run_chrome:
                start_sanity_chrome()
                is_run_chrome = False
        except BaseException as e:
            logging.error('Failed to do something: %s', e)
            import traceback

            post_to_slack("Ginseng landing page 3B to Thank You payment failed")
            post_to_opsgenie("Ginseng landing page 3B to Thank You payment failed")
            text = 'Ginseng landing page 3B to Thank You payment failed'
            recipients = ['dekela@shellanoo.com']
            post_to_mail(recipients, text)
            traceback.print_exc()
            m_now = datetime.now().strftime('Landing Ginseng page 3B to Thank You Failed %Y-%m-%d %H:%M')
            driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % m_now)

        _log.info('All done.')
        driver.close()
