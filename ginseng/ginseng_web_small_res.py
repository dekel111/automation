from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from helper_functions import write_to_log_file
from selenium.common.exceptions import TimeoutException
import logging
import time
import boto.ses
import requests
import sys
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import requests
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os
from datetime import datetime, date
from random import randint
from selenium.webdriver.support.ui import Select
from payment_ginseng import shopify_payment_firefox, strip_payment_firefox, strip_payment_chrome, shopify_payment_chrome

_log = logging.getLogger(__name__)
faillar_counter = 0


# def get_order_id(count=5):
#     global driver
#     y = 0
#     order_element = None
#     while y < count:
#         y += 1
#     try:
#         order_element = WebDriverWait(driver, 10).until(
#             lambda drive: driver.find_element_by_class_name('os-order-number'))
#     except TimeoutException as t_e:
#         print "Didn't found order ID {e}".format(e=t_e)
#
#     return order_element

# def get_order_id_strip(count=5):
#     global driver
#     y = 0
#     order_element = None
#     while y < count:
#         y += 1
#     try:
#         order_element = WebDriverWait(driver, 10).until(
#             lambda drive: driver.find_element_by_class_name('os-order-number__'))
#     except TimeoutException as t_e:
#         print "Didn't found order ID {e}".format(e=t_e)
#
#     return order_element


def delete_cookies():
    global driver
    driver.delete_all_cookies()
    time.sleep(5)


def sanity_firefox():
    global faillar_counter
    global driver
    site = "https://ginseng.life/"

    driver = webdriver.Firefox()
    driver.get(site)

    logging.info('Starting automation on %s Site' % site)
    driver.maximize_window()
    delete_cookies()
    if len(driver.find_elements_by_class_name('add-to-cart-btn-link')):

        prod_index = randint(0,
                             len((driver.find_elements_by_css_selector(
                                 '.widget-new-product .add-to-cart-btn-link'))) / 2 - 1)

        element = driver.find_elements_by_css_selector('.widget-new-product .add-to-cart-btn-link')[prod_index]

        driver.execute_script("arguments[0].scrollIntoView(false);", element)
        element.click()
        time.sleep(2)
    else:
        post_to_slack("Automation Ginseng Failed {error}".format(error=str(e)))
        now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
        times_fail()
        sanity_firefox()

    # Add product to cart #
    if len(driver.find_elements_by_css_selector('#product-add-to-cart')):
        element_1 = driver.find_element_by_id('product-add-to-cart')
        driver.execute_script("arguments[0].scrollIntoView(false);", element_1)
        element_1.click()
        time.sleep(2)
    else:
        post_to_slack("Automation Ginseng Failed {error}".format(error=str(e)))
        post_to_opsgenie("Add product is failed")
        now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
        sanity_firefox()
    time.sleep(2)
    current_url = driver.current_url

    driver.find_elements_by_css_selector(".btn-secondary")[1].click()
    time.sleep(3)
    if len(driver.find_elements_by_class_name('check_out')) == 1:
        driver.find_element_by_class_name('check_out').click()
    else:
        time.sleep(2)
    if current_url == driver.current_url:
        strip_payment_firefox(driver)
    else:
        shopify_payment_firefox(driver)

    driver.close()


def sanity_chrome():
    global driver
    site = 'https://ginseng.life'
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get(site)

    logging.info('Starting automation on %s Sites' % site)
    driver.set_window_size(1750, 1750)
    # driver.maximize_window()
    time.sleep(2)
    delete_cookies()

    # Product page #
    if len(driver.find_elements_by_class_name('add-to-cart-btn-link')):
        prod_index = randint(0,
                             len(driver.find_elements_by_css_selector('.widget-new-product .add-to-cart-btn-link'))) / 2

        element = driver.find_elements_by_css_selector('.widget-new-product .add-to-cart-btn-link')[prod_index]
        element.click()
        time.sleep(2)
    else:
        post_to_slack("Automation Ginseng Failed {error}".format(error=str(e)))
        post_to_opsgenie("Add to cart button is failed")
        now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
        sanity_chrome()

    # Add product to cart #
    if len(driver.find_elements_by_css_selector('#product-add-to-cart')):
        element_1 = driver.find_element_by_id('product-add-to-cart')
        driver.execute_script("arguments[0].scrollIntoView(false);", element_1)
        element_1.click()
        time.sleep(2)
    else:
        post_to_slack("Automation Ginseng Failed {error}".format(error=str(e)))
        now = datetime.now().strftime('%Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
        sanity_chrome()
        time.sleep(4)
    time.sleep(2)

    current_url = driver.current_url
    driver.find_elements_by_css_selector(".btn-secondary")[1].click()
    time.sleep(2)
    if len(driver.find_elements_by_class_name('check_out')) == 1:
        driver.find_element_by_class_name('check_out').click()
    else:
        time.sleep(2)
    if current_url == driver.current_url:
        strip_payment_chrome(driver)
    else:
        shopify_payment_chrome(driver)

    driver.close()


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def times_fail():
    global driver
    global failed_counter

    while failed_counter < 3:
        failed_counter += 1
        if is_run_firefox:
            sanity_firefox()
            # print that everything is COOL.

        if is_run_chrome:
            sanity_chrome()
            # print that everything is COOL.
        exit()

    error_string = "Automation Ginseng Failed 3 times {error} on browser:{browser}".format(error=str(e),
                                                                                           browser=failed_browser)
    post_to_slack(error_string)
    now = datetime.now().strftime('%Y-%m-%d %H:%M')
    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
    driver.close()
    exit()


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


if __name__ == '__main__':
    failed_counter = 0
    driver = None
    is_run_firefox = False
    is_run_chrome = False
    try:
        logging.basicConfig(level=logging.INFO)

        if len(sys.argv) > 1:
            if sys.argv[1] == 'firefox':
                _log.info('Running sanity_firefox')
                is_run_firefox = True
            elif sys.argv[1] == 'chrome':
                _log.info('Running sanity_chrome')
                is_run_chrome = True
            else:
                raise Exception('Unknown check {arg}'.format(arg=sys.argv[1]))
        else:
            _log.info('Running both sanity_firefox and sanity_chrome')
            # post_to_slack('Running both sanity_firefox and sanity_chrome')
            is_run_firefox = True
            is_run_chrome = True

        if is_run_firefox:
            sanity_firefox()
            is_run_firefox = False

        if is_run_chrome:
            sanity_chrome()
            is_run_chrome = False

        _log.info('All done.')

    except BaseException as e:
        _log.error('Failed to do something: %s', e)
        import traceback

        m_now = datetime.now().strftime('%Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % m_now)

        traceback.print_exc()

        failed_browser = "FireFox"
        if not is_run_firefox:
            failed_browser = "Chrome"

        error_string = "Automation Ginseng Failed {error} on browser:{browser}, Trying again".format(error=str(e),
                                                                                                     browser=failed_browser)
        m_now = datetime.now().strftime('%Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % m_now)
        # post_to_slack(error_string)
        # post_to_opsgenie(error_string)
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, error_string)
        times_fail()
