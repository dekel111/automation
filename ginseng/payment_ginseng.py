import time
from selenium.webdriver.support.wait import WebDriverWait
from helper_functions import write_to_log_file
from selenium.common.exceptions import TimeoutException
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os
from datetime import datetime, date
from random import randint
from selenium.webdriver.support.ui import Select
from selenium.webdriver.common.keys import Keys
import boto.ses
import logging
import requests

_log = logging.getLogger(__name__)


def shopify_payment_firefox(driver):
    iframe = len(driver.find_elements_by_tag_name("iframe"))
    if iframe == 1 or iframe == 0:
        time.sleep(1)
        driver.find_elements_by_class_name('field__input')[1].send_keys('dekela@shellanoo.com')
        driver.find_elements_by_class_name('field__input')[2].send_keys('Dekel')
        driver.find_elements_by_class_name('field__input')[3].send_keys('Amram')
        driver.find_elements_by_class_name('field__input')[4].send_keys('Maskit 25')
        driver.find_elements_by_class_name('field__input')[6].send_keys('Herzelia')
        driver.find_elements_by_class_name('field__input')[9].send_keys('4600')
        driver.find_elements_by_class_name('field__input')[10].send_keys('0547515457')
        time.sleep(1)
        continue_button = driver.find_elements_by_css_selector('.btn__content')[2]
        time.sleep(1)
        driver.execute_script("arguments[0].scrollIntoView(true);", continue_button)
        continue_button.click()
        time.sleep(3)
        driver.find_element_by_css_selector('.step__footer__continue-btn').click()
        time.sleep(5)

        # Get Promo Code #
        code = get_code_from_google_sheet()
        driver.find_element_by_id("checkout_reduction_code").send_keys(code)
        time.sleep(5)
        driver.find_elements_by_css_selector('.field__input-btn')[0].click()
        time.sleep(5)

        driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[0])
        time.sleep(1)
        # Insert Card Number #
        driver.find_element_by_id('number').send_keys(4594)
        driver.find_element_by_id('number').send_keys(4083)
        driver.find_element_by_id('number').send_keys(3115)
        driver.find_element_by_id('number').send_keys(9870)
        driver.switch_to.default_content()
        time.sleep(2)

        # Insert Card Name #
        if len(driver.find_elements_by_css_selector("#name")) == 0:
            driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[1])
            time.sleep(1)
        driver.find_element_by_id('name').send_keys("passportcard enabled")
        time.sleep(2)
        driver.switch_to.default_content()
        time.sleep(1)

        # Insert Expiry #
        if len(driver.find_elements_by_css_selector("#expiry")) == 0:
            driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[2])
            time.sleep(1)
        driver.find_element_by_id('expiry').send_keys("03")
        driver.find_element_by_id('expiry').send_keys("20")
        time.sleep(2)
        driver.switch_to.default_content()

        # Insert security code 3 digits #
        time.sleep(1)
        if len(driver.find_elements_by_css_selector("#expiry")) == 0:
            driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[3])
            time.sleep(1)
        driver.find_element_by_id('verification_value').send_keys("680")
        time.sleep(2)
        driver.switch_to.default_content()
        time.sleep(2)

        scroll = driver.find_element_by_class_name("content")
        scroll.send_keys(Keys.PAGE_DOWN)
        time.sleep(2)
        driver.find_element_by_class_name("step__footer__continue-btn").click()
        time.sleep(20)

        # order_element = get_order_id()
        order_element = driver.find_element_by_class_name('os-order-number')
        if order_element is not None:
            is_ord_display = order_element.is_displayed()
            if is_ord_display:
                success = "{order_id}, time:{time}".format(
                    order_id=order_element.text,
                    time=datetime.now().strftime('%Y-%m-%d %H:%M, Ginseng successful,'))
                write_to_log_file('ginseng', success)
        else:
            post_to_slack("Payment Shopify Firefox successfuly order id is missing")
            post_to_opsgenie("Payment Shopify Firefox failed")
            text = 'Automation Ginseng Shopify Firefox Failed'
            recipients = ['dekela@shellanoo.com']
            post_to_mail(recipients, text)
            now = datetime.now().strftime('%Y-%m-%d %H:%M')
            driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
            time.sleep(5)

    driver.close()


def strip_payment_firefox(driver):
    driver.find_element_by_class_name('promo_code_toggle').click()
    code = get_code_from_google_sheet()
    driver.find_element_by_id("checkout_reduction_code").send_keys(code)
    time.sleep(5)
    driver.find_element_by_id("promo_code_button").click()
    time.sleep(5)
    scroll = driver.find_element_by_class_name("main-content")
    scroll.send_keys(Keys.PAGE_DOWN)
    time.sleep(2)

    # Payment information #
    driver.find_elements_by_class_name('strip_open')[1].click()
    iframe = len(driver.find_elements_by_tag_name("iframe"))
    if iframe is not None:
        driver.switch_to.frame(
            driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 1])
    else:
        customer_details = get_customer_from_google_sheet()

        keys = ["Email", "Name", "Street", "Postcode", "City", "Country"]

        inputs_array = driver.find_elements_by_class_name('Fieldset-input')

        for index, current_input in enumerate(inputs_array):
            if index == 5:
                Select(current_input).select_by_visible_text(customer_details[5])
                continue
            current_input.send_keys(customer_details[keys.index(current_input.get_attribute("placeholder"))])
            time.sleep(2)
            driver.find_element_by_class_name('Button-content').click()

    time.sleep(4)

    # driver.find_element_by_css_selector("input[type=email]").send_keys('dekela@shellanoo.com') #added by Idan#
    # driver.find_element_by_id('email').send_keys('dekela@shellanoo.com')
    # driver.find_element_by_id('shipping-name').send_keys('Dekel Amram')
    # driver.find_element_by_id('shipping-street').send_keys('Maskit 25')
    # driver.find_element_by_id('shipping-zip').send_keys('4600')
    # driver.find_element_by_id('shipping-city').send_keys('Herzelia')
    # driver.find_element_by_class_name('iconContinue').click()

    customer_details_1 = get_customer_from_google_sheet()

    keys = ["Email", "Name", "Street", "Postcode", "City", "Country"]

    inputs_array = driver.find_elements_by_class_name('Fieldset-input')

    for index, current_input in enumerate(inputs_array):
        if index == 5:
            Select(current_input).select_by_visible_text(customer_details_1[5])
            continue
        current_input.send_keys(customer_details_1[keys.index(current_input.get_attribute("placeholder"))])

    ## for i, detail in enumerate(customer_details):
    ##     if i == 5:
    ##         Select(driver.find_elements_by_class_name('Fieldset-input')[i].find_element_by_tag_name(
    ##             'select')).select_by_visible_text(detail)
    ##         continue
    ##     driver.find_elements_by_class_name('Fieldset-input')[i].send_keys(detail)

    time.sleep(2)
    driver.find_element_by_class_name('Button-content').click()
    time.sleep(1)
    driver.switch_to.default_content()
    time.sleep(3)
    # driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[0])
    # time.sleep(2)


    # driver.find_elements_by_class_name('Fieldset-input')[0].send_keys('dekela@shellanoo.com')
    # driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('Dekel Amram')
    # driver.find_elements_by_class_name('Fieldset-input')[2].send_keys('Maskit 25')
    # driver.find_elements_by_class_name('Fieldset-input')[3].send_keys('Herzelia')
    # driver.find_elements_by_class_name('Fieldset-input')[4].send_keys('4600')
    # driver.find_element_by_class_name('Button-content').click()
    # driver.switch_to.default_content()
    # time.sleep(3)s

    iframe_credit = len(driver.find_elements_by_tag_name("iframe"))
    time.sleep(1)
    if iframe_credit is not None:
        driver.switch_to.frame(
            driver.find_elements_by_tag_name("iframe")[len(driver.find_elements_by_tag_name("iframe")) - 1])
        time.sleep(1)
    else:
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4594)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4083)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(3115)
        driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(9870)
        driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('03')
        driver.find_elements_by_class_name('Fieldset-input')[1].send_keys(20)
        driver.find_elements_by_class_name('Fieldset-input')[2].send_keys(680)
        time.sleep(2)
        driver.find_element_by_class_name('Button-content').click()
        time.sleep(2)
    # driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[1])
    time.sleep(2)

    # driver.find_element_by_id('card_number').send_keys('4594')
    # driver.find_element_by_id('card_number').send_keys('4083')
    # driver.find_element_by_id('card_number').send_keys('3115')
    # driver.find_element_by_id('card_number').send_keys('9870')
    # driver.find_element_by_id('cc-exp').send_keys('03')
    # driver.find_element_by_id('cc-exp').send_keys('20')
    # driver.find_element_by_id('cc-csc').send_keys('680')
    # driver.find_element_by_id('submitButton').click()
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4594)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4083)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(3115)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(9870)
    driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('03')
    driver.find_elements_by_class_name('Fieldset-input')[1].send_keys(20)
    driver.find_elements_by_class_name('Fieldset-input')[2].send_keys(680)
    driver.find_element_by_class_name('Button-content').click()

    driver.switch_to.default_content()
    time.sleep(20)

    order_element = get_order_id_strip(5)

    if order_element is not None:
        is_ord_display = order_element.is_displayed()
        if is_ord_display:
            success = "{order_id}, time:{time}".format(
                order_id=order_element.text,
                time=datetime.now().strftime('%Y-%m-%d %H:%M, Ginseng successful,'))
            write_to_log_file('ginseng', success)
    else:
        post_to_slack("Payment Stripe Firefox successfuly order id is missing")
        post_to_opsgenie("Payment Stripe Firefox failed")
        text = 'Automation Ginseng Stripe Firefox Failed'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)
        now = datetime.now().strftime('%Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
    time.sleep(5)

    driver.close()
    _log.info('Done with sanity_firefox')


def strip_payment_chrome(driver):
    driver.execute_script("window.scrollTo(0, 9000)")
    time.sleep(2)
    driver.find_element_by_class_name('promo_code_toggle').click()
    code = get_code_from_google_sheet()
    driver.find_element_by_id("checkout_reduction_code").send_keys(code)
    time.sleep(5)
    driver.find_element_by_id("promo_code_button").click()
    time.sleep(5)

    driver.execute_script("window.scrollTo(0, 9000)")
    driver.find_elements_by_class_name('strip_open')[1].click()
    driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[0])
    time.sleep(2)
    # driver.find_element_by_id('email').send_keys('dekela@shellanoo.com')
    # driver.find_element_by_id('shipping-name').send_keys('Dekel Amram')
    # driver.find_element_by_id('shipping-street').send_keys('Maskit 25')
    # driver.find_element_by_id('shipping-zip').send_keys('4600')
    # driver.find_element_by_id('shipping-city').send_keys('Herzelia')
    # driver.find_element_by_class_name('iconContinue').click()


    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys('dekela@shellanoo.com')
    driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('Dekel Amram')
    driver.find_elements_by_class_name('Fieldset-input')[2].send_keys('Maskit 25')
    driver.find_elements_by_class_name('Fieldset-input')[3].send_keys('Herzelia')
    driver.find_elements_by_class_name('Fieldset-input')[4].send_keys('4600')
    time.sleep(1)
    driver.find_element_by_class_name('Button').click()
    time.sleep(2)
    driver.switch_to.default_content()
    time.sleep(2)
    driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[0])
    time.sleep(2)
    # driver.find_element_by_id('card_number').send_keys('4594')
    # driver.find_element_by_id('card_number').send_keys('4083')
    # driver.find_element_by_id('card_number').send_keys('3115')
    # driver.find_element_by_id('card_number').send_keys('9870')
    # driver.find_element_by_id('cc-exp').send_keys('03')
    # driver.find_element_by_id('cc-exp').send_keys('20')
    # driver.find_element_by_id('cc-csc').send_keys('680')
    # driver.find_element_by_id('submitButton').click()
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4594)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(4083)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(3115)
    driver.find_elements_by_class_name('Fieldset-input')[0].send_keys(9870)
    driver.find_elements_by_class_name('Fieldset-input')[1].send_keys('03')
    driver.find_elements_by_class_name('Fieldset-input')[1].send_keys(20)
    driver.find_elements_by_class_name('Fieldset-input')[2].send_keys(680)
    time.sleep(2)
    driver.find_element_by_class_name('Button').click()
    time.sleep(2)
    driver.switch_to.default_content()
    time.sleep(20)
    order_element = get_order_id_strip(5)

    # Push this
    # if not str(driver.title).contains("Dekel"):
    #     print ('Title named Dekel is missing')

    if order_element is not None:
        is_ord_display = order_element.is_displayed()
        if is_ord_display:
            success = "{order_id}, time:{time}".format(
                order_id=order_element.text,
                time=datetime.now().strftime('%Y-%m-%d %H:%M, Ginseng successful,'))
            write_to_log_file('ginseng', success)
    else:
        post_to_slack("Payment Stripe Chrome successfully order id is missing")
        post_to_opsgenie("Payment Stripe Chrome failed")
        text = 'Automation Ginseng Stripe Chrome Failed'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)
        now = datetime.now().strftime('%Y-%m-%d %H:%M')
        driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
    time.sleep(5)

    driver.close()
    _log.info('Done with sanity_chrome')


def shopify_payment_chrome(driver):
    # driver.find_elements_by_class_name("btn-secondary")[1].click()  # Removed #
    # driver.find_element_by_css_selector('.check_out').click()
    iframe = len(driver.find_elements_by_tag_name("iframe"))
    if iframe == 1 or iframe == 0:
        # Shipping information #
        time.sleep(1)
        driver.find_elements_by_class_name("field__input")[1].send_keys("dekela@shellanoo.com")
        driver.find_elements_by_class_name("field__input")[2].send_keys("Dekel")
        driver.find_elements_by_class_name("field__input")[3].send_keys("Amram")
        driver.find_elements_by_class_name("field__input")[4].send_keys("Mascit 25")
        driver.find_elements_by_class_name("field__input")[6].send_keys("Herzelia")
        driver.find_elements_by_class_name("field__input")[9].send_keys("4600")
        driver.find_elements_by_class_name("field__input")[10].send_keys("0547515457")
        time.sleep(2)
        driver.execute_script("window.scrollTo(0, 9000)")
        time.sleep(1)
        driver.find_element_by_class_name("step__footer__continue-btn").click()
        time.sleep(3)
        driver.find_element_by_class_name("step__footer__continue-btn").click()
        time.sleep(3)

        # Insert Discount #
        code = get_code_from_google_sheet()
        driver.find_element_by_id("checkout_reduction_code").send_keys(code)
        time.sleep(1)
        driver.find_elements_by_class_name("btn")[0].click()
        time.sleep(5)

        # PAYMENT INFORMATION #
        if len(driver.find_elements_by_css_selector("#number")) == 0:
            driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[0])
        time.sleep(2)
        driver.find_element_by_id('number').send_keys('4594')
        driver.find_element_by_id('number').send_keys('4083')
        driver.find_element_by_id('number').send_keys('3115')
        driver.find_element_by_id('number').send_keys('9870')
        time.sleep(1)

        driver.switch_to.default_content()
        time.sleep(2)
        # Insert Card name #
        if len(driver.find_elements_by_css_selector("#name")) == 0:
            driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[1])
        time.sleep(2)
        driver.find_element_by_id('name').send_keys("passportcard enabled")
        time.sleep(1)
        driver.switch_to.default_content()

        # Insert Expiry #
        time.sleep(2)
        if len(driver.find_elements_by_css_selector("#expiry")) == 0:
            driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[2])
        time.sleep(2)
        driver.find_element_by_id('expiry').send_keys("03")
        driver.find_element_by_id('expiry').send_keys("20")
        time.sleep(1)
        driver.switch_to.default_content()
        time.sleep(2)

        # Insert security code 3 digits #
        if len(driver.find_elements_by_css_selector("#expiry")) == 0:
            driver.switch_to.frame(driver.find_elements_by_tag_name("iframe")[3])
        time.sleep(2)
        driver.find_element_by_id('verification_value').send_keys("680")
        time.sleep(1)
        driver.switch_to.default_content()
        time.sleep(2)
        # Scroll down #
        driver.execute_script("window.scrollTo(0, 9000)")
        time.sleep(2)
        driver.find_element_by_class_name("step__footer__continue-btn").click()
        time.sleep(20)

        # order_element = get_order_id(5)
        order_element = driver.find_element_by_class_name('os-order-number')
        if order_element is not None:
            is_ord_display = order_element.is_displayed()
            if is_ord_display:
                success = "{order_id}, time:{time}".format(
                    order_id=order_element.text,
                    time=datetime.now().strftime('%Y-%m-%d %H:%M, Ginseng successful,'))
                write_to_log_file('ginseng', success)
        else:
            post_to_slack("Payment Shopify Chrome successfuly order id is missing")
            post_to_opsgenie("Payment Shopify Chrome failed")
            text = 'Automation Ginseng Shopify Chrome Failed'
            recipients = ['dekela@shellanoo.com']
            post_to_mail(recipients, text)
            now = datetime.now().strftime('%Y-%m-%d_%H-%M')
            driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
    driver.close()

    _log.info('Done with sanity_chrome')


def get_order_id():
    global driver
    count = 5
    y = 0
    order_element = None
    while y < count:
        y += 1
    try:
        order_element = WebDriverWait(driver, 10).until(
            lambda drive: driver.find_element_by_class_name('os-order-number'))
    except TimeoutException as t_e:
        print ("Didn't found order ID {e}".format(e=t_e))
    return order_element


def get_order_id_strip(count=5):
    y = 0
    order_element = None
    while y < count:
        y += 1
    try:
        order_element = WebDriverWait(driver, 10).until(
            lambda drive: driver.find_element_by_class_name('os-order-number__'))
    except TimeoutException as t_e:
        print ("Didn't found order ID {e}".format(e=t_e))

    return order_element


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def local_storage_uid():
    global user_id_from_local
    if user_id_from_local == driver.execute_script("return localStorage['uid']"):
        print ('local storage UID is OK ')
        print (user_id_from_local)
    else:
        print ('local storage UID is changed')
        text = 'Landing Automation Ginseng-Local storage UID is changed'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)
