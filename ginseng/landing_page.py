import unittest
import logging
import traceback
import sys
from selenium import webdriver
import time
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.keys import Keys

# config = {
#     "teddydo": {
#
#         "title": "TEDDYDO - the kids fashion search engine"
#     },
#     "bellaboo": {
#
#     },
#     "huntsberg": {
#
#     }
# }

if __name__ == '__main__':

    sites = ["http://www1.ginseng.life/stress/", "http://www1.ginseng.life/mood/", "http://www.ginseng-life.com/memory", "http://www.ginseng-life.com/antiaging/", "http://ww2.ginseng.life/stress-relief/", "http://ww2.ginseng.life/memory-concentration/"]

    for site in sites:

        logging.basicConfig(level=logging.INFO)

        driver = webdriver.Firefox()
        driver.get(site)

        logging.info('Starting automation on %s Site' % site)

        # links = driver.find_elements_by_tag_name("a")
        # links_array = []
        # for link in links:
        #     links_array.append(link.get_attribute("href"))
        #
        # for link_to_go in links_array:
        #     driver.get(link_to_go)
        #     time.sleep(3)
        #     driver.back()
        #

        qty = driver.find_element_by_tag_name('input').get_attribute("value")
        qty != 0
        print(qty)

        driver.find_element_by_id("product-add-to-cart").click()  # Add to Cart #
        driver.back()
        time.sleep(4)


        driver.find_elements_by_class_name("inc")[2].click()  # Qty is 2 #
        time.sleep(2)
        driver.find_elements_by_class_name("inc")[2].click()  # Qty is 3 #
        time.sleep(2)
        driver.find_elements_by_class_name("dec")[2].click()  # Qty is 2 #
        driver.find_element_by_id("product-add-to-cart").click()  # Add to Cart #
        driver.back()
        time.sleep(4)

        scroll_time = 13
        scroll = driver.find_element_by_class_name("main-content")  # Scroll down#
        scroll.send_keys(Keys.PAGE_DOWN)

        # driver.find_element_by_tag_name("a").click()
        # driver.back()
        time.sleep(2)
        driver.find_elements_by_class_name("add-to-cart-btn-link")[0].click()  # View First product#
        driver.back()
        time.sleep(2)
        driver.find_elements_by_class_name("add-to-cart-btn-link")[1].click()  # View Second product#
        driver.back()
        time.sleep(2)
        driver.find_elements_by_class_name("add-to-cart-btn-link")[2].click()  # View Third product#
        driver.back()


    exit()



    try:
        # asserting Here. #
        assert config[site]["title"] in driver.title
        assert is_cookie_displayed_on_first_run
        assert is_cookie_not_displayed
        assert is_placeholder_has_changed
        assert is_have_over_text
        assert is_autocomplete_displayed
        assert autocomplete_length
        assert auto_complete_result
        assert hover
        assert is_Results_displayed

    except AssertionError:
        tb = sys.exc_info()
        traceback.print_tb(tb)
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]

        print('An error occurred on line {} in statement {}'.format(line, text))