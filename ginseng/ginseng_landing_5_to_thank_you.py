import sys
sys.path.append('.')

import logging
from selenium import webdriver
import time
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.wait import WebDriverWait
from helper_functions import write_to_log_file
from selenium.common.exceptions import TimeoutException
import requests
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import os
from datetime import datetime, date
import random
from random import randint
from selenium.webdriver.support.ui import Select
from payment_landing_5_to_thank_you import pay_with_shopify_5_to_thank_you, pay_with_stripe_5_to_thank_you
import boto.ses
import grequests
from config import app_config

_log = logging.getLogger(__name__)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': app_config['ops.gini.api.key'],
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def get_order_id_strip(count=5):
    global driver
    y = 0
    order_element = None
    while y < count:
        y += 1
    try:
        order_element = WebDriverWait(driver, 10).until(
            lambda drive: driver.find_element_by_class_name('os-order-number__'))
    except TimeoutException as t_e:
        print("Didn't found order ID {e}".format(e=t_e))

    return order_element


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def local_storage_uid():
    global user_id_from_local
    if user_id_from_local == driver.execute_script("return localStorage['uid']"):
        print('local storage UID is OK')
        print(user_id_from_local)
    else:
        uid_changed = driver.execute_script("return localStorage['uid']")
        print('local storage UID is changed')
        print(uid_changed)
        text = 'Landing Ginseng page 5 to Thank You page- Local storage UID changed'
        recipients = ['dekela@shellanoo.com']
        post_to_mail(recipients, text)


def start_sanity_firefox(site):
    global driver
    global user_id_from_local
    driver = webdriver.Firefox()
    driver.get(site)
    driver.maximize_window()
    user_id_from_local = driver.execute_script("return localStorage['uid']")

    # Fifth landing page #
    local_storage_uid()
    buy_now_packages_buttons = driver.find_elements_by_class_name('btn')
    buy_buttons_len = len(buy_now_packages_buttons) - 1
    test_env = 0
    for index in range(buy_buttons_len):
        buy_now_packages_buttons = driver.find_elements_by_class_name('btn')
        buy_button = buy_now_packages_buttons[index]

        if buy_button.is_displayed():
            q = buy_button.get_attribute("data-quantity")
            time.sleep(2)
            buy_button.click()
            time.sleep(2)

            # Checking if environment is test or production #
            if app_config['stats.debug']:
                if test_env == 0:
                    driver.find_element_by_id('password').send_keys('shellanoo1$')
                    driver.find_element_by_class_name('btn').click()
                    driver.get(site)
                    test_env = 1

            time.sleep(3)
            if len(driver.find_elements_by_class_name('product-thumbnail__quantity')):
                if q != driver.find_elements_by_class_name('product-thumbnail__quantity')[0].text:
                    post_to_slack("Landing Firefox Ginseng page 5 to Thank You page, QTY is wrong")
                    post_to_opsgenie("Landing Firefox Ginseng page 5 to Thank You page, QTY is wrong")
                    text = 'Landing Firefox Ginseng page 5 to Thank You page, QTY is wrong'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing Firefox Ginseng page 5 to Thank You page, QTY is wrong %Y-%m-%d %H:%M')
                    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
                else:
                    print("QTY is OK", q)

            time.sleep(2)
            # driver.execute_script("window.history.go(-1)")
            driver.get(site)
            time.sleep(2)

    ran_button = []
    buy_now_packages_buttons = driver.find_elements_by_class_name('btn')

    for button in buy_now_packages_buttons:
        if button.is_displayed():
            time.sleep(2)
            ran_button.append(button)
    current_url = driver.current_url
    time.sleep(2)
    buy_now_button = random.choice(ran_button)
    q = buy_now_button.get_attribute("data-quantity")
    print('QTY is', q)
    # ran_index = randint(0, len(ran_button) - 1)
    # buy_now_button = ran_button[ran_index]
    time.sleep(2)
    buy_now_button.click()
    time.sleep(20)

    if current_url == driver.current_url:
        # Strip open and insert email/name/street etc.. #
        pay_with_stripe_5_to_thank_you(driver, driveruser_id_from_local=user_id_from_local)
    else:
        pay_with_shopify_5_to_thank_you(driver, user_id_from_local=user_id_from_local)


def start_sanity_chrome(site):
    global driver
    global user_id_from_local
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get(site)
    driver.set_window_size(1750, 1750)
    user_id_from_local = driver.execute_script("return localStorage['uid']")

    # Fifth landing page #
    local_storage_uid()
    buy_now_packages_buttons = driver.find_elements_by_class_name('btn')
    buy_buttons_len = len(buy_now_packages_buttons) - 1
    test_env = 0
    for index in range(buy_buttons_len):
        buy_now_packages_buttons = driver.find_elements_by_class_name('btn')
        buy_button = buy_now_packages_buttons[index]

        if buy_button.is_displayed():
            q = buy_button.get_attribute("data-quantity")
            time.sleep(2)
            buy_button.click()
            time.sleep(2)

            # Checking if environment is test or production #
            if app_config['stats.debug']:
                if test_env == 0:
                    driver.find_element_by_id('password').send_keys('shellanoo1$')
                    driver.find_element_by_class_name('btn').click()
                    driver.get(site)
                    test_env = 1

            time.sleep(3)
            if len(driver.find_elements_by_class_name('product-thumbnail__quantity')):
                if q != driver.find_elements_by_class_name('product-thumbnail__quantity')[0].text:
                    post_to_slack("Landing Chrome Ginseng page 5 to Thank You page, QTY is wrong")
                    post_to_opsgenie("Landing Chrome Ginseng page 5 to Thank You page, QTY is wrong")
                    text = 'Landing Chrome Ginseng page 5 to Thank You page, QTY is wrong'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Landing Chrome Ginseng page 5 to Thank You page, QTY is wrong %Y-%m-%d %H:%M')
                    driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now)
                else:
                    print("QTY is OK", q)

            time.sleep(2)
            # driver.execute_script("window.history.go(-1)")
            driver.get(site)
            time.sleep(2)

    ran_button = []
    buy_now_packages_buttons = driver.find_elements_by_class_name('btn')

    for button in buy_now_packages_buttons:
        if button.is_displayed():
            time.sleep(2)
            ran_button.append(button)
    current_url = driver.current_url
    time.sleep(2)
    buy_now_button = random.choice(ran_button)
    q = buy_now_button.get_attribute("data-quantity")
    print('QTY is', q)
    # ran_index = randint(0, len(ran_button) - 1)
    # buy_now_button = ran_button[ran_index]
    time.sleep(2)
    buy_now_button.click()

    ################# Checkout pop up removed ##################################
    # time.sleep(2)
    # checkout_button = driver.find_elements_by_class_name('check_out')[0]
    # time.sleep(2)
    # checkout_button.click()

    time.sleep(20)

    if current_url == driver.current_url:
        pay_with_stripe_5_to_thank_you(driver)
    else:
        pay_with_shopify_5_to_thank_you(driver, user_id_from_local=user_id_from_local)


if __name__ == '__main__':

    sites = app_config['aws.fifth.urls']
    # "https://www.ginseng-life.com/wllp/fifth.html", "https://www.ginseng-life.com/wllp/wlb0.html"

    for site in sites:
        try:
            logging.basicConfig(level=logging.INFO)
            logging.info('Starting automation on %s Site' % site)
            _log.info('Running both sanity_firefox and sanity_chrome')
            # driver = None
            # is_run_firefox = True
            is_run_chrome = True

            # if is_run_firefox:
            #     start_sanity_firefox()
            #     is_run_firefox = False
            # driver.close()
            if is_run_chrome:
                start_sanity_chrome(site)
                is_run_chrome = False
        except BaseException as e:
            logging.error('Failed to do something: %s', e)
            import traceback
            if not app_config['stats.debug']:
                post_to_slack("Ginseng landing page 5 to Thank You payment failed")
                post_to_opsgenie("Ginseng landing page 5 to Thank You payment failed")
                text = 'Ginseng landing page 5 to Thank You payment failed'
                recipients = ['dekela@shellanoo.com']
                post_to_mail(recipients, text)
                traceback.print_exc()
                m_now = datetime.now().strftime('Landing Ginseng page 5 to Thank You Failed %Y-%m-%d %H:%M')
                driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % m_now)

        _log.info('All done.')
        # Delete all orders from Shopify #
        grequests.get('http://api.ginseng.life/orders/delete-shellanoo-orders-from-shopify')
        driver.close()
