import requests
__author__ = 'dibits'


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def main():
    text = 'This is a test'
    post_to_slack(text)


if __name__ == '__main__':
    main()