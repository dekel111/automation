import requests
import logging
import json


# def check_if_email_exist(email, suffix):
#     try:
#         r = requests.get('https://api.getresponse.com/v3/contacts?query[email]=@{suffix}'.format(suffix=suffix),
#                          headers={'X-Auth-Token': 'api-key dd184cb31eeb98318697081be5b90d88'})
#         emails_data = json.loads(r.text)
#
#         for single_email_data in emails_data:
#             if single_email_data['email'] == email:
#                 return True
#         return False
#
#     except BaseException as e:
#         logging.error('Failed to do something: %s', e)
#     import traceback
#     traceback.print_exc()
#
#
# if __name__ == '__main__':
#     print check_if_email_exist("idanm@shellanoo.com", "shellanoo")


def check_if_email_exist(email=None):
    try:
        url = 'https://api.getresponse.com/v3/contacts'

        if email is not None:
            url += '?query[email]={email}'.format(email=email)

        r = requests.get(url, headers={'X-Auth-Token': 'api-key dd184cb31eeb98318697081be5b90d88'})

        emails_data_as_json_array = json.loads(r.text)
        for contect in emails_data_as_json_array:
            print (contect['email'])
            print (contect['contactId'])

            # for single_email_data_json in emails_data_as_json_array:
            #     if single_email_data_json['email'] == email:
            #         return True
            # return False

    except BaseException as e:
        logging.error('Failed to do something: %s', e)
    import traceback
    traceback.print_exc()


if __name__ == '__main__':
    # check_if_email_exist()
    check_if_email_exist("@shellanoo.com")
