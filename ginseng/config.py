import logging
import os

import yaml

import utils

__version__ = '0.0.1'

_log = logging.getLogger(__name__)


class _AppConfig(dict):
    def __init__(self, iterable=None, **kwargs):
        super().__init__(iterable=iterable, **kwargs)

        config_yml_file = os.environ.get('APP_CONFIG')
        if config_yml_file is None:
            config_yml_file = os.path.join(os.path.dirname(__file__), '..', 'resources', 'config.yml')

        with open(config_yml_file, 'r') as stream:
            try:
                # Change when you are in PROD/TEST
                env = os.environ.get('APP_ENVIRONMENT', 'production')
                config_yml = yaml.load(stream)
                utils.merge_dicts(self, config_yml[env])
            except yaml.YAMLError as exc:
                _log.error('Error loading default development env, error: %s', exc)

    def get_bool(self, key, default=False):
        """
        :param key:
        :param default:
        :rtype: bool
        :return:
        """
        val = self.get(key)
        if val is None:
            return default

        val_type = type(val)
        if val_type == bool:
            return val

        if val_type == str:
            val = val.strip().lower()
            if val in ['true', 'yes', 'on', 'y', 't', '1']:
                return True
            elif val in ['false', 'no', 'off', 'n', 'f', '0']:
                return False

        return default

    def get_int(self, key, default=None):
        val = self.get(key)
        if val is None:
            return default

        return int(val)


app_config = _AppConfig()
