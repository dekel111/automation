import os
import gspread
import unittest
from time import sleep
from selenium import webdriver
from datetime import datetime, date
from oauth2client.service_account import ServiceAccountCredentials
from random import randint
import logging
from selenium.webdriver.support.ui import Select
import requests
import boto.ses
from helper_functions import write_to_log_file
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
import os

PLATFORM_VERSION = '5.1'


def write_to_log_file_failed(log_record):
    path = "../screenshots/ginseng/android/"

    if not os.path.exists(path):
        os.makedirs(path)

    path += "/screenshot.txt"

    # if not os.path.isfile(path):
    #     open(path, 'w').close()

    with open(path, "a") as log_file:
        log_file.write(log_record + "\n")


def get_code_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.get_worksheet(0)

    ws_list = worksheet.get_all_values()

    d = date.today()
    key = d.strftime('%m') + "/" + d.strftime('%d') + "/" + str(d.year)
    for row in ws_list:
        # TO DO - the current date//
        if row[0] == key:
            return row[1]


def get_customer_from_google_sheet():
    doc_id = "18IHn4kvLc9wfOhGVXQWP1XMrsXMl5rgmMQvV1GAWGfI"

    scope = ['https://spreadsheets.google.com/feeds']

    # From Local pc
    # credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("ginseng/Affiliates.json"), scope)
    # From automation pc
    credentials = ServiceAccountCredentials.from_json_keyfile_name(os.path.abspath("Affiliates.json"), scope)

    gc = gspread.authorize(credentials)

    sh = gc.open_by_key(doc_id)

    worksheet = sh.worksheet('Addresses')

    return worksheet.get_all_values()[randint(1, len(worksheet.get_all_values()) - 1)]


def post_to_slack(text):
    slack_webhook = 'https://hooks.slack.com/services/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'
    payload = {'text': text, 'username': 'QA-Automator'}
    requests.post(url=slack_webhook, json=payload)


def post_to_opsgenie(text):
    opsgenie_url = 'https://api.opsgenie.com/v1/json/alert'
    payload = {
        'apiKey': '273ba831-9a52-4128-b7ab-00d70de590e1',
        'message': text,
        'recipients': 'QA'
    }
    requests.post(url=opsgenie_url, json=payload)


def post_to_mail(recipients, text):
    conn = boto.ses.connect_to_region('us-east-1', aws_access_key_id='AKIAJXXJ5F3CFCM6W26A',
                                      aws_secret_access_key='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')
    conn.send_email(source='qa@shellanoo.com', subject='Ginseng Testing Email', body=text, to_addresses=recipients)


def local_storage_uid(self,):
    # global user_id_from_local
    if user_id_from_local == self.driver.execute_script("return localStorage['uid']"):
        print ('local storage UID is OK ')
        print (user_id_from_local)
    else:
        uid_changed = self.driver.execute_script("return localStorage['uid']")
        print ('local storage UID is changed')
        print (uid_changed)
        text = 'Landing Automation Ginseng-Local storage UID is changed'
        # recipients = ['dekela@shellanoo.com']
        # post_to_mail(recipients, text)


class AndroidWebViewTests(unittest.TestCase):
    def setUp(self):
        global PLATFORM_VERSION
        desired_caps = {
            'browserName': 'Browser',
            'platformName': 'Android',
            'platformVersion': PLATFORM_VERSION,
            'deviceName': 'Nexus 5X API 22',
            'takesScreenshot': 'true'
            # 'appPackage': 'com.android.chrome',
            # 'appActivity': 'com.google.android.apps.chrome.ChromeTabbedActivity'
        }

        # if (PLATFORM_VERSION != '5.1.1'):
        #     desired_caps['automationName'] = 'selendroid'

        self.driver = webdriver.Remote('http://localhost:4723/wd/hub',
                                       desired_caps)

    def test_get_url(self):

        try:
            global user_id_from_local
            self.driver.get('http://ginseng.life')
            sleep(4)
            self.driver.execute_script("window.scrollTo(0, 600);")
            sleep(2)
            user_id_from_local = self.driver.execute_script("return localStorage['uid']")
            sleep(2)

            prod_list = self.driver.find_elements_by_css_selector('.widget-new-product .add-to-cart-btn-link')
            if prod_list is not None:
                prod_index = randint(len(prod_list) / 2, len(prod_list) - 1)
                element = prod_list[prod_index]
                element.click()
            else:
                post_to_slack(
                    "Products are not display, Mobile Android Lollipop is failed")
                post_to_opsgenie("Products are not display, Mobile Android Lollipop failed")
                text = 'Products are not display, Automation Mobile Android Lollipop Ginseng Failed'
                recipients = ['dekela@shellanoo.com']
                post_to_mail(recipients, text)
                now = datetime.now().strftime(
                    'Products are not display- Mobile Android Lollipop is failed, %Y-%m-%d %H:%M')
                write_to_log_file_failed(now)

            sleep(2)
            add_to_cart = self.driver.find_element_by_id('product-add-to-cart1')
            if add_to_cart is not None:
                self.driver.execute_script("arguments[0].scrollIntoView(false);", add_to_cart)
                add_to_cart.click()
                sleep(2)
            else:
                # post_to_slack(
                #     "Add to cart button is missing, Mobile Android Lollipop is failed")
                # post_to_opsgenie("Add to cart button is missing, Mobile Android Lollipop failed")
                # text = 'Add to cart button is missing, Automation Mobile Android Lollipop Ginseng Failed'
                # recipients = ['dekela@shellanoo.com']
                # post_to_mail(recipients, text)
                now = datetime.now().strftime(
                    'Add to cart button is missing- Mobile Android Lollipop is failed, %Y-%m-%d %H:%M')
                write_to_log_file_failed(now)

                # No Need - Promo code removed #
                ##############################################################################
                # promo_code = self.driver.find_element_by_class_name('promo_code_toggle')
                # self.driver.execute_script("arguments[0].scrollIntoView(false);", promo_code)
                # promo_code.click()
                # sleep(2)

                # code = get_code_from_google_sheet()
                # if code is not None:
                #     self.driver.find_element_by_id("checkout_reduction_code").send_keys(code)
                #     sleep(4)
                # else:
                #     # post_to_slack(
                #     #     "Promo Code is missing, Mobile Android Lollipop is failed")
                #     # post_to_opsgenie("Promo Code is missing is missing, Mobile Android Lollipop failed")
                #     # text = 'Promo Code is missing is missing, Automation Mobile Android Lollipop Ginseng Failed'
                #     # recipients = ['dekela@shellanoo.com']
                #     # post_to_mail(recipients, text)
                #     now = datetime.now().strftime(
                #         'Promo Code is missing is missing- Mobile Android Lollipop is failed, %Y-%m-%d %H:%M')
                #     # write_to_log_file_failed(now)
                #
                # self.driver.execute_script("window.scrollTo(600, 500);")
                # self.driver.find_element_by_id("promo_code_button").click()
                # sleep(3)
                ##############################################################################

            current_url = self.driver.current_url
            sleep(2)
            button = self.driver.find_elements_by_class_name('check_out')[0]

            if button is not None:
                action = webdriver.common.action_chains.ActionChains(self.driver)
                action.move_to_element_with_offset(button, 5, 5)
                # Have to scroll to the top in order for the checkout button to be in the screen bounds #
                self.driver.execute_script("window.scrollTo(0, 0);")
                action.click()
                action.perform()
                sleep(3)

            else:
                post_to_slack(
                    "Stripe is not opened, Mobile Android Lollipop is failed")
                post_to_opsgenie("Stripe is not opened, Mobile Android Lollipop failed")
                text = 'Stripe is not opened, Automation Mobile Android Lollipop Ginseng Failed'
                recipients = ['dekela@shellanoo.com']
                post_to_mail(recipients, text)
                now = datetime.now().strftime(
                    'Stripe is not opened- Mobile Android Lollipop is failed, %Y-%m-%d %H:%M')
                write_to_log_file_failed(now)

            if current_url != self.driver.current_url:
                iframe = len(self.driver.find_elements_by_tag_name("iframe"))
                if iframe == 1 or iframe == 0:
                    sleep(3)
                    local_storage_uid(self,)
                    sleep(2)
                    self.driver.find_elements_by_class_name('field__input')[1].send_keys('dekela@shellanoo.com')
                    self.driver.find_elements_by_class_name('field__input')[2].send_keys('Dekel')
                    self.driver.find_elements_by_class_name('field__input')[3].send_keys('Amram')
                    self.driver.find_elements_by_class_name('field__input')[4].send_keys('Maskit 25')
                    self.driver.find_elements_by_class_name('field__input')[6].send_keys('Herzelia')
                    self.driver.find_elements_by_class_name('field__input')[9].send_keys('4600')
                    self.driver.find_elements_by_class_name('field__input')[10].send_keys('0547515457')
                    sleep(1)
                    continue_button = self.driver.find_elements_by_css_selector('.btn')[1]
                    sleep(1)
                    self.driver.execute_script("arguments[0].scrollIntoView(true);", continue_button)
                    continue_button.click()
                    sleep(6)
                    self.driver.find_element_by_css_selector('.step__footer__continue-btn').click()
                    sleep(5)
                    local_storage_uid(self,)
                    sleep(3)
                    code = get_code_from_google_sheet()
                    sleep(5)
                    self.driver.find_elements_by_class_name("field__input")[1].send_keys(code)
                    sleep(2)
                    self.driver.find_elements_by_class_name("btn")[1].click()
                    sleep(5)
                    # self.driver.switch_to.default_content()

                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#number")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[0])

                    sleep(2)
                    self.driver.find_element_by_id('number').send_keys(4594)
                    self.driver.find_element_by_id('number').send_keys(4083)
                    self.driver.find_element_by_id('number').send_keys(3115)
                    self.driver.find_element_by_id('number').send_keys(9870)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#name")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[1])
                        sleep(1)
                    self.driver.find_element_by_id('name').send_keys("passportcard enabled")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#expiry")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[2])
                        sleep(1)
                    self.driver.find_element_by_id('expiry').send_keys("03")
                    self.driver.find_element_by_id('expiry').send_keys("20")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(1)
                    if len(self.driver.find_elements_by_css_selector("#expiry")) == 0:
                        self.driver.switch_to.frame(self.driver.find_elements_by_tag_name("iframe")[3])
                        sleep(1)
                    self.driver.find_element_by_id('verification_value').send_keys("680")
                    sleep(2)
                    self.driver.switch_to.default_content()
                    sleep(2)
                    self.driver.execute_script("window.scrollTo(0, 600);")
                    self.driver.find_element_by_class_name('step__footer__continue-btn').click()
                    sleep(18)

                    order_element = self.driver.find_element_by_class_name('os-order-number')
                    sleep(3)
                    local_storage_uid(self,)
                    sleep(2)
                    if order_element is not None:
                        is_ord_display = order_element.is_displayed()
                        if is_ord_display:
                            success = "{order_id}, time:{time}".format(
                                order_id=order_element.text,
                                time=datetime.now().strftime(
                                    '%Y-%m-%d %H:%M, Ginseng mobile Android Lollipop Shopify successful,'))
                            write_to_log_file('ginseng', success)
                    else:
                        post_to_slack("Payment mobile Android Lollipop Shopify successfully order id is missing")
                        post_to_opsgenie("Payment mobile Android Lollipop Shopify successfully order id is missing")
                        text = 'Payment mobile Android Lollipop Shopify successfully order id is missing'
                        recipients = ['dekela@shellanoo.com']
                        post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Automation Ginseng mobile Android Lollipop Shopify Failed, %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)
                        # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #
                        sleep(10)
            else:
                customer_details = get_customer_from_google_sheet()
                if customer_details is not None:
                    self.driver.switch_to.window(self.driver.window_handles[1])
                    sleep(2)
                else:
                    post_to_slack(
                        "Customer details is missing, Mobile Android Lollipop is failed")
                    post_to_opsgenie("Customer details is missing, Mobile Android Lollipop failed")
                    text = 'Customer details is missing, Automation Mobile Android Lollipop Ginseng Failed'
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime(
                        'Customer details is missing- Mobile Android Lollipop is failed, %Y-%m-%d %H:%M')
                    write_to_log_file_failed(now)

                # print (self.driver.page_source)

                for i, detail in enumerate(customer_details):
                    if i == 5:
                        Select(self.driver.find_elements_by_class_name('control')[i].find_element_by_tag_name(
                            'select')).select_by_visible_text(detail)
                        continue
                    self.driver.find_elements_by_class_name('control')[i].send_keys(detail)

                self.driver.find_element_by_class_name('iconContinue').click()

                self.driver.find_element_by_id('card_number').send_keys('4594')
                self.driver.find_element_by_id('card_number').send_keys('4083')
                self.driver.find_element_by_id('card_number').send_keys('3115')
                self.driver.find_element_by_id('card_number').send_keys('9870')
                self.driver.find_element_by_id('cc-exp').send_keys('03')
                self.driver.find_element_by_id('cc-exp').send_keys('20')
                self.driver.find_element_by_id('cc-csc').send_keys('680')
                self.driver.find_element_by_id('submitButton').click()
                sleep(12)
                self.driver.switch_to.window(self.driver.window_handles[0])
                if len(self.driver.find_elements_by_class_name('os-order-number__')):
                    order_element = self.driver.find_element_by_class_name('os-order-number__')

                    if order_element is not None:
                        is_ord_display = order_element.is_displayed()
                        if is_ord_display:
                            success = "{order_id}, time:{time}".format(
                                order_id=order_element.text,
                                time=datetime.now().strftime(
                                    '%Y-%m-%d %H:%M, Ginseng Mobile Android Lollipop Stripe successful,'))
                            write_to_log_file('ginseng', success)
                    else:
                        post_to_slack("Payment Mobile Stripe Android Lollipop successfully order id is missing")
                        post_to_opsgenie("Payment Mobile Stripe Android Lollipop successfully order id is missing")
                        text = 'Payment Mobile Stripe Android Lollipop successfully order id is missing'
                        recipients = ['dekela@shellanoo.com']
                        post_to_mail(recipients, text)
                        now = datetime.now().strftime(
                            'Payment Mobile Stripe Android Lollipop successfully order id is missing, %Y-%m-%d %H:%M')
                        write_to_log_file_failed(now)
                        # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #
                        sleep(10)
                else:
                    post_to_slack("Automation Ginseng Mobile Android Lollipop Purchase failed order id is missing")
                    post_to_opsgenie("Automation Ginseng Mobile Android Lollipop Purchase failed order id is missing")
                    text = 'Automation Ginseng Mobile Android Lollipop Purchase failed order id is missing '
                    recipients = ['dekela@shellanoo.com']
                    post_to_mail(recipients, text)
                    now = datetime.now().strftime('Mobile Android Lollipop OrderID is missing, %Y-%m-%d %H:%M')
                    write_to_log_file_failed(now)
                    # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #

                sleep(10)

        except BaseException as w:
            import traceback
            logging.error('Failed to do something: %s', w)
            traceback.print_exc()
            post_to_slack("Payment Ginseng Mobile Android Lollipop is failed")
            post_to_opsgenie("Payment Ginseng Mobile Android failed")
            text = 'Automation Ginseng Mobile Android Lollipop Ginseng Failed'
            recipients = ['dekela@shellanoo.com']
            post_to_mail(recipients, text)
            now = datetime.now().strftime('Ginseng Mobile Android Lollipop payment failed, %Y-%m-%d %H:%M')
            write_to_log_file_failed(now)
            # self.driver.save_screenshot('../screenshots/ginseng/screenshot-%s.png' % now) # NOT working in Android #
            sleep(15)
            self.driver.quit()

    def tearDown(self):
        self.driver.quit()


if __name__ == '__main__':
    suite = unittest.TestLoader().loadTestsFromTestCase(AndroidWebViewTests)
    unittest.TextTestRunner(verbosity=2).run(suite)
