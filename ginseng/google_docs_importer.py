import httplib2
import os
from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools


def get_credentials():
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'sheets.googleapis.com-python-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def get_google_doc_by_id_and_range(spreadsheet_id, range_name="A:Z"):
    try:
        import argparse

        flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
    except ImportError:
        flags = None

    SCOPES = 'https://www.googleapis.com/auth/spreadsheets.readonly'
    CLIENT_SECRET_FILE = 'client_secret.json'
    APPLICATION_NAME = 'Affiliates'
    credentials = get_credentials()

    http = credentials.authorize(httplib2.Http())
    discoveryUrl = ('https://sheets.googleapis.com/$discovery/rest?'
                    'version=v4')
    service = discovery.build('sheets', 'v4', http=http,
                              discoveryServiceUrl=discoveryUrl)

    result = service.spreadsheets().values().get(
        spreadsheetId=spreadsheet_id, range=range_name).execute()
    values = result.get('values', [])

    if not values:
        print('No data found.')

    spreadsheet_dictionary = []
    headers_array = []

    first = True
    for row in values:
        if first:
            for cell in row:
                headers_array.append(str(cell))
            first = False
            continue
        else:
            row_dictionary = {}
            for index, cell in enumerate(row):
                key = headers_array[index].encode('utf-8')
                row_dictionary[key] = cell.encode('utf-8')
            spreadsheet_dictionary.append(row_dictionary)

    return spreadsheet_dictionary

    #
    # if __name__ == '__main__':
    #     spreadsheet_dictionary = get_google_doc_by_id_and_range('1fGfKsX9phIqDKq9NhTMqNZLUS7uF27c87QyslCOzjT4', '!A:T')
