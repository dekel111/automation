
import unittest
import os
from random import randint
from appium import webdriver
from time import sleep
from selenium.webdriver.common.touch_actions import TouchActions

class SimpleIOSTests(unittest.TestCase):

    def setUp(self):
        # set up appium
        app = os.path.abspath('/Users/dekel/Desktop/Radiowave.app')
        self.driver = webdriver.Remote(
                command_executor='http://127.0.0.1:4723/wd/hub',
                desired_capabilities={
                    'app': app,
                    'platformName': 'iOS',
                    'platformVersion': '9.2',
                    'deviceName': 'iPhone 6'
                })

    def tearDown(self):
        self.driver.quit()


    def test_ui_computation(self):


        self.driver.swipe(200,200,-200,200,5000)
        first_join_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]').is_displayed() # Tutorial first page text #
        print(first_join_text)
        self.driver.swipe(200,200,-200,200,5000)
        second_join_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]').is_displayed() # Tutorial second page text #
        print(second_join_text)
        third_join_text = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAScrollView[1]/UIAStaticText[1]').is_displayed() # Tutorial third page text #
        print(third_join_text)


        self.driver.find_element_by_id('Get Started').click()  # Tap on Get start#

        chillout = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').is_displayed() # Is chillout display #
        print(chillout)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click() # Tap on Chillout and navigate to categories page#

        first = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[1]/UIAButton[1]')
        first == self.driver.find_element_by_id('Chillout')  # Is Chillout display first #
        print first.text    # Print Chillout #
        self.driver.background_app(1)     # Minimize the app #
        sleep(10)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click() # Back to categories #


        # Print categories and checking if any button is selected in categories page s#
        i = 1
        while i < 18:
            xpath = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[1]' % i
            group = self.driver.find_element_by_xpath(xpath).text
            print group
            xpath_1 = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[1]' % i
            selec = self.driver.find_element_by_xpath(xpath_1).is_selected()
            print selec
            i +=  1


        close = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').is_displayed() # Is close display #
        print close

        listen = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]').is_displayed()  # Is the text  "You are listening" to display #
        print listen

        category = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]').is_displayed() # Is the current name of the category displayed #
        print category

        play = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[2]/UIAButton[1]').click() # Click on the second category #


        # player screen #
        play = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').is_displayed()    # Is category named display #
        print play
        song = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[1]').is_displayed()   # Is song name display #
        print song
        singer = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]').is_displayed()  # Is singer name display #
        print singer
        share = self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[3]').is_displayed()   # Is share display #
        print share
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]').click() # Pause the music #
        sleep(5)
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAStaticText[2]').click() # Play the music #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click()   # Tap on the category to navigate to the category page #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click()   # Tap on Close #
        self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click()   # Tap on the category to navigate to the category page #

        # Click on each category #
        i = 1
        while i < 18:
            xpath = '//UIAApplication[1]/UIAWindow[1]/UIACollectionView[1]/UIACollectionCell[%d]/UIAButton[1]' % i
            cat = self.driver.find_element_by_xpath(xpath).click()
            self.driver.find_element_by_xpath('//UIAApplication[1]/UIAWindow[1]/UIAButton[1]').click()
            sleep(5)
            i +=  1

        sleep(5)








#if __name__ == '__main__':
#    suite = unittest.TestLoader().loadTestsFromTestCase(SimpleIOSTests)
#    unittest.TextTestRunner(verbosity=2).run(suite)
