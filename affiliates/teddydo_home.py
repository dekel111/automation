import logging
import sys
import time

from selenium import webdriver
from selenium.webdriver.common.action_chains import ActionChains

config = {
    "teddydo": {

        "title": "TEDDYDO - the kids fashion search engine"
    },
    "bellaboo": {

    },
    "huntsberg": {

    }
}



def check_footer_categories(driver):
    cat_opt = ["girls", "boys", "baby-girls", "baby-boys"]
    for i in range(len(cat_opt)):

        count = 0
        sub_menu = driver.find_elements_by_css_selector(".dropdown-toggle-" + cat_opt[count] + " li .text")

        for n in sub_menu:
            cat_menu = driver.find_element_by_class_name("pull-left")  # Mousehover on Category #

            hover = ActionChains(driver).move_to_element(cat_menu)  # Mousehover on Category #
            hover.perform()
            time.sleep(1)

            gender_list_cat = driver.find_elements_by_css_selector('.drop-down-list ul li')

            girls_hover = driver.find_elements_by_css_selector(".dropdown-toggle-" + cat_opt[i] + " label")[0]
            hover = ActionChains(driver).move_to_element(girls_hover)  # Mousehover on Category girl #
            hover.perform()

            sub_menu = driver.find_elements_by_css_selector(".dropdown-toggle-" + cat_opt[i] + " li .text")

            print(sub_menu[count].text)
            sub_menu[count].click()
            is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display #
            is_Results_displayed = not is_no_Results_displayed.is_displayed()
            print(is_Results_displayed)

            time.sleep(1)

            driver.find_element_by_class_name("search-logo").click()
            time.sleep(1)
            count += 1

            # count_1 = 0
            # sum = 40
            # cat_opt_4 = driver.find_elements_by_css_selector(".drop-down-list ul li")[sum].text
            # for x in range(len(cat_opt_4)):
            #         cat_menu = driver.find_element_by_class_name("pull-left")  # Mousehover on Category#
            #         hover = ActionChains(driver).move_to_element(cat_menu)  # Mousehover on Category#
            #         hover.perform()
            #         time.sleep(1)
            #         #gender_list_cat = driver.find_elements_by_css_selector('.drop-down-list ul li')
            #
            #         girls_hover = driver.find_elements_by_css_selector("dropdown-toggle-baby-boys")[3]
            #         hover = ActionChains(driver).move_to_element(girls_hover)  # Mousehover on Category baby boys #
            #         hover.perform()
            #
            #         cat_opt_4 = driver.find_elements_by_css_selector(".drop-down-list ul li")[sum].text
            #
            #         print(cat_opt_4[count_1].text)
            #         cat_opt_4[count_1].click()
            #         is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
            #         is_Results_displayed = not is_no_Results_displayed.is_displayed()
            #         print(is_Results_displayed)
            #
            #         time.sleep(1)
            #
            #         driver.find_element_by_class_name("search-logo").click()
            #         time.sleep(1)
            #         count_1 += 1
            #         sum += 1


def all_checks(driver):
    cookies_label = driver.find_element_by_id("cookies-notification")  # Find the cookies #
    is_cookie_displayed_on_first_run = cookies_label.is_displayed()  # Is cookie displayed #

    driver.find_element_by_css_selector("#close-cookies-notification span").click()  # Close cookie #
    is_cookie_not_displayed = not cookies_label.is_displayed()

    search_bar_origin_place_holder = driver.find_element_by_id("autocomplete").get_attribute(
        "placeholder")  # Placeholder#
    print(search_bar_origin_place_holder)  # Print placeholder#

    time.sleep(6)

    is_placeholder_has_changed = search_bar_origin_place_holder != driver.find_element_by_id("autocomplete").get_attribute("placeholder")  # Checking if placeholder changed #

    # Checking placeholder text #
    placeholders_array = []
    while (True):
        time.sleep(3)
        place_holder = driver.find_element_by_id("autocomplete").get_attribute("placeholder")
        print(place_holder)
        if place_holder in placeholders_array:
            break

        placeholders_array.append(place_holder)

    is_have_over_text = driver.find_element_by_class_name("over-brands").text == "*Over 28,000 fashion related brands"

    driver.find_element_by_class_name("form-control").send_keys("jeans")  # Insert jeans text to search field #

    driver.find_element_by_class_name("btn-search").click()  # Press on search button #

    driver.find_element_by_class_name("search-logo").click()  # Back to home page #

    driver.find_element_by_class_name("form-control").send_keys("d")  # Insert d to search field#

    time.sleep(3)

    suggestions = driver.find_element_by_class_name('autocomplete-suggestions')

    is_autocomplete_displayed = suggestions.is_displayed()  # checking if autocomplete display #

    autocomplete = driver.find_elements_by_class_name("autocomplete-suggestion")

    autocomplete_required_length = 10

    auto_complete_result = len(autocomplete) < autocomplete_required_length  # Checking if autocomplete less than 10 #

    print(autocomplete)
    time.sleep(4)

    autocomplete[1].click()  # Select an item from autocomplete #

    driver.find_element_by_class_name("search-logo").click()  # Back to home page #

    time.sleep(2)

    # Checking footer #

    driver.find_elements_by_css_selector(".filters li")[53].click()  # Click on partner#
    is_Results_displayed = driver.find_element_by_class_name("logo-list")  # Checking if results display#
    is_Results_displayed = is_Results_displayed.is_displayed()
    print(is_Results_displayed)
    # driver.find_element_by_css_selector("#close-cookies-notification span").click() # Close cookie #

    driver.find_elements_by_css_selector(".filters li")[1].click()  # Click on Home #

    driver.find_elements_by_css_selector(".filters li")[2].click()  # Click on About#
    is_Results_displayed = driver.find_element_by_class_name("inner-page")  # checking if results display #
    is_Results_displayed = is_Results_displayed.is_displayed()
    print(is_Results_displayed)

    driver.find_elements_by_css_selector(".filters li")[1].click()  # Click on Home #

    driver.find_elements_by_css_selector(".filters li")[54].click()  # Click on Terms #
    is_Results_displayed = driver.find_element_by_class_name("inner-page")  # checking if results display #
    is_Results_displayed = is_Results_displayed.is_displayed()
    print(is_Results_displayed)

    driver.find_elements_by_css_selector(".filters li")[1].click()  # Click on Home #

    driver.find_elements_by_css_selector(".filters li")[55].click()  # Click on About #
    driver.find_elements_by_css_selector(".filters li")[1].click()  # Click on Home #
    # driver.find_elements_by_css_selector(".filters li")[56].click() # Click on Contact Us #
    # driver.find_elements_by_css_selector(".filters li")[1].click() # Click on Home #
    # driver.find_elements_by_css_selector(".btn-social")[2].click() # Click on Facebook #
    # driver.find_elements_by_css_selector(".btn-social")[3].click() # Click on Instagram #


if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    site = "teddydo"
    # if len(sys.argv) > 1:
    #     site = sys.argv[1]
    # else:
    #     print("Site not specified")
    #     exit()

    driver = webdriver.Firefox()
    driver.get("https://www." + site + ".com/")

    logging.info('Starting automation on %s Site' % site)

    # all_checks(driver)
    check_footer_categories(driver)
