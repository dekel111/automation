import unittest
import logging
import traceback
import sys
from selenium import webdriver
import time
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.keys import Keys

config = {
    "teddydo": {

        "title": "TEDDYDO - the kids fashion search engine"
    },
    "bellaboo": {

    },
    "huntsberg": {

    }
}

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    site = ""
    if len(sys.argv) > 1:
        site = sys.argv[1]
    else:
        print("Site not specified")
        exit()

    driver = webdriver.Firefox()
    driver.get("https://www." + site + ".com/")

    logging.info('Starting automation on %s Site' % site)



    driver.find_element_by_class_name("form-control").send_keys("nike")  # Insert jeans text to search field #
    time.sleep(3)

    suggestions = driver.find_element_by_class_name('autocomplete-suggestions')

    is_autocomplete_displayed = suggestions.is_displayed()  # checking if autocomplete display #

    autocomplete = driver.find_elements_by_class_name("autocomplete-suggestion")

    autocomplete_required_length = 10

    auto_complete_result = len(autocomplete) < autocomplete_required_length  # Checking if autocomplete less than 10 #

    print(autocomplete)
    time.sleep(2)
    autocomplete[1].click()

    is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
    is_Results_displayed = not is_no_Results_displayed.is_displayed()
    print(is_Results_displayed)

    driver.find_element_by_class_name("search-logo").click()  # Back to home page #

    time.sleep(2)

    exit()
