import unittest
import logging
import traceback
import sys
from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException
import time
from selenium.webdriver.common.action_chains import ActionChains
from datetime import datetime, date
from helper_functions import write_to_log_file

config = {
    "teddydo": {

        "title": "TEDDYDO - the kids fashion search engine"
    },
    "bellaboo": {

    },
    "huntsberg": {

    }
}


def search_by_text(text_search=""):
    global driver
    driver.find_element_by_class_name("form-control").send_keys(text_search)
    driver.find_element_by_class_name("btn-search").click()


def is_cookies_tag_is_display():
    cookies_label = driver.find_elements_by_css_selector("#cookies_text_container")
    if len(cookies_label) > 0:
        print(cookies_label[0].text)
        driver.find_element_by_css_selector("#close-cookies-notification span").click()  # close cookie#

        return cookies_label[0].is_displayed()
    else:
        return False

        # is_cookie_displayed_on_first_run = cookies_label.is_displayed()  # is cookie displayed#
        # is_cookie_not_displayed = not cookies_label.is_displayed()


def start_program():
    global driver
    driver.maximize_window()
    search_by_text("shirts")
    time.sleep(2)
    cookies_displayed = is_cookies_tag_is_display()
    if not cookies_displayed:
        print("cokkies message not displayed")


        # for Chrome use this for scroll down #
        # driver.execute_script("window.scrollTo(0, 9000)")

        # This scroll is for firefox #
        # scroll_time = 13
        # scroll = driver.find_element_by_class_name("content")  # Scroll down#
        # scroll.send_keys(Keys.PAGE_DOWN)
        # scroll = driver.find_element_by_class_name("content")  # Scroll down
        # scroll.send_keys(Keys.PAGE_DOWN)
        # loading_icon = driver.find_element_by_class_name("loading")  # checking if Loading icon display#
        # loading_icon = loading_icon.is_displayed()
        # time.sleep(2)

        # Checking subscribe pop up window #
        # Check_text_1 = driver.find_element_by_class_name("modal-newsletter-head").is_displayed()
        # print(Check_text_1)
        # Check_text_1 = driver.find_element_by_class_name("modal-newsletter-text").is_displayed()
        # print(Check_text_1)
        # Check_text_1 = driver.find_element_by_class_name("modal-newsletter-subscribe").is_displayed()
        # print(Check_text_1)
        #
        #
        # driver.find_element_by_class_name("form-control").send_keys("ddddd")
        # driver.find_element_by_css_selector(".modal-newsletter-button").click() # Close pop up #
        # error_msg = driver.find_element_by_class_name("error-msg").is_displayed()
        # print(error_msg)
        # driver.find_element_by_class_name("form-control").clear()
        # driver.find_element_by_css_selector(".exit-modal img").click() # Close pop up #


def sort_by():
    # Checking filter Sort By #
    element_to_hover_over = driver.find_element_by_css_selector(".dropdown")  # Mousehover Sort by #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Sort by #
    hover.perform()
    sort_by_elements = element_to_hover_over.find_elements_by_class_name('text')

    i = 0
    count = 0

    sort_config = {"Relevance": False,
                   "Price: Low to High": True,
                   "Price: High to Low": True
                   }

    for i in xrange(len(sort_by_elements)):
        # From here i need.
        sort_type = sort_by_elements[i].text
        print(sort_by_elements[i].text)

        is_need_to_sort = sort_config.get(sort_type, False)

        element_to_hover_over = driver.find_element_by_class_name("dropdown")
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Sort by #
        hover.perform()

        sort_by_elements[i].click()
        time.sleep(2)

        # Validate the Sort By#
        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

        results_prices = driver.find_elements_by_css_selector("#results .price")

        if is_need_to_sort:

            # If the sort is high to low we need to flip the list.
            if sort_type == "Price: High to Low":
                results_prices.reverse()

            first_element = float(results_prices[0].text.replace("$", ""))

            for price_element in results_prices:

                product_price = float(price_element.text.replace("$", ""))
                if product_price >= first_element:
                    print(first_element)
                    first_element = product_price
                else:
                    print ("not sorted")
                    # break
                    time.sleep(2)

                    # Checking filter Prices#


def filter_price():
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[1]
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Price #
    hover.perform()
    price_elements = element_to_hover_over.find_elements_by_class_name("text")

    i = 0
    count = 0

    for i in xrange(len(price_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[1]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Price #
        hover.perform()
        if i > 0:
            price_elements[i - 1].click()
            time.sleep(1)
            # first_element = results_prices[0].text.replace("USD", "")
            # last_element = results_prices[len(results_prices)].text.replace("USD", "")
            # if first_element <=
        print(price_elements[i].text)
        price_elements[i].click()
        time.sleep(2)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

        # Clear Price filter #
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[1]  # Mousehover Brand #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Brand #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()  # Clear Brand #


    # Checking Category filter #


def category_filter():
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[2]  # Mousehover Category Search #
    print (element_to_hover_over.find_element_by_tag_name('span').text)
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Category Search #
    hover.perform()
    category_elements = element_to_hover_over.find_elements_by_class_name('list-element')

    i = 0
    count = 0

    # print(category_elements)
    for i in xrange(len(category_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[2]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Category #
        hover.perform()

        if i > 0:
            category_elements[i - 1].click()
            time.sleep(1)
        print(category_elements[i].text)
        category_elements[i].click()
        time.sleep(2)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[2]  # Clear Category filter #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Clear Category filter #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()

    # Checking Color filter #


def color_filter():
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[3]  # Mousehover on Color #
    print (element_to_hover_over.find_element_by_tag_name('span').text)
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Color #
    hover.perform()
    color_elements = element_to_hover_over.find_elements_by_class_name('list-element')  # insert color to list #

    i = 0
    count = 0

    for i in xrange(len(color_elements)):
        print(color_elements[i].text)
        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[3]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Color #
        hover.perform()

        print(color_elements[i].find_element_by_tag_name('input').get_attribute("value"))  # print color #
        color_elements[i].click()  # Click on Color #
        time.sleep(2)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[3]  # Clear Color filter #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Clear Color filter #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()

    # Checking Store filter #


def store_filter():
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[4]  # Mousehover on Store #
    print (element_to_hover_over.find_element_by_tag_name('span').text)
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Store #
    hover.perform()
    store_elements = element_to_hover_over.find_elements_by_css_selector(".list-element .text")

    i = 0
    count = 0

    # print(store_elements)
    for i in xrange(len(store_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[4]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Store #
        hover.perform()
        if i > 0:
            store_elements[i - 1].click()
            time.sleep(2)

        print(store_elements[i].text)
        store_elements[i].click()
        time.sleep(2)

        store_name = driver.find_elements_by_css_selector("#results .site")
        for n in store_name:
            if store_elements[i].text.lower() != n.text.lower():
                print("Store name not match")
                print(store_elements[i].text)
                print(n.text)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[4]  # Clear Store filter #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Clear Store filter #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()  # Clear Store filter #

    # Checking Brand filter #


def brand_filter():
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[5]  # Mousehover Brand #
    print (element_to_hover_over.find_element_by_tag_name('span').text)
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Brand #
    hover.perform()
    brand_elements = element_to_hover_over.find_elements_by_class_name('list-element')

    i = 0
    count = 0

    for i in xrange(len(brand_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[5]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Store #
        hover.perform()
        if i > 0:
            brand_elements[i - 1].click()
            time.sleep(1)
        print(brand_elements[i].text)
        brand_elements[i].click()
        time.sleep(2)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

        # Clear Brand filter #
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[5]  # Mousehover Brand #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Brand #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()  # Clear Brand #


def on_sale():
    on_sale = driver.find_element_by_class_name("onsale")
    print(on_sale.text)
    driver.find_element_by_class_name("onsale").click()  # Select Onsale #
    time.sleep(2)
    is_Results_displayed = driver.find_element_by_id("results")  # checking if results display#
    is_Results_displayed = is_Results_displayed.is_displayed()
    print(is_Results_displayed)
    driver.find_element_by_class_name("onsale").click()  # deselect On Sale #
    time.sleep(5)


if __name__ == '__main__':
    try:
        logging.basicConfig(level=logging.INFO)

        site = "bellaboo"
        # driver = webdriver.Firefox()
        driver = webdriver.Chrome('/usr/local/bin/chromedriver')  # For Chrome #
        # driver.get("https://www." + site + ".com/")
        driver.get("http://bellaboo.mmtst.com")

        logging.info('Starting automation on %s Site' % site)
        start_program()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
    count = 0

    try:
        sort_by()

    except NoSuchElementException as e:
        print "error in sort by {error}".format(error=e)
        sort_by()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)

    try:
        filter_price()
    except NoSuchElementException as e:
        print "error in filter price {error}".format(error=e)
        filter_price()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)

    try:
        category_filter()
    except NoSuchElementException as e:
        print "error in filter price {error}".format(error=e)
        category_filter()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)

    try:
        color_filter()
    except NoSuchElementException as e:
        print "error in filter price {error}".format(error=e)
        color_filter()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)

    try:
        store_filter()
    except NoSuchElementException as e:
        print "error in filter price {error}".format(error=e)
        store_filter()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)

    try:
        brand_filter()
    except NoSuchElementException as e:
        print "error in filter price {error}".format(error=e)
        brand_filter()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
    try:
        on_sale()
    except NoSuchElementException as e:
        print "error in filter price {error}".format(error=e)
        on_sale()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        driver.save_screenshot('../screenshots/affiliates/screenshot-%s.png' % m_now)
    success = datetime.now().strftime('%Y-%m-%d_%H-%M, Bellaboo successful')
    write_to_log_file('affiliates', success)
    time.sleep(5)
    driver.close()







    # try:
    #     # asserting Here. #
    #     assert config[site]["title"] in driver.title
    #     assert is_cookie_displayed_on_first_run
    #     assert is_cookie_not_displayed
    #     assert is_placeholder_has_changed
    #     assert is_have_over_text
    #     assert is_autocomplete_displayed
    #     # assert autocomplete_length
    #     assert auto_complete_result
    #     assert loading_icon
    #     assert Onsale_is_selected
    #     assert is_Results_displayed
    #     assert results_price
    #     assert store_elements
    #     assert store_name
    #
    # except AssertionError:
    #     tb = sys.exc_info()
    #     traceback.print_tb(tb)
    #     tb_info = traceback.extract_tb(tb)
    #     filename, line, func, text = tb_info[-1]
    #
    #     print('An error occurred on line {} in statement {}'.format(line, text))


    # cookies = driver.get_cookies()

    # for cookie in cookies:
    #    print cookie
