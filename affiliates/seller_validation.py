from helper_functions import get_google_sheet_values_by_id
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from mail import post_to_mail
from selenium import webdriver
import time

websites = {
    'Bellaboo': 'https://www.bellaboo.com/',
    'Huntsberg': 'http://huntsberg.com',
    'TeddyDo': 'https://www.teddydo.com/'
}

current_site = ''
buffer_string = ""
if __name__ == '__main__':

    affiliates_networks = []

    doc_id = '1fGfKsX9phIqDKq9NhTMqNZLUS7uF27c87QyslCOzjT4'

    doc_values = get_google_sheet_values_by_id(doc_id)
    doc_values.pop(0)
    for row in doc_values:
        if row[6] == 'Yes':
            aff_sites = row[4].split(',')
            for site in aff_sites:
                affiliates_networks.append({'name': row[2], 'site': site})

                # sorted(affiliates_networks, key=lambda name: 'site')  # Sorted #

    ordered_list = sorted(affiliates_networks, key=lambda k: k['site'])

    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    for row in ordered_list:
        site = row['site'].strip()
        if current_site != site:
            print "loading ", site
        try:
            driver.get(websites[site])
            time.sleep(2)
            driver.execute_script('window.stop();')
            current_site = site

            driver.find_element_by_id('autocomplete').send_keys(row['name'])
            driver.find_element_by_class_name('icon-search').click()
            time.sleep(2)
            driver.execute_script('window.stop();')
            if len(driver.find_elements_by_class_name('results-box')) == 0:
                # print 'Seller:', row['name'], ' Site:', row['site'], ' Does not have result..'
                print "Seller {seller_name},  site {seller_site},  Does not have result..".format(
                    seller_name=row['name'], seller_site=row['site'])

                # buffer_string += 'Seller: ' + {row['name']} + ' Site:' + str(
                #     row['site']) + ' Does not have result..\n'
                buffer_string += 'Seller: {name}, Site: {site}, Does not have results,\n'.format(name=row['name'],
                                                                                               site=row['site'])

        except TimeoutException as e:
            print "Error with {seller_name} with site {site} TimeoutException".format(seller_nam=row['name'],
                                                                                      site=row['site'])
            continue
        except NoSuchElementException as e:
            print "Error with {seller_name} with site {site} NoSuchElementException".format(seller_nam=row['name'],
                                                                                            site=row['site'])
            continue
        except BaseException as e:
            print "Error with {seller_name} with site {site} TimeoutException".format(seller_nam=row['name'],
                                                                                      site=row['site'])
            continue

        driver.find_element_by_id('autocomplete').clear()
        driver.back()
    recipients = ['ce@shellanoo.com', 'db@shellanoo.com']
    post_to_mail(recipients=recipients, text=buffer_string)
