import unittest
import logging
import traceback
import sys
from selenium import webdriver
import time
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.keys import Keys
#
# config = {
#     "teddydo": {
#
#         "title": "TEDDYDO - the kids fashion search engine"
#     },
#     "bellaboo": {
#
#     },
#     "huntsberg": {
#
#     }
# }

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    site = ""
    if len(sys.argv) > 1:
        site = sys.argv[1]
    else:
        print "Site not specified"
        # exit()

    # driver = webdriver.Firefox()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')  # For Chrome #
    # driver.get("https://www." + site + ".com/")
    driver.get("http://teddydo.com")
    logging.info('Starting automation on %s Site' % site)

    driver.find_element_by_class_name("form-control").send_keys("jeans")  # Insert jeans text to search field #

    driver.find_element_by_class_name("btn-search").click()

    # Checking filter Sort By #
    element_to_hover_over = driver.find_element_by_css_selector(".sort-by .dropdown")  # Mousehover Sort by #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Sort by #
    hover.perform()
    sort_by_elements = element_to_hover_over.find_elements_by_class_name('text')

    i = 0
    count = 0

    sort_config = {"Relevance": False,
                   "Price: Low to High": True,
                   "Price: High to Low": True
                   }

    for i in xrange(len(sort_by_elements)):

        sort_type = sort_by_elements[i].text
        print(sort_by_elements[i].text)




        is_need_to_sort = sort_config.get(sort_type, False)


        element_to_hover_over = driver.find_element_by_class_name("dropdown")
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Sort by #
        hover.perform()

        sort_by_elements[i].click()
        time.sleep(2)

        # Validate the Sort By#
        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

        results_prices = driver.find_elements_by_css_selector("#results .price")

        if is_need_to_sort:



            # If the sort is high to low we need to flip the list.
            if sort_type == "Price: High to Low":
                results_prices.reverse()

            first_element = results_prices[0].text.replace("USD", "")

            for price_element in results_prices:

                product_price = price_element.text.replace("USD", "")
                if product_price >= first_element:
                    print(first_element)
                    first_element = product_price
                else:
                    print ("not sorted")
                    # break
                    time.sleep(2)


    # Checking filter Prices#
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[1]
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Price #
    hover.perform()
    price_elements = element_to_hover_over.find_elements_by_class_name("text")

    i = 0
    count = 0

    for i in xrange(len(price_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[1]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Price #
        hover.perform()
        if i > 0:
            price_elements[i - 1].click()
            time.sleep(1)

        print(price_elements[i].text)
        price_elements[i].click()
        time.sleep(2)


        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

      # Clear Price filter #
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[1]  # Mousehover Brand #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Brand #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()  # Clear Brand #

    #Checking gender filter #

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[2]  # Mousehover Gender #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Gender #
    hover.perform()
    gender_elements = driver.find_elements_by_css_selector('.gender-dropdown-list .dropdown-menu li')

    i = 0
    count = 0

    print(gender_elements)
    for i in xrange(len(gender_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[2]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Gender #
        hover.perform()
        if i > 0:
            gender_elements[i - 1].click()
            time.sleep(1)

        gender_elements[i].click()
        time.sleep(2)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    #Checking Category filter #
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[3]  # Mousehover Category Search #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Category Search #
    hover.perform()
    category_elements = element_to_hover_over.find_elements_by_class_name('text')

    i = 0
    count = 0

    print(category_elements)
    for i in xrange(len(category_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[3]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Price #
        hover.perform()
        if i > 0:
            category_elements[i - 1].click()
            time.sleep(1)
        print(category_elements[i].text)
        category_elements[i].click()
        time.sleep(2)


        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[3]  # Clear Category filter #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Clear Category filter #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()


    # Checking Color filter #
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[4]  # Mousehover on Color #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Color #
    hover.perform()
    color_elements = element_to_hover_over.find_elements_by_class_name('pointer')  # Select Black color #

    i = 0
    count = 0

    print(color_elements)
    for i in xrange(len(color_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[4]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Color #
        hover.perform()

        print(color_elements[i].find_element_by_tag_name('input').get_attribute("value"))

        color_elements[i].click()
        time.sleep(2)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[4]  # Clear Category filter #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Clear Category filter #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()

    # Checking Store filter #

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[5]  # Mousehover on Store #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Store #
    hover.perform()
    store_elements = element_to_hover_over.find_elements_by_class_name('text')

    i = 0
    count = 0

    print(store_elements)
    for i in xrange(len(store_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[5]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Store #
        hover.perform()
        if i > 0:
            store_elements[i - 1].click()
            time.sleep(1)


        print(store_elements[i].text)

        store_elements[i].click()
        time.sleep(2)

        store_name = driver.find_elements_by_css_selector("#results .site")
        for n in store_name:
            if store_elements[i].text.lower() != n.text.lower():
                print("Store name not match")
                print(store_elements[i].text)
                print(n.text)



        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[5]  # Clear Store filter #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Clear Store filter #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click() # Clear Store filter #




    # Checking Brand filter #
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[6]  # Mousehover Brand #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Brand #
    hover.perform()
    brand_elements = element_to_hover_over.find_elements_by_class_name('text')

    i = 0
    count = 0

    print(brand_elements)
    for i in xrange(len(brand_elements)):

        element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[6]
        hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover on Store #
        hover.perform()
        if i > 0:
            brand_elements[i - 1].click()
            time.sleep(1)

        brand_elements[i].click()
        time.sleep(2)

        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)

    # Clear Brand filter #
    element_to_hover_over = driver.find_elements_by_css_selector(".dropdown")[6]  # Mousehover Brand #
    hover = ActionChains(driver).move_to_element(element_to_hover_over)  # Mousehover Brand #
    hover.perform()
    element_to_hover_over.find_element_by_css_selector(".dropdown-menu .border .clear").click()  # Clear Brand #




    # Checking On Sale filter #
    Onsale_is_selected = driver.find_element_by_class_name("onsale")  # Checking if Onsale is selected #
    Onsale_is_selected = Onsale_is_selected.is_selected()  # Checking if Onsale is selected #

    driver.find_element_by_class_name("onsale").click()  # Select Onsale #

    is_Results_displayed = driver.find_element_by_id("results")  # checking if results display#
    is_Results_displayed = is_Results_displayed.is_displayed()
    print(is_Results_displayed)

    driver.find_element_by_class_name("onsale").click()  # deselect On Sale #

    exit()




    try:
        # asserting Here. #
        assert config[site]["title"] in driver.title
        assert is_cookie_displayed_on_first_run
        assert is_cookie_not_displayed
        assert is_placeholder_has_changed
        assert is_have_over_text
        assert is_autocomplete_displayed
        # assert autocomplete_length
        assert auto_complete_result
        assert loading_icon
        assert Onsale_is_selected
        assert is_Results_displayed
        assert results_price
        assert store_elements
        assert store_name

    except AssertionError:
        tb = sys.exc_info()
        traceback.print_tb(tb)
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]

        print('An error occurred on line {} in statement {}'.format(line, text))


        # cookies = driver.get_cookies()

        # for cookie in cookies:
        #    print cookie()
