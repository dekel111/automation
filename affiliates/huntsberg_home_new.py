import unittest
import logging
import traceback
import sys
import webbrowser
from datetime import datetime, date
from selenium import webdriver
import time
from helper_functions import write_to_log_file
from selenium.webdriver import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

# config = {
#     "teddydo": {
#
#         "title": "TEDDYDO - the kids fashion search engine"
#     },
#     "bellaboo": {
#
#     },
#     "huntsberg": {
#
#     }
# }



def start_prog():
    global driver
    title = driver.title
    print(title)  # Print title #

    # Checking Cookies #
    cookies_label = driver.find_element_by_id("cookies-notification").text
    print(cookies_label)
    cookies_label = driver.find_element_by_id("cookies-notification")  # Find the cookies #
    is_cookie_displayed_on_first_run = cookies_label.is_displayed()  # Is cookie displayed #

    driver.find_element_by_css_selector("#close-cookies-notification span").click()  # Close cookie#
    is_cookie_not_displayed = not cookies_label.is_displayed()

    search_bar_origin_place_holder = driver.find_element_by_id("autocomplete").get_attribute("placeholder")  # Placeholder #
    print(search_bar_origin_place_holder) # Print placeholder #
    time.sleep(6)


    # Checking Search #
def search():
    global driver
    driver.find_element_by_id("autocomplete").send_keys("jeans")  # Insert jeans text to search field #
    driver.find_element_by_class_name("btn-search").click()   # Press on search button #
    if len(driver.find_elements_by_class_name('results-box')) == 0:     # Checking if results displayed #
        print('no results')                                             # print if no results #
    driver.find_element_by_class_name("search-logo").click()  # Back to home page #




    # Checking Shop now #
def shop_now():
    global driver
    banner_text = driver.find_element_by_class_name('banner-text-1').text
    driver.find_elements_by_class_name('btn')[2].click()
    driver.find_element_by_class_name("search-logo").click()  # Back to home page #
    print(banner_text)


    # Checking autocomplete #
def autocomplete():
    global driver
    driver.find_element_by_class_name("form-control").send_keys("d")  # Insert d to search field#
    time.sleep(2)

    suggestions = driver.find_element_by_class_name('autocomplete-suggestions')

    is_autocomplete_displayed = suggestions.is_displayed()  # checking if autocomplete display #
    print(is_autocomplete_displayed)

    autocomplete_1 = driver.find_elements_by_class_name("autocomplete-suggestion") # Print Autocomplete list #
    for x in xrange(len(autocomplete_1)):
        autocomplete_1 = driver.find_elements_by_class_name("autocomplete-suggestion")[x] # Print Autocomplete list #
        print(autocomplete_1.text)     # Print Autocomplete list #

    autocomplete = driver.find_elements_by_class_name("autocomplete-suggestion")

    autocomplete_required_length = 10

    auto_complete_result = len(autocomplete) < autocomplete_required_length  # Checking if autocomplete less than 10 #
    print(auto_complete_result, 'autocomplete length is 10')
    autocomplete[0].click()  # Select an item from autocomplete #
    if len(driver.find_elements_by_class_name('results-box')) == 0:     # Checking if results displayed #
        print('no results')
    driver.find_element_by_class_name("search-logo").click()  # Back to home page #

    time.sleep(2)



    # Checking Shops #
def shops():
    global driver
    nav = driver.find_elements_by_css_selector('.navbar-nav li')
    for i in xrange(len(nav)):
        pr = driver.find_elements_by_css_selector('.navbar-nav li')[i].text
        driver.find_elements_by_css_selector('.navbar-nav li')[i].click()
        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # Checking if results display #
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(pr)
        print(is_Results_displayed)
        driver.find_element_by_class_name("search-logo").click()

    time.sleep(2)

    prd_title = driver.find_element_by_css_selector('.products-title span')  # Print "TRENDING NOW" #
    print(prd_title.text)



    # Checking trending now #
def trending():
    global driver
    itm = driver.find_elements_by_css_selector('.product-item')
    for i in xrange(len(itm)):
        trend_name = driver.find_elements_by_css_selector('.list-group-item-heading')[i].text
        print(trend_name)
        driver.find_elements_by_css_selector('.product-item')[i].click()
        is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # Checking if results display#
        is_Results_displayed = not is_no_Results_displayed.is_displayed()
        print(is_Results_displayed)
        driver.find_element_by_class_name("search-logo").click()

    driver.execute_script("document.getElementsByTagName('body')[0].style.overflowX = 'visible';")

    driver.execute_script("window.scrollTo(0, 9000)")   # Scroll page down #
    time.sleep(1)



    # Checking Editor Blog #
def editor_blog():
    global driver
    box_title = driver.find_elements_by_css_selector('.box-title')[1]  # Print "EDITORS' BLOG" #
    print(box_title.text)

    blog = driver.find_elements_by_css_selector('.blog-post-img')
    for y in xrange(len(blog)):
        editor_name = driver.find_elements_by_css_selector('.blog-text')[y].text
        print(editor_name)
        blog = driver.find_elements_by_css_selector('.blog-post-img')[y]
        driver.find_elements_by_css_selector('.blog-post-img')[y].click()
        time.sleep(2)
        driver.close()
        driver = webdriver.Chrome('/usr/local/bin/chromedriver')
        driver.get("http://huntsberg.mmtst.com")
    time.sleep(5)


    # Checking Instagram #
def instagram():
    global driver
    box_title = driver.find_elements_by_css_selector('.box-title')[2]  # Print Instagram #
    print(box_title.text)

    ins = driver.find_elements_by_css_selector('.instagram-lite li a')
    for y in xrange(len(ins)):
        ins = driver.find_elements_by_css_selector('.instagram-lite li a')[y]
        driver.find_elements_by_css_selector('.instagram-lite li a')[y].click()
        time.sleep(2)
        driver.close()
        driver = webdriver.Chrome('/usr/local/bin/chromedriver')
        driver.get("http://huntsberg.mmtst.com")



    # Checking text bottom #
def text_bottom():
    global driver
    promo = driver.find_element_by_css_selector('.col-xs-9 .looks-promos')
    print(promo.text)
    promo = driver.find_element_by_css_selector('.col-xs-9 .looks-promos').text == "LOOKS & PROMOS JUST FOR YOU"
    print(promo) # print text "LOOKS & PROMOS JUST FOR YOU" #


    grey = driver.find_element_by_css_selector('.col-xs-9 .grey1')
    print(grey.text)                         # print "Stay up to date with the latest trends, styles and sales" #
    grey = driver.find_element_by_css_selector('.col-xs-9 .grey1').is_displayed()
    print(grey)


    form = driver.find_element_by_css_selector('.form-terms')
    print(form.text)                                          # print text "*By entering your email address..." #
    form = driver.find_element_by_css_selector('.form-terms').is_displayed()
    print(form)

    subscribe_place_holder = driver.find_element_by_id("email").get_attribute("placeholder")  # Placeholder#
    print(subscribe_place_holder)      # print placeholder "YOUR EMAIL" #


    # Checking Subscribe #
def subscribe():
    global driver
    driver.find_element_by_id('email').send_keys('dekel')
    driver.find_elements_by_class_name('btn')[9].click()
    error_print = driver.find_element_by_class_name('error-msg2')
    print (error_print.text)                               # Print error mail "Invalid address" #
    time.sleep(2)
    driver.find_element_by_id('email').clear()
    time.sleep(1)
    driver.find_element_by_id('email').send_keys('aaa@shellanoo.com')
    driver.find_elements_by_class_name('btn')[9].click()
    time.sleep(2)
    thank_you = driver.find_element_by_class_name('thank-you-msg')
    print (thank_you.text)      # Print "Thank you" #
    time.sleep(5)



    # Footer #
def footer():
    global driver
    shellanoo = driver.find_element_by_class_name('shellanoo').text  # Print shellanoo link#
    print(shellanoo)
    shellanoo = driver.find_element_by_class_name('shellanoo').is_displayed()  # is shellanoo link text displayed #
    print(shellanoo)
    driver.find_element_by_class_name('shellanoo').click() # Click on Shellano link #
    driver.close()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get("http://huntsberg.mmtst.com")

    # Also visit text #
    also_visit = driver.find_element_by_class_name('also-visit').text  # Print also visit: #
    print(also_visit)
    also_visit = driver.find_element_by_class_name('also-visit').is_displayed()  # is also visit displayed #
    print(also_visit)

    # Bellaboo link #
    hunts_logo = driver.find_element_by_class_name('bellaboo-logo')
    print(hunts_logo.find_element_by_tag_name('a').get_attribute("title"))  # Print Bellaboo #
    hunts_logo = driver.find_element_by_class_name('bellaboo-logo').is_displayed()  # is Bellaboo displayed #
    print(hunts_logo)
    driver.find_element_by_class_name('bellaboo-logo').click()
    driver.close()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get("http://huntsberg.mmtst.com")

    # Teddydo link #
    teddy_logo = driver.find_element_by_class_name('teddy-logo')
    print(teddy_logo.find_element_by_tag_name('a').get_attribute("title"))  # Print Teddydo #
    teddy_logo = driver.find_element_by_class_name('teddy-logo').is_displayed()  # is Teddydo displayed #
    print(teddy_logo)
    driver.find_element_by_class_name('teddy-logo').click()
    driver.close()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get("http://huntsberg.mmtst.com")

    # Home link #
    home = driver.find_element_by_class_name('home-link').text  # Print Home #
    print(home)
    home = driver.find_element_by_class_name('home-link').is_displayed()  # Print Home #
    print(home)

    # About page #
    about = driver.find_element_by_class_name('about-link').text  # Print about #
    print(about)
    about = driver.find_element_by_class_name('about-link').is_displayed()  # is About displayed #
    print(about)
    driver.find_element_by_class_name('about-link').click()  # Click on about #
    about = driver.find_element_by_class_name('col-md-12').is_displayed()  # Checking if texts display in the about page #
    print(about)
    about = driver.find_element_by_class_name('inner-page').text
    print(about)
    driver.back()

    # Blog page #
def blog():
    global driver
    blog = driver.find_element_by_class_name('blog-link').text  # Print Blog #
    print(blog)
    blog = driver.find_element_by_class_name('blog-link').is_displayed()  # is Blog displayed #
    print(blog)
    driver.find_element_by_class_name('blog-link').click()
    driver.close()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get("http://huntsberg.mmtst.com")

    # Partner page #
    partner = driver.find_element_by_class_name('partners-link').text  # Print partner name #
    print(partner)
    partner = driver.find_element_by_class_name('partners-link').is_displayed()  # is partners displayed #
    print(partner)
    driver.find_element_by_class_name('partners-link').click()  # Click on partner #
    partner = driver.find_element_by_class_name('inner-page').is_displayed()  # Checking if partners display in the partner page #
    print(partner)
    partner = driver.find_element_by_class_name('inner-page').text
    print(partner)
    logo_list = driver.find_elements_by_css_selector('.logo-list img')
    for logo in logo_list:                       # print all partners name #
        print(logo.get_attribute('alt'))
    driver.back()


    # Privacy page #
    privacy = driver.find_element_by_class_name('privacy-link').text  # Print Privacy #
    print(privacy)
    privacy = driver.find_element_by_class_name('privacy-link').is_displayed()  # is Privacy displayed #
    print(privacy)
    driver.find_element_by_class_name('privacy-link').click()  # Click on Privacy #
    privacy = driver.find_element_by_class_name('inner-page').is_displayed()  # Checking if texts display in the Privacy page #
    print(privacy)
    privacy = driver.find_element_by_class_name('inner-page').text
    print(privacy)
    driver.back()

    # Terms page #
    terms = driver.find_element_by_class_name('terms-link').text  # Print Terms #
    print(terms)
    terms = driver.find_element_by_class_name('terms-link').is_displayed()  # is Terms displayed #
    print(terms)
    driver.find_element_by_class_name('terms-link').click()  # Click on Terms #
    terms = driver.find_element_by_class_name('inner-page').is_displayed()  # Checking if texts display in the Terms page #
    print(terms)
    terms = driver.find_element_by_class_name('inner-page').text
    print(terms)
    driver.back()

    # Contact Us page #
    contact = driver.find_element_by_class_name('contact-link').text  # Print Contact US #
    print(contact)
    contact = driver.find_element_by_class_name('contact-link').is_displayed()  # is Contact US displayed #
    print(contact)
    driver.find_element_by_class_name('contact-link').click()
    driver.close()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get("http://huntsberg.mmtst.com")

    # Facebook #
    facebook = driver.find_element_by_class_name('face-link').is_displayed()  # is Facebook displayed #
    print(facebook)
    driver.find_element_by_class_name('face-link').click()
    driver.close()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get("http://huntsberg.mmtst.com")

    # Instagram #
    instagram = driver.find_element_by_class_name('insta-link').is_displayed()  # is Instagram displayed #
    print(instagram)
    driver.find_element_by_class_name('insta-link').click()
    driver.close()
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')
    driver.get("http://huntsberg.mmtst.com")
    driver.execute_script("window.scrollTo(0, 9000)")   # Scroll page down #
    time.sleep(10)

    driver.close()

if __name__ == '__main__':
    try:
        logging.basicConfig(level=logging.INFO)

        site = ""
        if len(sys.argv) > 1:
            site = sys.argv[1]
        # driver = webdriver.Firefox()
        driver = webdriver.Chrome('/usr/local/bin/chromedriver')
        # driver.get("https://www." + site + ".com/")
        driver.get("http://huntsberg.mmtst.com")
        logging.info('Starting automation on %s Site' % site)
        start_prog()
    except BaseException as w:
        logging.error('Failed to do something: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page error",
                                                                                               ))
    count = 0

    try:
        search()

    except NoSuchElementException as e:
        print "error in search {error}".format(error=e)
        search()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Search error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Search error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Search error",
                                                                                               ))
    try:
        shop_now()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        shop_now()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Shop Now error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Shop now error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Shop Now error",
                                                                                               ))
    try:
        autocomplete()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        autocomplete()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Autocomplete error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Autocomplete error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Autocomplete error",
                                                                                               ))
    try:
        shops()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        shops()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Shops error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Shops error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Shops error",
                                                                                               ))
    try:
        trending()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        trending()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Trending error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Trending error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Trending error",
                                                                                               ))
    try:
        editor_blog()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        editor_blog()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Editor Blog error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page editor blog error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home Page Editor Blog error",
                                                                                               ))
    try:
        instagram()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        instagram()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Instagram error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Instagram error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Instagram error",
                                                                                               ))
    try:
        text_bottom()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        text_bottom()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Text bottom error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page bottom page error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Text bottom error",
                                                                                               ))
    try:
        subscribe()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        subscribe()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Subscribe error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Subscribe error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Subscribe error",
                                                                                               ))
    try:
        footer()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        footer()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Footer error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Footer error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Footer error",
                                                                                               ))
    try:
        blog()

    except NoSuchElementException as e:
        print "error in shop now {error}".format(error=e)
        blog()
        count += 1
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Blog error",
                                                                                               ))
        if count > 3:
            exit()
    except BaseException as w:
        logging.error('Huntsberg-Home page Blog error: %s', w)
        m_now = datetime.now().strftime('%Y-%m-%d_%H-%M')
        driver.save_screenshot('../screenshots/affiliates/screenshot-{time}{error}.png'.format(time=m_now,
                                                                                               error="Huntsberg-Home page Blog error",
                                                                                               ))
    success = datetime.now().strftime('%Y-%m-%d_%H-%M, Huntsberg-home page successful')
    write_to_log_file('affiliates', success)
    time.sleep(5)
    driver.close()