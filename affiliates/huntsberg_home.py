import unittest
import logging
import traceback
import sys
from selenium import webdriver
import time
from selenium.webdriver.common.action_chains import ActionChains

from selenium.webdriver.common.keys import Keys

config = {
    "teddydo": {

        "title": "TEDDYDO - the kids fashion search engine"
    },
    "bellaboo": {

    },
    "huntsberg": {

    }
}

if __name__ == '__main__':
    logging.basicConfig(level=logging.INFO)

    site = ""
    if len(sys.argv) > 1:
        site = sys.argv[1]
    else:
        print "Site not specified"
        exit()

    driver = webdriver.Firefox()
    driver.get("https://www." + site + ".com/")

    logging.info('Starting automation on %s Site' % site)


    # cookies_label = driver.find_element_by_id("cookies-notification")  # find the cookies#
    # is_cookie_displayed_on_first_run = cookies_label.is_displayed()  # is cookie displayed#
    #
    # driver.find_element_by_css_selector("#close-cookies-notification span").click()  # close cookie#
    # is_cookie_not_displayed = not cookies_label.is_displayed()
    #
    # search_bar_origin_place_holder = driver.find_element_by_id("autocomplete").get_attribute(
    #     "placeholder")  # Placeholder#
    #
    # time.sleep(6)
    #
    # is_placeholder_has_changed = search_bar_origin_place_holder != driver.find_element_by_id(
    #         "autocomplete").get_attribute("placeholder")  # checking if placeholder changed#
    #
    # is_have_over_text = driver.find_element_by_class_name("over-brands").text == "*Over 28,000 fashion related brands"

    # driver.find_element_by_class_name("form-control").send_keys("jeans")  # Insert jeans text to search field #
    #
    # driver.find_element_by_class_name("btn-search").click()   # Press on search button #
    #
    # driver.find_element_by_class_name("search-logo").click()  # Back to home page #

    # driver.find_element_by_class_name("form-control").send_keys("d")  # Insert d to search field#
    #
    # time.sleep(2)
    #
    # suggestions = driver.find_element_by_class_name('autocomplete-suggestions')
    #
    # is_autocomplete_displayed = suggestions.is_displayed()  # checking if autocomplete display #
    #
    # autocomplete = driver.find_elements_by_class_name("autocomplete-suggestion")
    #
    # autocomplete_required_length = 10
    #
    # auto_complete_result = len(autocomplete) < autocomplete_required_length  # Checking if autocomplete less than 10 #
    #
    # print (autocomplete)
    #
    # autocomplete[0].click()  # Select an item from autocomplete #
    #
    # driver.find_element_by_class_name("search-logo").click()  # Back to home page #
    #
    # time.sleep(2)


    # Checking footer #
    #
    # driver.find_elements_by_css_selector(".filters li")[4].click() #click on partner#
    # is_Results_displayed = driver.find_element_by_class_name("inner-page")  # checking if results display#
    # is_Results_displayed = is_Results_displayed.is_displayed()
    # print(is_Results_displayed)
    # driver.find_element_by_css_selector("#close-cookies-notification span").click() #close cookie#
    #
    # driver.find_elements_by_css_selector(".filters li")[1].click() #Click on Home#
    #
    #
    # driver.find_elements_by_css_selector(".filters li")[2].click() #Click on About#
    # is_Results_displayed = driver.find_element_by_class_name("inner-page")  # checking if results display#
    # is_Results_displayed = is_Results_displayed.is_displayed()
    # print(is_Results_displayed)
    #
    #
    # driver.find_elements_by_css_selector(".filters li")[1].click() #Click on Home#
    #
    #
    # driver.find_elements_by_css_selector(".filters li")[60].click() #Click on Terms#
    # is_Results_displayed = driver.find_element_by_class_name("inner-page")  # checking if results display#
    # is_Results_displayed = is_Results_displayed.is_displayed()
    # print(is_Results_displayed)
    #
    #
    #
    # driver.find_elements_by_css_selector(".filters li")[1].click() #Click on Home#


    # driver.find_elements_by_css_selector(".filters li")[55].click() #Click on About#
    # driver.find_elements_by_css_selector(".filters li")[1].click() #Click on Home#
    # driver.find_elements_by_css_selector(".filters li")[56].click() #Click on Contact Us#
    # driver.find_elements_by_css_selector(".filters li")[1].click() #Click on Home#
    # driver.find_elements_by_css_selector(".btn-social")[2].click() #Click on Facebook#
    # driver.find_elements_by_css_selector(".btn-social")[3].click() #Click on Instagram#






    cat_opt = ["clothing", "shoes", "bags", "accessories"]

    for i in range(len(cat_opt)):
        count = 0

        sub_menu = driver.find_elements_by_css_selector(".dropdown-toggle-"+cat_opt[count]+" li .text")

        for n in sub_menu:


            cat_menu = driver.find_element_by_class_name("pull-left")  # Mousehover on Category#

            hover = ActionChains(driver).move_to_element(cat_menu)  # Mousehover on Category#
            hover.perform()
            time.sleep(1)

            gender_list_cat = driver.find_elements_by_css_selector('.drop-down-list ul li')

            girls_hover = driver.find_elements_by_css_selector(".dropdown-toggle-" + cat_opt[i] + " label")[0]
            hover = ActionChains(driver).move_to_element(girls_hover)  # Mousehover on Category sub menu#
            hover.perform()

            sub_menu = driver.find_elements_by_css_selector(".dropdown-toggle-" + cat_opt[i] + " li .text")
            print(sub_menu[count].text)
            sub_menu[count].click()

            is_no_Results_displayed = driver.find_element_by_id("results-no-search")  # checking if results display#
            is_Results_displayed = not is_no_Results_displayed.is_displayed()
            print(is_Results_displayed)

            time.sleep(1)

            driver.find_element_by_class_name("search-logo").click()
            time.sleep(1)
            count += 1



    exit()



    try:
        # asserting Here. #
        assert config[site]["title"] in driver.title
        assert is_cookie_displayed_on_first_run
        assert is_cookie_not_displayed
        assert is_placeholder_has_changed
        assert is_have_over_text
        assert is_autocomplete_displayed
        assert autocomplete_length
        assert auto_complete_result
        assert hover
        assert is_Results_displayed

    except AssertionError:
        tb = sys.exc_info()
        traceback.print_tb(tb)
        tb_info = traceback.extract_tb(tb)
        filename, line, func, text = tb_info[-1]

        print('An error occurred on line {} in statement {}'.format(line, text))


        # cookies = driver.get_cookies()

        # for cookie in cookies:
        #    print cookie
