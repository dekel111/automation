from selenium import webdriver
import time
import requests

site = "https://ginseng.life/"
driver = webdriver.Firefox()
driver.get(site)

user_id_from_local = driver.execute_script("return localStorage['uid']") # this should be in the website and not here #

if user_id_from_local == driver.execute_script("return localStorage['uid']"):
    print('local storage UID is OK ')
    print(user_id_from_local)
else:
    print ('local storage UID is changed')
# s = requests.session()
# print s

time.sleep(2)
driver.close()

if __name__ == '__main__':
    pass
